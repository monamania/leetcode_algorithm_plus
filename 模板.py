#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@ Author: CJK_Monomania
@ Data: 2021-12-21
"""

from typing import *
from pprint import pprint
import numpy as np  # 力扣里面没有
import pandas as pd
import sys   # sys.maxsize---int的最大值

'''
@ 题目: Python
@ 参考链接: 
int最大值: sys.maxsize
float最大值: float('inf')
'''
# Definition for singly-linked list.--链表
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def function(self, cost: List[int]) -> int:
        pass


def main():
    solu = Solution()
    print(solu.function())


if __name__ == '__main__':
    main()