/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include<numeric>  // 用于对vector求和----accumulate函数
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<T>& arr, string const str){
    cout << str << ": ";
    // typename T::const_iterator it = arr.begin();
    for(auto it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
// 注意一个问题 abab 和 abac的next数组是一样的---需要增加一个条件 s[n - 1] != s[n - 1 - maxLen]
// 思路: len(s) % (len(s) -  maxLen) = 0----maxLen就是最长公共子串
// https://leetcode-cn.com/problems/repeated-substring-pattern/solution/acm-xuan-shou-tu-jie-leetcode-zhong-fu-d-vl7i/
class Solution {
public:
    bool repeatedSubstringPattern(string s) {
        int lenS = s.length();
        if (lenS <= 1) return false;
        vector<int> next(lenS);
        vector<int> next_cur(lenS);
        getNext(next,s);
        getNext_cur(next_cur,s);

        int maxLen = next[lenS-1]+1;
        // s[lenS - 1] != s[lenS - 1 - maxLen]意味着，前缀表最后一位对应为0
        if (maxLen == 0 || s[lenS - 1] != s[lenS - 1 - maxLen]) return false;
        // cout << maxLen << endl;
        if(lenS % (lenS-maxLen) == 0) return true;
        else return false;
    }
    void getNext(vector<int>& next, string t){
        int i=0;
        int j=-1;
        next[0] = j;
        while(i<t.length()) {
            // 没匹配时----（隐含j!=-1才可以进行匹配）
            if(j >=0 && t[i] != t[j]){
                j = next[j];
            } else {
                i++;
                j++;
                next[i]=j;
            }
        }
        visit_arr(next,"自己 next: ");
    }
    void getNext_cur(vector<int>& next, string t){
        int j = -1;
        next[0] = j;
        for (int i=1; i<t.length(); i++) {  // 注意i从1开始
            while (j >= 0 && t[i] != t[j+1]) {  // 前后缀不相同了
                j = next[j]; // 回退
            }
            if (t[i] == t[j+1]) { // 找到相同的前后缀
                j++;
            }
            next[i] = j;
        }
        visit_arr(next,"卡尔 nest: ");
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s = "abab";
    // string s = "abac";
    // string s = "asdfasdfasdf";
    string s = "aba";
    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    cout << std::boolalpha << solu.repeatedSubstringPattern(s) << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}