<!-- TOC -->

- [1. KMP模式匹配算法](#1-kmp模式匹配算法)
  - [1.1. 要解决的问题](#11-要解决的问题)
  - [1.2. 方法](#12-方法)
    - [1.2.1. 暴力法](#121-暴力法)
  - [1.3. KMP算法-理论](#13-kmp算法-理论)
    - [得出前缀表：关键找出最长相等前后缀](#得出前缀表关键找出最长相等前后缀)
    - [如何利用前缀表匹配](#如何利用前缀表匹配)
    - [next数组（prefix数组）----其实就是前缀表的改版](#next数组prefix数组----其实就是前缀表的改版)
  - [KMP算法-实现](#kmp算法-实现)
    - [求next数组](#求next数组)
      - [直接拿前缀表作为next数组](#直接拿前缀表作为next数组)

<!-- /TOC -->
!!! 建议看《大话数据结构》

[代码随想录-KMP讲解--复杂](https://programmercarl.com/0028.%E5%AE%9E%E7%8E%B0strStr.html#%E5%85%B6%E4%BB%96%E8%AF%AD%E8%A8%80%E7%89%88%E6%9C%AC)

[ACM大佬--采用](https://mp.weixin.qq.com/s?__biz=MzI0NjAxMDU5NA==&mid=2475924907&idx=1&sn=6f6fc1475be2d7d2ca5ab6e0ec755bca&chksm=ff22fa26c8557330a906f6ed9f444d71064a590109b093d8e97f0ab1cd82e5106a5138e8aecd&scene=21#wechat_redirect)----与大话数据结构相同
# 1. KMP模式匹配算法

## 1.1. 要解决的问题
文本串：aabaabaaf

模式串：aabaaf

要求：在文本串中找出模式串出现的第一个位置并返回，如果不存在返回-1，模式串为空则返回0


## 1.2. 方法
### 1.2.1. 暴力法
两层for循环，挨个匹配

时间复杂度：$O(m\times n)$

## 1.3. KMP算法-理论
dagshub

### 得出前缀表：关键找出最长相等前后缀
关键：求出一个子串（模式串）的**最长相等前后缀**

如：aabaaf 很明显最长相等前后缀的下标为2即b这个位置

举例
```
子串    最长相等前后缀长度
a       0（没有）
aa      1   为a
aab     0   没有
aaba    1   为a
aabaa   2   为aa
aabaaf  0   没有

所以前缀表：010120
按我的算法实现方式，next为：-101012   【右移一位并将0索引处设置为-1】
```
### 如何利用前缀表匹配
文本串：aabaabaaf

模式串：aabaaf

前缀表：010120

  下标：012345

> 当5的位置下标f没有匹配时，查找前缀表前一个位置的值（最长相等前后缀）为2
> 即从下标为2的位置开始从模式串进行匹配

### next数组（prefix数组）----其实就是前缀表的改版


## KMP算法-实现
前言
文本串：aabaabaaf

模式串：aabaaf

前缀表：010120

  下标：012345

### 求next数组
存在多种实现方式
- 整体右移一位并不减1【推荐】---0位作为特殊情况设置为-1
  - **但是**由于位移过，需要注意一个问题 abab 和 abac的next数组是一样的---判断两个模式串是否相等时-在next相等的基础上需要增加一个条件 s[n - 1] != s[n - 1 - maxLen]
  - s[n - 1] != s[n - 1 - maxLen]意味着，前缀表最后一位对应为0
  > 文本串：aabaabaaf
  > 
  > 模式串：aabaaf
  > 
  > 前缀表：010120
  >
  >  next: -1 0 1 0 1 2 --- 一一对应更方便
- 整体减1【卡尔】
  > 文本串：aabaabaaf
  > 
  > 模式串：aabaaf
  > 
  > 前缀表：010120
  >
  >  next: -1 0 -1 0 1 -1
- 也有直接拿前缀表作为next数组


![整体减1](https://pic.leetcode-cn.com/1609232094-zPjQMj-file_1609232095602)
#### 直接拿前缀表作为next数组
步骤
- 初始化
- 前后缀不同的情况
- 前后缀相同的情况
- 更新next数组的值

**伪代码---不建议--carl的**
```cpp
// 输入next数组、模式串s
// 指定i指向后缀末尾位置，j指向前缀末尾位置
void getNext(next, s){
    // 初始化
    j = 0;
    next[0]=0;
    for(i=1; i<=s.length; i++){
        // 前后缀不同的情况
        while(j>0&& s[i]!=s[j]){
            j = next[j-1];
        }
        if(s[i]==s[j]){
            j++;
            next[i] = j;
        }
    }
}
```




自己的：相较卡尔的进行了右移一位并不减1的操作，更加方便。但是由于位移过，需要注意一个问题 abab 和 abac的next数组是一样的---判断两个模式串是否相等时-在next相等的基础上需要增加一个条件 s[n - 1] != s[n - 1 - maxLen]
- s[n - 1] != s[n - 1 - maxLen]意味着，前缀表最后一位对应为0
如力扣题目《重复的子字符串》就会出一些问题
```C++
class Solution {
public:
    // s为文本串，t为模式串
    int strStr(string s, string t) {
        int lenS = s.length(), lenT = t.length();
        if (lenT == 0) return 0;
        int next[lenT+1];
        getNext(next, t);
        // 检查一下next对不对
        for(int i = 0; i < lenT+1; i++) {
            cout << next[i] << " ";
        }
        cout << endl;

        // 开始匹配
        int i=0,j=0;
        while(i<lenS && j<lenT) {
            // 如果没有匹配
            if(j>=0 && s[i] != t[j]){
                j = next[j];
            } else {
                i++;
                j++;
            }
        }
        if(j>=lenT) return i-lenT;
        else return -1;
    }
    void getNext(int *next, string t) {
        int i = 0; // 遍历串---0不可以不管
        int j = -1; // 代表当前遍历时的，最大前后缀位置长度
        next[0] = j; // 初始化别忘了！！
        while(i < t.length()) {
            // 如果没有匹配到则回溯--主要如果开始匹配操作时，那j肯定不是-1了
            if(j >= 0 && t[i]!=t[j]){
                j = next[j];
            }else{
                i++; 
                j++;
                next[i] = j;
            }
        }
    }
};
```