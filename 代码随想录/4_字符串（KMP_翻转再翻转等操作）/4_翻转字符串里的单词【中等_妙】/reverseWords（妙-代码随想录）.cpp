/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include<numeric>  // 用于对vector求和----accumulate函数
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<T>& arr, string const str){
    cout << str << ": ";
    // typename T::const_iterator it = arr.begin();
    for(auto it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// https://leetcode-cn.com/problems/reverse-words-in-a-string/
/**
 * @brief 解题思路
 * 1 移除多余空格
 * 2 将整个字符串反转
 * 3 将单个单词反转
 */
class Solution {
public:
    string reverseWords(string s) {
        int left = 0,right = 0;
        // 1 删除多余重复的空格
        removeExtraSpaces(s);
        // cout << s << endl;
        // 2 反转整个字符串
        myrevers(s, 0, s.length()-1);
        // cout << s << endl;
        for(;right<s.length();right++) {
            if(s[right] == ' ') {
                myrevers(s, left, right - 1);
                left = right+1;
            }
        }
        myrevers(s, left, right - 1);
        return s;
    }

    void myrevers(string& s, int start, int end) {
        for(int i = start, j = end;i < j;i++,j--) {
            swap(s[i], s[j]);
        }
    }

    void removeExtraSpaces(string& s){
        // 利用快慢指针实现移除空格操作----慢的用来记录，快的指针用来遍历
        int slow=0,fast=0;
        // 过滤掉字符串前面的空格
        while(s.length()>0 && fast < s.length() && s[fast] == ' ') {
            fast++;
        }
        // 后续重复的空格都删掉
        for(;s.length()>0 && fast < s.length();fast++) {
            if(fast - 1 > 0 && s[fast-1] == s[fast] && s[fast] == ' ') {
                continue;
            } else {
                s[slow++] = s[fast];
            }
        }
        // 删除后面的空格---为什么不用循环，是由于上一步已经去掉重复的空格了
        if(slow - 1 > 0 && s[slow-1] == ' ') {
            s.resize(slow - 1);
        } else {
            s.resize(slow); 
        }
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s = " 123 3  46  ";
    // cout << s.length() << endl;
    // solu.removeExtraSpaces(s);
    // cout << s << ":" << s.length() << endl;

    // string s = "the sky is blue";
    string s = "  hello world  ";
    // string s = "  Bob    Loves  Alice   ";
    string ans = solu.reverseWords(s);

    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    cout << "answer: " << ans << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}