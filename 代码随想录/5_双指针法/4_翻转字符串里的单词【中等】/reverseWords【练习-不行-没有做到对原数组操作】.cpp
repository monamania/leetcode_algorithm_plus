/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include<numeric>  // 用于对vector求和----accumulate函数
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<T>& arr, string const str){
    cout << str << ": ";
    // typename T::const_iterator it = arr.begin();
    for(auto it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// https://leetcode-cn.com/problems/reverse-words-in-a-string/
/**
 * @brief 解题思路
 * 1 处理空格  --- eraseExtraSpaces(string& s);
 * 2 整体翻转  ---  myReverse(string, start, end)
 * 3 翻转当个单词
 */
class Solution {
public:
    string reverseWords(string s) {
        if (s.length() == 0) return s;
        // 处理空格
        eraseExtraSpaces(s);
        // 整体翻转
        myReverse(s, 0, s.length()-1);
        int left = 0,right = 0;
        for(;right<s.length();right++) {
            if(s[right] == ' ') {
                myReverse(s, left, right - 1);
                left = right+1;
            }
        }
        myReverse(s, left, right - 1);
        return s;
    }
    // 没有做到对原数据操作
    void eraseExtraSpaces(string& s){
        int slow=0,fast=0;
        int lenS = s.length();
        // 删除首部空格
        for(slow;slow<lenS && s[slow] == ' ';slow++);
        // 删除尾部空格
        for(fast = lenS-1;fast>slow && s[fast] == ' ';fast--);
        s = s.substr(slow,fast-slow+1);
        for(slow=0,fast=0;fast<s.length();fast++){
            if(fast>0 && s[fast] == s[fast-1] && s[fast] == ' ') continue;
            else s[slow++] = s[fast];
        }
        s.resize(slow);
        // cout << s;   
    }
    // 左闭右闭
    void myReverse(string& s, int start, int end){
        for(int slow=start, fast=end; fast > slow; slow++,fast--){
            swap(s[slow],s[fast]);
        }
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s="  ad  f  ";
    string s="  hello world  ";
    solu.eraseExtraSpaces(s);
    cout << s << ";"<<endl;
    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    cout << solu.reverseWords(s) << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}