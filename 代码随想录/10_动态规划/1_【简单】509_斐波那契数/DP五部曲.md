[代码随想录](https://programmercarl.com/0509.%E6%96%90%E6%B3%A2%E9%82%A3%E5%A5%91%E6%95%B0.html)
# 斐波那契数题目
[509_斐波那契数](https://leetcode-cn.com/problems/fibonacci-number/)

斐波那契数，通常用 F(n) 表示，形成的序列称为 斐波那契数列 。该数列由 0 和 1 开始，后面的每一项数字都是前面两项数字的和。也就是： F(0) = 0，F(1) = 1 F(n) = F(n - 1) + F(n - 2)，其中 n > 1 给你n ，请计算 F(n) 。

示例 1： 输入：2 输出：1 解释：F(2) = F(1) + F(0) = 1 + 0 = 1

示例 2： 输入：3 输出：2 解释：F(3) = F(2) + F(1) = 1 + 1 = 2

示例 3： 输入：4 输出：3 解释：F(4) = F(3) + F(2) = 2 + 1 = 3

提示：

0 <= n <= 30


# DP五部曲

1. 确定dp数组以及下标的含义

    dp[i]的定义为：第i个数的斐波那契数值是dp[i]

2. 确定递推公式【重要】

    **题目已经把递推公式直接给我们了**：状态转移方程 `dp[i] = dp[i - 1] + dp[i - 2];`

3. dp数组如何初始化

    ```C++
    dp[0] = 0;
    dp[1] = 1;
    ```

4. 确定遍历顺序--从i=2开始【边界条件为<=n】
   
    从递归公式dp[i] = dp[i - 1] + dp[i - 2];中可以看出，dp[i]是依赖 dp[i - 1] 和 dp[i - 2]，那么遍历的顺序一定是从前到后遍历的

5. 举例推导dp数组

按照这个递推公式dp[i] = dp[i - 1] + dp[i - 2]，我们来推导一下，当N为10的时候，dp数组应该是如下的数列：

0 1 1 2 3 5 8 13 21 34 55

如果代码写出来，发现结果不对，就把dp数组打印出来看看和我们推导的数列是不是一致的。

# 程序源码

```CPP
class Solution {
public:
    int fib(int N) {
        if (N <= 1) return N;
        vector<int> dp(N + 1);
        dp[0] = 0;
        dp[1] = 1;
        for (int i = 2; i <= N; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[N];
    }
};
```
- 时间复杂度：O(n)
- 空间复杂度：O(n)

**优化**
当然可以发现，我们只需要维护两个数值就可以了，不需要记录整个序列。

```c++
class Solution {
public:
    int fib(int N) {
        if (N <= 1) return N;
        int dp[2];
        dp[0] = 0;
        dp[1] = 1;
        for (int i = 2; i <= N; i++) {
            int sum = dp[0] + dp[1];
            dp[0] = dp[1];
            dp[1] = sum;
        }
        return dp[1];
    }
};
```
- 时间复杂度：O(n)
- 空间复杂度：O(1)


# 递归解法
```C++
class Solution {
public:
    int fib(int N) {
        if (N < 2) return N;
        return fib(N - 1) + fib(N - 2);
    }
};
```
- 时间复杂度：O(2^n)
- 空间复杂度：O(n) 算上了编程语言中实现递归的系统栈所占空间
