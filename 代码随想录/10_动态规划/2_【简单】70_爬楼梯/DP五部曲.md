[代码随想录](https://programmercarl.com/0070.%E7%88%AC%E6%A5%BC%E6%A2%AF.html#_70-%E7%88%AC%E6%A5%BC%E6%A2%AF)
# 爬楼梯题目
[70_爬楼梯](https://leetcode-cn.com/problems/climbing-stairs/)

假设你正在爬楼梯。需要 n 阶你才能到达楼顶。

每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？

注意：给定 n 是一个正整数。

示例 1： 输入： 2 输出： 2 解释： 有两种方法可以爬到楼顶。
```
1 阶 + 1 阶
2 阶
```
示例 2： 输入： 3 输出： 3 解释： 有三种方法可以爬到楼顶。
```
1 阶 + 1 阶 + 1 阶
1 阶 + 2 阶
2 阶 + 1 阶
```
# 思路


## DP五部曲

1. 确定dp数组以及下标的含义

    dp[i]：爬到第i层楼梯，有dp[i]种方法....同时确定大小

2. 确定递推公式【重要】

    dp[i]可以从两个方向推出来
     1. 从dp[i-1]再走1个台阶就到dp[i];
     2. 从dp[i-2]再走2个台阶就到dp[i]; 【注意不要想着连续两个1步，这在上一个情况下已经包括了】
    
    因此: `dp[i] = dp[i - 1] + dp[i - 2]`

3. dp数组如何初始化

    ```C++
    //dp[0] = 0; // 按照答案推导，认为一开始就是1----题目说n为正整数，没有0的情况
    dp[1] = 1;
    dp[2] = 2;
    ```

4. 确定遍历顺序--从i=几开始 **确定startIndex**【边界条件<=n】
    从递归公式dp[i] = dp[i - 1] + dp[i - 2];中可以看出，dp[i]是依赖 dp[i - 1] 和 dp[i - 2]，那么遍历的顺序一定是从前到后遍历的

5. 举例推导dp数组

按照这个递推公式dp[i] = dp[i - 1] + dp[i - 2]，我们来推导一下，当N为10的时候，dp数组应该是如下的数列：

1 2 3 5 8 13 21 34 55

如果代码写出来，发现结果不对，就把dp数组打印出来看看和我们推导的数列是不是一致的。

# 程序源码

```CPP
class Solution {
public:
    int climbStairs(int n) {
        if(n <= 2) return n;
        vector<int> dp(n+1);
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; ++i) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }
};
```
- 时间复杂度：O(n)
- 空间复杂度：O(n)

**优化**
当然可以发现，我们只需要维护两个数值就可以了，不需要记录整个序列。

```c++
class Solution {
public:
    int climbStairs(int n) {
        if(n <= 2) return n;
        int dp[3];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            int sum = dp[1] + dp[2];
            dp[1] = dp[2];
            dp[2] = sum;
        }
        return dp[2];
    }
};
```
- 时间复杂度：O(n)
- 空间复杂度：O(1)


# 递归解法
```C++
class Solution {
public:
    int fib(int N) {
        if (N <= 2) return N;
        return fib(N - 1) + fib(N - 2);
    }
};
```
- 时间复杂度：O(2^n)
- 空间复杂度：O(n) 算上了编程语言中实现递归的系统栈所占空间
