/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include<bits/stdc++.h>  // 万能头文件
#include <vector> 
#include <string>
#include <cstring>  // atoi()【需要将string 用 c_str()函数转换】--stoi()函数--用于字符串转整数
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>  // greater less等
#include <iterator>
#include<numeric>  // 用于对vector求和----accumulate函数
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<T>& arr, string const str){
    cout << str << ": ";
    // typename T::const_iterator it = arr.begin();
    for(auto it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
// struct TreeNode {
//     int val;
//     TreeNode *left;
//     TreeNode *right;
//     TreeNode() : val(0), left(nullptr), right(nullptr) {}
//     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
//     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
// };
typedef struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
}TreeNode,*TreeLink;



// https://leetcode-cn.com/problems/symmetric-tree/
/**
 * @brief 解题思路
 * 首先想清楚，判断对称二叉树要比较的是哪两个节点，要比较的可不是左右节点！
 * 对于二叉树是否对称，要比较的是根节点的左子树与右子树是不是相互翻转的，
 * 理解这一点就知道了其实我们要比较的是两个树（这两个树是根节点的左右子树），
 * 所以在递归遍历的过程中，也是要同时遍历两棵树。
 * 
 * 本题遍历只能是“后序遍历”，因为我们要通过递归函数的返回值来判断两个子树的内侧节点和外侧节点是否相等。
 * 
 */
class Solution {
public:
    bool isSymmetric(TreeNode* root) {
        if (!root) return true;
        return compare(root->left,root->right);
        
    }
    bool compare(TreeNode* left, TreeNode* right) {
        // 确定终止条件
        /*节点为空的情况有：（注意我们比较的其实不是左孩子和右孩子，所以如下我称之为左节点右节点）
            左节点为空，右节点不为空，不对称，return false
            左不为空，右为空，不对称 return false
            左右都为空，对称，返回true
        此时已经排除掉了节点为空的情况，那么剩下的就是左右节点不为空：
            左右都不为空，比较节点数值，不相同就return false
        此时左右节点不为空，且数值也不相同的情况我们也处理了。*/

        if (left == NULL && right != NULL) return false;
        else if (left != NULL && right == NULL) return false;
        else if (left == NULL && right == NULL) return true;
        else if (left->val != right->val) return false; // 注意这里我没有使用else

        // 确定单层逻辑
        bool outside = compare(left->left, right->right);   // 外层-左子树：左、 右子树：右
        bool inside = compare(left->right, right->left);    // 内层-左子树：右、 右子树：左
        bool isSame = outside && inside;                    // 左子树：中、 右子树：中（逻辑处理）
        return isSame;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // 常见边界值
    // cout << INT32_MAX << endl;
    // cout << INT32_MIN << endl;
    // cout << INT_MAX << endl;
    // cout << INT_MIN << endl;
    int target = 7;
    vector<int> nums = {2,3,1,2,4,3};
    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    // cout << solu.minSubArrayLen(target, nums) << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}