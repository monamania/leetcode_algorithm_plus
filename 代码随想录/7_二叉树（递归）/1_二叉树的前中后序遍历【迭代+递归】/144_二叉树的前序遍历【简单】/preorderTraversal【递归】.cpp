#include<iostream>
#include<vector>
using namespace std;

// 二叉树的定义
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int val) : val(val) , left(nullptr), right(nullptr) {}
    TreeNode(int val, TreeNode *left, TreeNode *right) : val(val), left(left), right(right) {}
};

class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        if (!root) return vector<int>();
        vector<int> res;
        traverse(root, res);
        return res;
    }
    void traverse(TreeNode* cur, vector<int>& res) {
        if (!cur) return;
        res.push_back(cur->val);
        traverse(cur->left, res);
        traverse(cur->right, res);
    }
};

int main(){
    
}
