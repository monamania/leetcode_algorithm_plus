#include<iostream>
#include<vector>
#include<stack>
using namespace std;

// 二叉树的定义
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int val) : val(val) , left(nullptr), right(nullptr) {}
    TreeNode(int val, TreeNode *left, TreeNode *right) : val(val), left(left), right(right) {}
};

class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        stack<TreeNode*> st;
        if (!root) return vector<int>();
        vector<int> res;
        st.push(root);
        while(!st.empty()) {
            TreeNode* node = st.top();  // 中
            st.pop();
            res.push_back(node->val);
            // 入栈
            if(node->right) st.push(node->right);  // 右
            if(node->left) st.push(node->left);    // 左
            // 为啥先右后左呢，这是由于入栈后，下一个循环出栈时
        }
        return res;
    }
};

// 统一写法
class Solution {
public:
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int> result;
        stack<TreeNode*> st;
        if (root != NULL) st.push(root);
        while (!st.empty()) {
            TreeNode* node = st.top();
            if (node != NULL) {
                st.pop();
                if (node->right) st.push(node->right);  // 右
                if (node->left) st.push(node->left);    // 左
                st.push(node);                          // 中
                st.push(NULL);
            } else {
                st.pop();
                node = st.top();
                st.pop();
                result.push_back(node->val);
            }
        }
        return result;
    }
};

int main(){
    
}
