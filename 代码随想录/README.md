# 前言
本部分主要用于记录“代码随想录”的学习情况，旨在提高算法思维能力，进而提高编程能力。
[随想录地址](https://programmercarl.com/)

# 小技巧
## VIM的配置
使用“代码随想录”里的配置----PowerVim
Github地址：https://github.com/youngyangyang04/PowerVim
![效果](https://code-thinking.cdn.bcebos.com/gifs/vim_overview.gif)

### 安装

```shell
git clone https://github.com/youngyangyang04/PowerVim.git

cd PowerVim

sh install.sh
```

### 特性
目前PowerVim支持如下功能，这些都是自己配置的：

 - CPP、PHP、JAVA代码补全，如果需要其他语言补全，可自行配置关键字列表在PowerVim/.vim/dictionary目录下
 - 显示文件函数变量列表
 - MiniBuf显示打开过的文件
 - 语法高亮支持C++ (including C++11)、 Go、Java、 Php、 Html、 Json 和 Markdown
 - 显示git状态，和主干或分支的添加修改删除的情况
 - 显示项目文件目录，方便快速打开
 - 快速注释，使用gcc注释当前行，gc注释选中的块
 - 项目内搜索关键字和文件夹
 - 漂亮的颜色搭配和状态栏显示


### 使用
![使用说明](https://code-thinking-1253855093.file.myqcloud.com/pics/20211013102249.png)