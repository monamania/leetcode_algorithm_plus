#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@ Author: CJK_Monomania
@ Data: 2021-12-21
"""

from typing import *
from pprint import pprint
import numpy as np  # 力扣里面没有
import pandas as pd
import sys   # sys.maxsize---int的最大值

'''
@ 题目: Python
@ 参考链接: 
int最大值: sys.maxsize
float最大值: float('inf')
'''
# Definition for singly-linked list.--链表
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next
'''
@brief 两种方式
1 直接使用原来的链表来进行移除节点操作----头节点单独处理
2 设置一个虚拟头结点在进行移除节点操作----最后返回时记得要返回虚拟头节点的next
'''
# 方式1：直接使用原来的链表进行移除节点操作
class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        # 针对头节点
        while head != None and head.val == val:
            tmp = head
            head = head.next
            del tmp  # 可以没有这一步---有垃圾管理机制
        
        # 针对非头节点
        cur = head
        while cur != None and cur.next != None:
            if(cur.next.val == val):
                tmp = cur.next
                cur.next = tmp.next
                del tmp
            else:
                cur = cur.next
        
        return head


# 方式2：设置一个虚拟头结点在进行移除节点操作
class Solution:
    def removeElements(self, head: ListNode, val: int) -> ListNode:
        # 创建一个假的头节点
        dummyHead = ListNode(val, head)
        
        # 针对非头节点
        cur = dummyHead
        while cur != None and cur.next != None:
            if(cur.next.val == val):
                tmp = cur.next
                cur.next = tmp.next
                del tmp
            else:
                cur = cur.next
        
        head = dummyHead.next
        del dummyHead
        return head



def main():
    solu = Solution()
    print(solu.function())


if __name__ == '__main__':
    main()