#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@ Author: CJK_Monomania
@ Data: 2021-12-21
"""

from typing import *
from pprint import pprint
import sys

'''
@ 题目: Python
@ 参考链接: 
'''
# # 版本1 
# class Solution:
#     def minSubArrayLen(self, target: int, nums: List[int]) -> int:
#         left = 0
#         right = 0
#         ans = sys.maxsize
#         sum = 0
#         for right in range(len(nums)):
#             sum += nums[right]
#             while(sum >= target):
#                 ans = min(right - left + 1, ans)
#                 sum -= nums[left]
#                 left += 1
        
#         return  0 if ans == sys.maxsize else ans

# # 版本2
# class Solution:
#     def minSubArrayLen(self, target: int, nums: List[int]) -> int:
#         left = 0
#         right = 0
#         ans = sys.maxsize
#         while(right < len(nums)):
#             if(sum(nums[left:right+1]) < target):
#                 right += 1
#             else:
#                 ans = min(right - left + 1, ans)
#                 left += 1
        
#         return  0 if ans == sys.maxsize else ans

# 版本3
class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        left = 0
        right = 0
        ans = sys.maxsize
        tmp = -1
        n_sum = 0
        while(right < len(nums) and left <= right):
            if(tmp != right):
                n_sum += nums[right]
                tmp = right
            
            if(n_sum < target):
                right += 1
            else:
                ans = min(right - left + 1, ans)
                n_sum -= nums[left]
                left += 1
            
        return  0 if ans == sys.maxsize else ans


def main():
    target = 7
    nums = [2,3,1,2,4,3]
    solu = Solution()
    print(solu.minSubArrayLen(target, nums))
    

if __name__ == '__main__':
    main()