#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@ Author: CJK_Monomania
@ Data: 2021-12-21
"""

from typing import *
from pprint import pprint
import sys   # sys.maxsize---int的最大值

'''
@ 题目: Python
@ 参考链接: 
int最大值: sys.maxsize
float最大值: float('inf')
'''

class Solution:
    def minWindow(self, s: str, t: str) -> str:
        pass


def main():
    solu = Solution()
    print(solu.function())


if __name__ == '__main__':
    main()