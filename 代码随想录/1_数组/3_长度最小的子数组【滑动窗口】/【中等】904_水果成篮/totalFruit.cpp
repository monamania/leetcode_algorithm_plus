/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<int>& arr, string const str){
    vector<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

/**
 * @brief 题目解析
 * 英文直译如下 您正在参观一个农场，该农场有一排从左到右排列的果树。
 * 树由整数数组fruits表示，其中水果[i]是第i棵树产生的水果类型。 
 * 你想收集尽可能多的水果。但是，所有者有一些严格的规则，您必须遵守： 
 *      你只有两个篮子，每个篮子只能装一种水果。每篮水果的数量没有限制。 
 *      从您选择的任何一棵树开始，您必须在向右移动时从每棵树（包括起始树）中恰好摘下一个水果,摘下的水果必须放在你的一个篮子里。 
 *      一旦你到了一棵树上，树上的果实没法放入你的篮子（两个篮子已经满了），你必须停下来。 
 *      给定整数数组水果，返回可以拾取的最大水果数。 
 * 
 * 实质：本题，其实就是选只有两个元素的最长连续子序列，比如1，2，3，2，2最长就是2，3，2，2（只包括2或者3，而且是最长的）。
 *     --- 所以可以滑动窗口法
 */
class Solution {
public:
    int totalFruit(vector<int>& fruits) {
        int ans = 0; // 为了求出最大总量
        int left=0, right=0;
        int ln = fruits[left], rn = fruits[right];
        while(right < fruits.size()) {
            if (fruits[right] == ln||fruits[right] == rn) { // 继续
                ans = ans > (right-left+1) ? ans : (right-left+1);
                right++;
            } else {
                // if (ln == rn) {
                //     rn = fruits[right];
                // } else {
                    left = right - 1;
                    ln = fruits[left];
                    while(left >= 1 && fruits[left - 1] == ln) left--;
                    rn = fruits[right];
                    cout << ln << " " << rn << " " <<endl;
                // }

                ans = ans > (right-left+1) ? ans : (right-left+1);
            }
        }
        return ans;
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    // 常见边界值
    // cout << INT32_MAX << endl;
    // cout << INT32_MIN << endl;
    // cout << INT_MAX << endl;
    // cout << INT_MIN << endl;
    // vector<int> fruits = {1,2,1};
    vector<int> fruits = {3,3,3,1,2,1,1,2,3,3,4};
    // vector<int> fruits = {1,2,3,2,2};
    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    cout << solu.totalFruit(fruits) << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}