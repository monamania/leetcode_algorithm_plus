#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@ Author: CJK_Monomania
@ Data: 2021-12-21
"""

from typing import *
from pprint import pprint

'''
@ 题目: Python
@ 参考链接: 
'''

class Solution:
    def totalFruit(self, fruits: List[int]) -> int:
        left = 0
        right = 0
        lNum = fruits[left]
        rNum = fruits[right]
        ans = 0
        while(right < len(fruits)):
            # 如果在这两个值里面就继续
            # print("left: %d, right: %d, ans: %d" % (left, right, ans))
            if fruits[right] == lNum or fruits[right] == rNum:
                ans = max(right- left + 1, ans)
                right += 1
            else:
                if lNum==rNum:
                    rNum = fruits[right]
                else:
                    left = right - 1
                    lNum = fruits[left]
                    while left > 0 and lNum == fruits[left - 1]:
                        left -= 1
                    rNum = fruits[right]
                # ans = max(right- left + 1, ans)
        return ans


def main():
    solu = Solution()
    fruits = [5,0,0,7,0,7,2,7]
    print(solu.totalFruit(fruits))


if __name__ == '__main__':
    main()