#!/usr/bin/env python3
from typing import List

class Solution:
    def isPerfectSquare(self, num:int) -> bool:
        sqrt = self.mySqrt(num)
        return sqrt*sqrt == num
    
    def mySqrt(self, num: int) -> int:
        if (num == 1):
            return 1;
        left = 0
        right = num
        while right - left>1:
            mid = left + (right - left)//2
            if mid*mid > num:
                right = mid
            else:
                left = mid
        return left

# 最最简单的方式
class Solution:
    def isPerfectSquare(self, num:int) -> bool:
        return num ** 0.5 % 1 == num

def main():
    solu = Solution()
    nums = [-1,0,3,5,9,12]
    target = 9
    # print(solu.search(nums, target)


if __name__ == '__main__':
    main()