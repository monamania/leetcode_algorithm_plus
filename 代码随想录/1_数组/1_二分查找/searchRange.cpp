/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<int>& arr, string const str){
    vector<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    // 使用[left, right]
    int search(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size()-1;
        while(left <= right) {
            int mid = left + (right - left)/2;
            if(nums[mid] == target) return mid;
            if(nums[mid] > target) right = mid - 1;
            else if (nums[mid] < target) left = mid + 1;
        }
        return -1;

    }

    vector<int> searchRange(vector<int>& nums, int target) {
        int mid = search(nums, target);
        if (mid == -1) return vector<int>({-1, -1});
        if (nums.size()==1) return vector<int>({mid,mid});
        int low = mid, high = mid;
        while (low>=0 && high <= nums.size()-1)
        {
            if(low <= 0) low = 0;
            if(high >= nums.size()-1 ) high = nums.size()-1;
            if (nums[low] != target && nums[high] != target) break;
            if (nums[low] == target){
                low--;
            }
            if (nums[high] == target){
                high++;
            }
        }
        cout << low << " " << high<<endl;
        if(low <= 0) low = 0;
        if(high >= nums.size()-1 ) high = nums.size()-1;
        // if (nums.size()==1)
        //     return vector<int>({low, low});
        if(nums[low] != target) low = min(low+1, (int)nums.size()-1);
        if (nums[high] != target) high = max(high-1, 0);
        return vector<int>({low, high});
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    vector<int> nums = {2,2};
    int target = 2;
    // cout << solu.search(nums, target) << endl;
    vector<int> ans = solu.searchRange(nums, target);
    visit_arr(ans, "ans: ");

    return 0;
}