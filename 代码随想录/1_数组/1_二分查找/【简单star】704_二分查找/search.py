#!/usr/bin/env python3
from typing import List

# 第一种情况[left,right]
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        left = 0
        right = len(nums) - 1
        while(left <= right):
            # mid = (left + right)/2
            mid = left + (right-left)//2  # 整除
            if nums[mid] > target:
                right = mid - 1
            elif nums[mid] < target:
                left = mid + 1
            else:
                return mid
        return -1

# 第二种情况[left,right)
class Solution:
    def search(self, nums: List[int], target: int) -> int:
        left = 0
        right = len(nums)
        while(left < right):
            # mid = (left + right)/2
            mid = left + (right-left)//2  # 整除
            if nums[mid] > target:
                right = mid
            elif nums[mid] < target:
                left = mid + 1
            else:
                return mid
        return -1

def main():
    solu = Solution()
    nums = [-1,0,3,5,9,12]
    target = 9
    print(solu.search(nums, target))


if __name__ == '__main__':
    main()