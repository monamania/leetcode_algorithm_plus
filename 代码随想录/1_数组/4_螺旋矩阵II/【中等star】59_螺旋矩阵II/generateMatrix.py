#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@ Author: CJK_Monomania
@ Data: 2021-12-21
"""

from typing import *
from pprint import pprint
import numpy as np
import pandas as pd
import sys   # sys.maxsize---int的最大值

'''
@ 题目: Python
@ 参考链接: 
int最大值: sys.maxsize
float最大值: float('inf')
'''
# Definition for singly-linked list.--链表
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

# https://leetcode-cn.com/problems/spiral-matrix-ii/submissions/
import numpy as np
class Solution:
    def generateMatrix(self, n: int) -> List[List[int]]:
        ans = np.zeros((n,n), dtype=np.int)   # 不能用
        ans = [[0] * n for _ in range(n)]
        count = 1
        startx = 0
        starty = 0
        offset = 1
        loop = n // 2   # 圈数，n为偶数时正好结束，n为奇数时需要额外进行一次处理
        mid = n // 2    # n为奇数时，需要进行处理的中间点下标
        while loop:
            i = startx
            j = starty
            # 上行填充
            while(j < n-offset):
                ans[i][j] = count
                count += 1
                j += 1
            # 右行填充
            while(i < n-offset):
                ans[i][j] = count
                count += 1
                i += 1
            # 下行填充
            while(j > starty):
                ans[i][j] = count
                count += 1
                j -= 1

            # 左行填充
            while(i > startx):
                ans[i][j] = count
                count += 1
                i -= 1

            loop -= 1
            startx += 1
            starty += 1
            offset += 1
             
        
        if n % 2:
            ans[mid][mid] = count

        return ans


def main():
    solu = Solution()
    n = 5
    print(solu.generateMatrix(n))


if __name__ == '__main__':
    main()