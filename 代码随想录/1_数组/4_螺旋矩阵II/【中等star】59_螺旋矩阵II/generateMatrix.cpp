/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include<numeric>  // 用于对vector求和----accumulate函数
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<T>& arr, string const str){
    cout << str << ": ";
    for(auto it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

/**
 * @brief 注意事项
 * 这里一圈下来，我们要画四条边，这四条边怎么画，、
 * 每画一条边都要坚持一致的左闭右开，或者左开又闭的原则。
 * 
 * 建议：统一进行左闭右开处理
 */
// 题目：https://leetcode-cn.com/problems/spiral-matrix-ii/
// 解析：https://programmercarl.com/0059.%E8%9E%BA%E6%97%8B%E7%9F%A9%E9%98%B5II.html#_59-%E8%9E%BA%E6%97%8B%E7%9F%A9%E9%98%B5ii
class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        int numOfMatrices = n*n;
        int loop = n / 2;  // 圈数，n为偶数时正好结束，n为奇数时需要额外进行一次处理
        int mid = n / 2; // n为奇数时，需要进行处理的中间点下标
        int startx = 0;
        int starty = 0;
        int count = 1; // 记录数值
        int offset = 1;  // 用于确保左闭右开
        int i,j;
        vector<vector<int>> ans(n, vector<int>(n, 0)); // n 行 n列值为0的二维数组
        while(loop--) {
            i = startx;
            j = starty;
            // 填充上行
            for(j=starty; j < n-offset; j++){
                ans[i][j] = count++;
            }
            // 填充右行
            for(i=startx; i < n-offset; i++){
                ans[i][j] = count++;
            }
            // 填充下行
            for(; j > starty; j--){
                ans[i][j] = count++;
            }

            // 填充左行
            for(; i > startx; i--){
                ans[i][j] = count++;
            }
            startx++;
            starty++;
            offset++;
        }

        // 如果n为ie奇数
        if(n % 2) {
            ans[mid][mid] = count;
        }
        return ans;

    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // 常见边界值
    // cout << INT32_MAX << endl;
    // cout << INT32_MIN << endl;
    // cout << INT_MAX << endl;
    // cout << INT_MIN << endl;
    int target = 7;
    vector<int> nums = {2,3,1,2,4,3};
    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    // cout << solu.minSubArrayLen(target, nums) << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}