#include<iostream>
#include <vector> 
// #define NDEBUG
#include <assert.h>
using namespace std;


#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int preNum;
        int len = 0;
        for (int i = 0; i < nums.size(); ++i){
            if (nums[i] != preNum){
                preNum = nums[i];
                nums[len++] = preNum;
            }
        }
        return len;
    }
};


int main(int argc, char** argv){
    vector<int> nums = {0,0,1,1,1,2,2,3,3,4};
    // vector<int> nums = {1,1,2};
    Solution solu = Solution();
    int len = solu.removeDuplicates(nums);
    for (int i = 0; i < len; ++i) {
        cout << nums[i] << endl;
    }
    return 0;
}