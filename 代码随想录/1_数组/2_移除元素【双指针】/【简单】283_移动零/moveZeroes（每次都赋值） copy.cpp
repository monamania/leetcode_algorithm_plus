/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int firstZero; // 记录0的位置
        int i;
        for (i = 0, firstZero=0; i < nums.size(); i++) {
            nums[firstZero] = nums[i];
            if (nums[i] != 0)
            {
                firstZero++;
            }
        }
        while (firstZero < nums.size())
        {
            nums[firstZero++] = 0;
        }
    }
};


int main(int argc, const char** argv) {
    Solution *solu = new Solution();
    vector<int> nums = {0,1,0,3,12};
    solu->moveZeroes(nums);
    for (int i = 0; i < nums.size(); i++){
        cout << nums[i] << endl;
    }

    free(solu);
    return 0;
}