#include<iostream>
#include<string>
#include<vector>
using namespace std;

class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int findNumIndex = 0, i = 0;
        int len = nums.size();
        for (i = 0; i < nums.size(); ++i){
            if (val != nums[i]){
                nums[findNumIndex] = nums[i];
                findNumIndex++;
            }
        }
        return findNumIndex;
    }
};




int main(int argc, char** argv) {
    Solution solu = Solution();
    vector<int> nums = {0,1,2,2,3,0,4,2};
    int val = 2;
    int len = nums.size();
    
    int relen = solu.removeElement(nums, val);
    for (int i = 0; i < relen; i++){
        cout << nums[i] << endl;
    }

    return 0;
}