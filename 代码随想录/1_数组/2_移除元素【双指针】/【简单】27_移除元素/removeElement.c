#include<stdio.h>
#include<stdlib.h>
// #define NDEBUG
#include <assert.h>


#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

int removeElement(int* nums, int numsSize, int val){
    int findNumIndex = 0, i = 0;
    int len = numsSize;
    for (i = 0; i < numsSize; ++i){
        if (val != nums[i]){
            nums[findNumIndex] = nums[i];
            findNumIndex++;
        }
    }
    return findNumIndex;

}


int main(int argc, char** argv){
    // int nums[] = {3,2,2,3};
    int nums[] = {0,1,2,2,3,0,4,2};
    // int nums[] = {1,1,2};
    int len = sizeof(nums)/sizeof(nums[0]);
    int val = 2;
    
    int relen = removeElement(nums, len, val);
    
    for (int i = 0; i < relen; ++i) {
        printf("%d ", nums[i]);
    }
    putchar('\n');
    return 0;
}
