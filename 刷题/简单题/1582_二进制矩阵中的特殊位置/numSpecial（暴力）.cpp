/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    int numSpecial(vector<vector<int>>& mat) {
        int nums = 0;
        vector<int> indexs;
        int i, j;
        for (i = 0; i < mat.size(); ++i)
        {
            int tmpflag = 0;
            int k = 0, l = 0;
            for (j = 0; j < mat[i].size(); ++j)
            {
                if (mat[i][j] == 1){
                    for (k = 0; k < mat.size(); ++k){
                        if (k==i)
                            continue;
                        if (mat[k][j] == 1)
                        {
                            break;
                        }
                    }
                    for (l = j + 1; l < mat[i].size(); ++l){
                        if (l==j)
                            continue;
                        if (mat[i][l] == 1){
                            break;
                        }
                    }
                    if (k==mat.size() && l == mat[i].size())
                    {
                        nums++;
                    }
                }
            }
        }
        return nums;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();


    return 0;
}