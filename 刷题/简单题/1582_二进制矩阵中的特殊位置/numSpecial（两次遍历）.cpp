/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<int>& arr, string const str){
    vector<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}


class Solution
{
public:
    int numSpecial(vector<vector<int>>& mat) {
        int nums = 0;
        int lenRow = mat.size();
        int lenCol = mat[0].size();
        vector<int> row(lenRow);
        vector<int> col(lenCol);

        // 把每行的值加起来
        for (int i = 0; i < lenRow; ++i){
            for (int j = 0; j < lenCol;++j){
                row[i] += mat[i][j];
                col[j] += mat[i][j];
            }
        }
        visit_arr(row, "row");
        visit_arr(col, "col");
        for (int i = 0; i < lenRow; ++i)
        {
            for (int j = 0; j < lenCol;++j){
                if(mat[i][j] == 1 && row[i] == 1 && col[j]==1){
                    nums++;
                }
            }
        }

        return nums;
    }
};

int main(int argc, const char** argv) {
    Solution solu = Solution();


    return 0;
}