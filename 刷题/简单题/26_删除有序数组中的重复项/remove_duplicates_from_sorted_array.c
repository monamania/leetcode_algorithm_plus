#include<stdio.h>
#include<stdlib.h>
// #define NDEBUG
#include <assert.h>


#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


int removeDuplicates(int* nums, int numsSize){
    int preNum;
    int len = 0;
    for (int i = 0; i < numsSize; ++i){
        if (nums[i] != preNum){
            preNum = nums[i];
            nums[len++] = preNum;
        }
    }
    return len;
}

int main(int argc, char** argv){
    // int nums[] = {0,0,1,1,1,2,2,3,3,4};
    int nums[] = {1, 1};
    // int nums[] = {1,1,2};
    int len = sizeof(nums)/sizeof(nums[0]);
    
    int relen = removeDuplicates(nums, len);
    for (int i = 0; i < relen; ++i) {
        printf("%d ", nums[i]);
    }
    putchar('\n');
    return 0;
}