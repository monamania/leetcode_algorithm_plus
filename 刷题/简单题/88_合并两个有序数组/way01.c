#include<stdio.h>

void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n){
    // int maxSize = nums1Size > nums2Size ? nums1Size : nums2Size;
    while(--nums1Size){
        if(!n || ( m && nums1[--m] >= nums2[--n])) nums1[nums1Size] = nums1[--m];
        else nums1[nums2Size] = nums2[--n];
    }
}
// void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n){
//     while(nums1Size--)
//         if(!n || (m && nums1[m - 1] >= nums2[n - 1])) nums1[nums1Size] = nums1[--m];
//         else nums1[nums1Size] = nums2[--n];
// }

int main(int argc, char** argv){
    int nums1[] = {1,2,3,0,0,0};
    int m = 3;
    int nums2[] = {2,5,6};
    int n = 3;
    merge(nums1,6,m,nums2,3,n);
    for (int i = 0; i <6;i++){
        printf("%d ", nums1[i]);
    }
    printf("\r\n");
    return 0;
}