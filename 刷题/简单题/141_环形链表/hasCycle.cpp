/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}


const int MAX_NUM = 10000;

// 利用快慢指针，若指针相遇，则有环
class Solution {
public:
    bool hasCycle(ListNode *head) {
        int count=0;
        // 链表为空或只有头结点，则不是环
        if (!head || !head->next) return false;
        ListNode* slow = head, *fast = head->next;
        while(fast){
            cout << count << ": "<< slow->val << " " << fast->val << endl;
            if (fast == slow && fast && slow) return true;
            count++;
            slow = slow->next;
            fast = fast->next ? fast->next->next: fast->next; 
            if (count>MAX_NUM || !fast || !slow) return false;
        }
        return false;
    }
};

int main(int argc, const char** argv) {
    Solution solu = Solution();
    ListNode *head;
    cout << solu.hasCycle(head) << endl;

    return 0;
}