/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<int>& arr, string const str){
    vector<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


// 圆形的周长为2*pi*r----数学解法
class Solution {
public:
    int orchestraLayout(int num, int xPos, int yPos) {
        long long circle = min({ xPos, num - 1 - xPos, yPos, num - 1 - yPos });
        long long seq = 4 * (num - circle) * circle;
        long long side = num - 2 * circle - 1, xVertex = circle, yVertex = circle;

        if (xPos == xVertex && yPos < yVertex + side) {
            seq += ((long long)yPos + 1 - yVertex);
        }
        else {
            seq += side;
            if (yPos == yVertex + side && xPos < xVertex + side) {
                seq += ((long long)xPos + 1 - xVertex);
            }
            else {
                seq += side;
                if (xPos == xVertex + side && yPos > yVertex) {
                    seq += (yVertex + side + 1 - yPos);
                }
                else {
                    seq += (side + xVertex + side + 1 - xPos);
                }
            }
        }

        seq %= 9;
        return (int)(seq == 0 ? 9 : seq);
    }
};


// 0层的数字共有(num - 1) * 4

// 1层的数字共有(num - 3) * 4 , 前两层(2 * num - 2 * 2) * 4

// 2层的数字共有(num - 5) * 4 , 前三层(3 * num - 3 * 3) * 4

// ...... 依此类推

// L层 开始的数字应该是 (L * num - L * L) * 4 + 1

// 再判断给定坐标是在四条边的哪一条边上
class Solution {
public:
    int orchestraLayout(int num, int xPos, int yPos) {
        //先找到它在第几层
        //计算出层开始时的编号
        //算出对应乐器编号
        long l = min(min(xPos, num - 1 - xPos), min(yPos, num - 1 - yPos));
        long pre = (num * l - l * l) * 4 % 9;
        long ans = 0;
        if(xPos == l){
            ans = pre + (yPos - l + 1);
            ans %= 9;
        }else if(num - 1 - yPos == l){
            ans = pre + num - (l * 2) - 1 + (xPos - l + 1);
            ans %= 9;
        }else if(num - 1 - xPos == l){
            ans = pre + (num - (l * 2) - 1) * 2 + (num - l - yPos);
            ans %= 9;
        }else{
            ans = pre + (num - (l * 2) - 1) * 3 + (num - l - xPos);
            ans %= 9;
        }
        return ans == 0 ? 9 : ans;
    }
};






int main(int argc, const char** argv) {
    Solution solu = Solution();
    int num = 3;
    int xPos = 0;
    int yPos = 3;


    return 0;
}