#include<iostream>
#include <vector>  
using namespace std;
/**
 * @brief vector容器，参考：https://www.w3cschool.cn/cpp/cpp-i6da2pq0.html
 * 
 */
class Solution {
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        int len = nums.size();
        for (int i = 0; i < len; ++i) {
            for (int j = i + 1; j < len; ++j) {
                if (nums[i] + nums[j] == target) {
                    return {i, j};
                }
            }
        }
        return {};
    }
};

int main(int argc, char** argv){
    vector<int> input = {3,2,4};
    int target = 6;
    Solution solut = Solution();
    vector<int> result;
    result = solut.twoSum(input, target);
    cout << result[0] << ", "<< result[1] << endl;

    return 0;
}