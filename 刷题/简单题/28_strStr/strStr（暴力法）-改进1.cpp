#include<iostream>
#include<string>
#include<vector>
using namespace std;


class Solution {
public:
    int strStr(string haystack, string needle) {
        int i,j;
        if (needle.size()==0) return 0;
        for(i = 0; i < haystack.size();++i){
            // 出现不等，或者haystack剩余长度超过needle长度，则停止
            for(j = 0; j < needle.size() && needle[j] == haystack[i+j] && haystack.size()-i+1 > needle.size();++j);
            if (j == needle.size())
                return i;
        }
        if (i == haystack.size()) return -1;
        else return i;
    }
};


int main(int argc, char** argv) {
    Solution solu = Solution();
    string haystack = "hello";
    string needle = "ll";
    // string haystack = "aaaaa";
    // string needle = "bba";
    // string haystack = "aaaaa";
    // string needle = "";

    
    int firstplace =  solu.strStr(haystack, needle);
    cout << firstplace << endl;

    return 0;
}