#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;



// class Solution {
// public:
//     vector<int> plusOne(vector<int>& digits) {
//         std::vector<int> ans; 
//         int carry = 1;
//         std::vector<int>::reverse_iterator  rit;//声明一个迭代器，来访问vector容器，作用：遍历或者指向vector容器的元素
//         rit=digits.rbegin();
//         while(carry != 0 || rit != digits.rend()) {
//             carry += *rit;
//             ans.push_back(carry%10);
//             if (rit == digits.rend()) break;
//             carry = carry/10;
//             rit++;
//         }
//         std::reverse(ans.begin(),ans.end());

//         return ans;
//     }
// };

// class Solution {
// public:
//     vector<int> plusOne(vector<int>& digits) {
//         vector<int> ans, tmp; 
//         int carry = 1;
//         int len = digits.size();
//         int i = len - 1;
//         while(carry != 0 || i >= 0) {
//             carry += digits[i];
//             tmp.push_back(carry%10);
//             if (i < 0) break;
//             carry = carry/10;
//             i--;
//         }
//         // reverse(ans.begin(),ans.end());  
//         for (int i = tmp.size()-1; i >= 0; i--)
//         {
//             ans.push_back(tmp[i]);
//         }
//         return ans;
//     }
// };

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        vector<int> ans, tmp; 
        int carry = 1;
        int len = digits.size();
        for( int i = len - 1; i >=0; i--) {
            carry += digits[i];
            tmp.push_back(carry%10);
            carry = carry/10;
        }
        if (carry != 0) {
            tmp.push_back(carry%10);
        }
        // reverse(ans.begin(),ans.end());  
        for (int i = tmp.size()-1; i >= 0; i--)
        {
            ans.push_back(tmp[i]);
        }
        return ans;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    vector<int> num = vector<int>({9});
    vector<int>::iterator it;//声明一个迭代器，来访问vector容器，作用：遍历或者指向vector容器的元素 
    num = solu.plusOne(num);
    for(it=num.begin();it!=num.end();it++)
    {
        cout<<*it<<" ";
    }

    return 0;
}

