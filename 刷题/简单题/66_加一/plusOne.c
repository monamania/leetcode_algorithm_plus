#include <stdio.h>
#include <stdlib.h>
// #define NDEBUG
#include <assert.h>


#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// void swap(SElemType *a,SElemType *b){
//     SElemType tmp = *a;
//     *a = *b;
//     *b = tmp;
// }
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* plusOne(int* digits, int digitsSize, int* returnSize){
    int tmp[digitsSize+1]; 
    // int *tmp = (int*)malloc(digitsSize*sizeof(int));  //leetcode有问题
    int carry = 1;
    int k = 0;
    for (int i = digitsSize-1; i >= 0; i--){
        carry += digits[i];
        tmp[k++] = carry%10;
        carry = carry/10;
    }
    if (carry != 0) {
        tmp[k++] = carry%10;
    }
    // 逆序
    int j=0;
    int *ans = (int*)malloc(k*sizeof(int));
    for (int i = k-1; i >= 0; i--)
    {
        ans[j++] = tmp[i];
    }
    *returnSize = k;
    return ans;
}

int main(int argc, char** argv)
{
    int arr[] = {9};
    int len = (int)sizeof(arr)/sizeof(arr[0]);
    int endlen = 0;
    int *b = plusOne(arr, len,&endlen);
    for(int i = 0; i < endlen; i++){
        printf("%d ", b[i]);
    }
    
}