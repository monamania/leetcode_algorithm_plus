/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0 ) return false;
        return (reverse(x) == x);
    }
    // 整数反转
    int reverse(int x) {
        long long ret=0;  // int 4个字节max=0x7fffffff,min=0
        for(;x;ret=ret*10+x%10,x/=10);
        // return ret=tmp>max||tmp<min?0:tmp;
        return (int)ret==ret? (int)ret:0;
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    int x = 10;
    bool flag = solu.isPalindrome(x);
    cout << flag << endl;

    return 0;
}