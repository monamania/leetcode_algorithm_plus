#include<iostream>
#include<string>
#include<vector>
using namespace std;

// 越界处理

// class Solution {
// public:
//     long long reverse(int x) {
//         long long ans = 0;
//         while (x != 0){
//             ans *= 10;
//             ans += x%10;
//             x /= 10; 
//         }
//         // cout << ans << endl;
//         // return ans;
//         // 超过范围了 
//         return (int)ans==ans? (int)ans:0;
//     }
// };

class Solution {
public:
    long long reverse(int x) {
        long long ans = 0;

        for(;x;ans=ans*10+x%10,x/=10);
        // cout << ans << endl;
        // return ans;
        // 超过范围了 
        return (int)ans==ans? (int)ans:0;
    }
};


int main(void) {
    int x1 = 123;
    int x2 = -123;
    int x3 = 120;
    int x4 = 1534236469;
    long long ans = 0;
    Solution *solu = new Solution();
    ans = solu->reverse(x2);
    cout << ans << endl;

    free(solu);
    return 0;   
}