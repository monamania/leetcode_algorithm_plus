//https://leetcode-cn.com/problems/valid-palindrome/
/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<int>& arr, string const str){
    vector<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    bool isPalindrome(string s) {
        // tolower(s); // 转换为小写
        // if (s.length()==1) return isalnum(s[0]) || s[0]==' ';
        if (s.length()==1) return true;
        int start = 0;
        int end = s.length()-1;
        while(start < end) {
            for(;!isalnum(s[start]) && start < end; ++start);
            for(;!isalnum(s[end]) && start < end; --end);
            if(isalpha(s[start])) s[start] = tolower(s[start]);
            if(isalpha(s[end])) s[end] = tolower(s[end]);
            cout << start << " " << s[start] << endl;
            cout << end << " " << s[end] << endl;
            if (s[start] != s[end]) return false; 
            else{
                start++;
                end--;
            }
        }
        return true;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s = "A man, a plan, a canal: Panama";
    // string s = "race a car";
    string s = " ";
    cout << std::boolalpha <<solu.isPalindrome(s) << endl;

    return 0;
}