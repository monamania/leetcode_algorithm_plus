/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */



class Solution {
public:
    int getMinDistance(vector<int>& nums, int target, int start) {
        for (int i = 0; i < nums.size(); i++) {
            if (start - i >= 0) {
                if (nums[start - i] == target){
                    // cout << "in " << i << endl;
                    // return abs(-i);
                    return i;
                }
            }
            if ( (start + i < nums.size()) && nums[start + i] == target){
                // cout << "out " << i << endl;
                return i;
            }
        }
        return -1;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // vector<int> nums = {1,2,3,4,5};
    // vector<int> nums = {1,1,1,1,1,1,1,1,1,1};
    // vector<int> nums = {5,3,6};
    vector<int> nums = {1538,7994,465,6387,7091,9953,35,7298,4364,3749};

    int target = 1538;
    int start = 9;
    cout << solu.getMinDistance(nums, target, start) << endl;

    return 0;
}