/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


/**
 * 递归
 * 递归出口：当前节点为空，返回''
 * 如果左右子树都为空，则只返回当前数的值
 * 只有左子树为空，返回 3()(2) 的形式
 * 只有右子树为空，返回 3(2)的形式
 * 左右子树都不为空，返回 3(2)(4) 的形式
*/
class Solution {
public:
    string ans;
    string tree2str(TreeNode *root){
        if (!root) return "";
        // 只允许左子树为空时用()替代节点，如果右子树为空或者左右子树都为空就不要瞎折腾了
        if (!root->left && !root->right)
            return to_string(root->val);
        if (!root->left)
            return to_string(root->val) + "()(" + tree2str(root->right)+")";
        if (!root->right)
            return to_string(root->val) + "(" +  tree2str(root->left) + ")";
        return to_string(root->val) + "(" +  tree2str(root->left) + ")" + "(" + tree2str(root->right)+")";
    }
};

int main(int argc, const char** argv) {
    Solution solu = Solution();


    return 0;
}