/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <algorithm> // std::minmax_element
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


class Solution {
public:
    int lengthOfLastWord(string s) {
        // 去除后空格
        // s.erase(std::find_if(s.rbegin(), s.rend(),
        // std::not1(std::ptr_fun(::isspace))).base(),s.end());
        // 去除后空格
        // s.erase(s.find_last_not_of(" ") + 1);
        
        int cnt = 0;
        for (int i = s.find_last_not_of(" "); i>=0 && s[i] != ' ';i--){
            cnt++;
        }
        return cnt;
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s =  "   fly me   to   the moon  ";
    string s =  "a";
    // cout << s.find_last_not_of(" ") << endl;
    // cout << s[s.find_last_not_of(" ")]<< endl;
    cout << solu.lengthOfLastWord(s) << endl;

    return 0;
}