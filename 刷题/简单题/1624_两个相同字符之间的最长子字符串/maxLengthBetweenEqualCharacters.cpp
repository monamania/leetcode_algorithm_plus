/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    int maxLengthBetweenEqualCharacters(string s) {
        int i,j;
        int maxLength = -1;
        int startIndex=0;
        int endIndex=0;
        for (i = s.length()-1; i >= 0; i--) {
            cout << "============= "<< endl;
            cout <<"i: "<< i << s[i] << endl;
            for(j = 0; j < i-1 && s[i]!=s[j]; j++)
            {
                cout <<"j="<< j << s[j] << " ";
            };
            cout << endl;
            if (s[i]==s[j]) {
                if ((i-j-1) > maxLength){
                    maxLength = (i-j-1);
                    startIndex = j;
                    endIndex = i;
                }
            };
        }
        if (maxLength < 0)
            return -1;
        return maxLength;
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s = "abca";
    // string s = "cbzxy";
    // string s = "cabbac";
    // string s = "ojdncpvyneq";
    string s = "mgntdygtxrvxjnwksqhxuxtrv";
    cout << solu.maxLengthBetweenEqualCharacters(s) << endl;

    return 0;
}