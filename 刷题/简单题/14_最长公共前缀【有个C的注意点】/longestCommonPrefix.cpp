/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <map>
// #define NDEBUG
#include <assert.h>
#include <math.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF -99999
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status;
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

class Solution
{
public:
    string longestCommonPrefix(vector<string> &strs)
    {
        if (strs.size() <= 1)
            return strs[0];

        string prefix = strs[0];
        int endPrefixNmb = prefix.size(); // 前缀字符个数
        int i, j;
        for (i = 1; i < strs.size(); ++i)
        {
            // cout << endPrefixNmb << endl;
            endPrefixNmb = min(endPrefixNmb, (int)strs[i].size());
            for (j = 0; j < endPrefixNmb && prefix[j] == strs[i][j]; ++j);
            endPrefixNmb = min(j, endPrefixNmb);
        }
        // cout << endPrefixNmb << endl;
        // cout << prefix << endl;
        if (endPrefixNmb <= 0)
            return "";
        else
            return prefix.substr(0, endPrefixNmb);
    }
};

int main(int argc, const char **argv)
{
    Solution solu = Solution();
    // vector<string> strs = {"flower", "flow", "flight"};
    vector<string> strs = {"ab", "a"};
    // vector<string> strs = {"flower","flower","flower","flower"};
    cout << solu.longestCommonPrefix(strs) << endl;

    return 0;
}