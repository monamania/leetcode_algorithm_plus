/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<stdio.h>
#include<stdlib.h>
// #define NDEBUG
#include <assert.h>
#include <string.h>
#include <math.h>


#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF -99999
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status;
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

int min(int numA, int numB){
    return numA < numB ? numA : numB;
}

// char * longestCommonPrefix(char ** strs, int strsSize){
//     if (strsSize<=1) return strs[0];

//     char* prefix = strs[0];
//     printf("%s: ", prefix);
//     int endPrefixNmb = strlen(prefix);  // 前缀字符个数
//     int i,j;
//     for (i = 1; i < strsSize; ++i)
//     {
//         // cout << endPrefixNmb << endl;
//         endPrefixNmb = min(endPrefixNmb, (int)strlen(strs[i]));
//         for(j = 0; j < endPrefixNmb && prefix[j] == strs[i][j]; ++j);
//         endPrefixNmb = min(j, endPrefixNmb);
//     }
//     // cout << endPrefixNmb << endl;
//     // cout << prefix << endl;
//     if (endPrefixNmb <= 0) return "";
//     else 
//     {
//         char *ans = "";
//         for(int i=0; i < endPrefixNmb; i++)
//         {
//             ans[i] = prefix[i];
//         }
//         ans[endPrefixNmb] = '\0';
//         // return prefix.substr(0, endPrefixNmb);
//         return ans;
//     }


// }

char * longestCommonPrefix(char ** strs, int strsSize){
    if(strsSize == 0)                           //输入不存在公共前缀
        return "";
    printf("%s\r\n", strs[0]);

    for(int i=0; i < strlen(strs[0]); i++){     //纵向遍历字符串数组
        for(int j=0; j < strsSize; j++){
            if(strs[j][i] != strs[0][i]){       //与的一个字符串同列字符不相同
                strs[0][i] = '\0';              //将第一个字符串从该列结束
                return strs[0];                 //返回的一个字符串
            }
        }
    }

    return strs[0];                             //全部符合则第一个字符串即为最长公共前缀
}




int main(int argc, const char **argv)
{
    // char** strs = {"flower", "flow", "flight"};
    // char** strs = {"ab", "a"};
    char* strs[] = {"ab", "a"};  // 必须这样写。我也不知道为啥
    // char** strs = {"flower","flower","flower","flower"};
    int strsSize = sizeof(strs)/sizeof(strs[0]);

    // int rows=sizeof(strs)/sizeof(strs[0]);
    // int cols=sizeof(strs[1])/sizeof(strs[0][0]);
    // printf("%d rows, %d cols", rows, cols);

    char* ans = longestCommonPrefix(strs, strsSize);
    // printf("%s\r\n", ans);

    return 0;
}