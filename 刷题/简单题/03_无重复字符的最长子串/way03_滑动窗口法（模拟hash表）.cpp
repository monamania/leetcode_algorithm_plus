#include <iostream>
#include <vector>
// #define NDEBUG
#include <assert.h>
#include <string>
using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        vector<int> m(128, 0);  // 创建一个大小为128的容器，值都为0。因为ASCII码最大为128
        int maxLen = 0;
        int left = 0;   // 就类似start，end
        for (int right = 0; right < s.size(); right++) {
            left = max(left, m[s[right]]);
            m[s[right]] = right + 1; // 记录出现过字符的下标位置
            maxLen = max(maxLen, right - left + 1);
        }
        return maxLen;
    }
};



int main(int argc, char **argv)
{
    // string s = "au";
    string s = "abcabcbb";
    // string s = "";
    Solution solu;
    int result = 0;
    result = solu.lengthOfLongestSubstring(s);
    cout << "result: " << result << endl;

    // system("pause");
    return 0;
}