/**
 * @file filename.cpp
 * @brief 法1：朴素法
 *          string 用法：https://blog.csdn.net/tengfei461807914
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include <iostream>
#include <vector>
// #define NDEBUG
#include <assert.h>
#include <string>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF -99999
#define MAXSIZE 10000 /* 存储空间初始分配量 */

typedef int Status;
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

class Solution
{
public:
    int lengthOfLongestSubstring(string s)
    {
        
        int result = 1;
        int len = s.length();
        char tmp[MAXSIZE];
        if (s.empty())
            return 0;
        for (int i = 0; i < len; ++i){
            int cnt = 0;
            
            int k = 0;
            tmp[cnt++] = s[i];
            for (int j = i+1; j < len; ++j)
            {
                // cout<<j<<" "<< s[j]<<endl;
                int m=0;
                for (m=0; m < cnt; ++m)
                {
                    if (tmp[m] == s[j])
                        break;
                }
                if (m != cnt)//找到相同的元素了
                {
                    break;
                }
                tmp[cnt++] = s[j];
                
            }
            result = result>cnt ? result : cnt;
        }
        return result;
    }
};

int main(int argc, char **argv)
{
    // string s = "au";
    string s = "abcabcbb";
    // string s = "";
    Solution solu;
    int result = 0;
    result = solu.lengthOfLongestSubstring(s);
    cout << "result: " << result << endl;

    // system("pause");
    return 0;
}