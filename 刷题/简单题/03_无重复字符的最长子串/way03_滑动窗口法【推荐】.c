#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int lengthOfLongestSubstring(char* s) {
    int right, left;
    int max = 0, tmp = 1;
    if (s == NULL)
        return 0;
    int len = strlen(s);
    if (len == 1)
        return 1;
    for ( right = 1; right < len; ++right) {
        for (left = right - tmp; left < right; ++left ) {
            if (*(s+right) == *(s+left)) {
                tmp = right - left;
                break;
            }
        }
        if (right == left)
            tmp++;
        max = (max > tmp) ? max : tmp;
    }
    return max;
}

int main(){
    // string s = "au";
    char* s = "abcabcbb";
    // string s = "";

    int result = 0;
    result = lengthOfLongestSubstring(s);
    printf("result: %d", result);

    // system("pause");
    return 0;
}

