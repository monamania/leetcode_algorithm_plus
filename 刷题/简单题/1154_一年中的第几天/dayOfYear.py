#!/usr/bin/env python3

class Solution(object):
    mouth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    def dayOfYear(self, date)->int:
        """
        :type date: str
        :rtype: int
        """
        y,m,d = map(int, date.split('-'))
        # print(y,m,d)
        if y%400 == 0 or (y%4==0 and y%100!=0):
            self.mouth[2] += 1
        return sum(self.mouth[:m]) + d

def main():
    solu = Solution()
    date = "2015-04-20"
    day = solu.dayOfYear(date)
    print(str(day))

if __name__ == '__main__':
    main()
