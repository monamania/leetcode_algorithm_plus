/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};



class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
        if (l1 == nullptr) return l2;
        if (l2 == nullptr) return l1;
        ListNode* dummyHead = new ListNode(0); // 虚拟头节点---新的链表
        ListNode* pre = dummyHead;
        while(l1 && l2) {
            if (l1->val < l2->val) {
                pre->next = l1;
                l1 = l1->next;
            } else {
                pre->next = l2;
                l2 = l2->next;
            }
            pre = pre->next;
        }
        // 剩下的元素直接接到最后面
        pre->next = ((l1 == nullptr) ? l2 : l1);
        return dummyHead->next;
    }
};


int main(int argc, char** argv){
    ListNode *L1 = new ListNode(2);
    L1->next = new ListNode(4);
    L1->next->next = new ListNode(3);

    ListNode *L2 = new ListNode(5);
    L2->next = new ListNode(6);
    L2->next->next = new ListNode(4);

    Solution solu;
    
    // system("pause");
    return 0;
}