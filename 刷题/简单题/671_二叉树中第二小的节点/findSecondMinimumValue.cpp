/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

/**解题思路：
 * 由于根节点一定是最小的
 */
class Solution {
public:

    int findSecondMinimumValue(TreeNode *root)
    {
        if (!root->left || !root->right || !root)
            return -1;
        return myfun(root, root->val);
    }
    int myfun(TreeNode *root, int val){
        if (!root)
            return -1;
        if (root->val > val) {
            return root->val;
        }
        int l = myfun(root->left, val);
        int r = myfun(root->right, val);
        // 如果左右两边都比根节点的值大的，则选择其中最小的
        if (l > val && r > val) {
            return min(l,r);
        }
        return max(l,r);
    }
};

int main(int argc, const char** argv) {
    Solution solu = Solution();
    TreeNode *root = new TreeNode(2);
    root->left = new TreeNode(2);
    root->right = new TreeNode(5, new TreeNode(5),new TreeNode(7));


    return 0;
}