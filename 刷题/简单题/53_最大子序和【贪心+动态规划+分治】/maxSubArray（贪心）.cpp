/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


/**
 * 核心思想：
 * 若当前指针所指元素之前的和小于0，则丢弃当前元素之前的序列
 * 
 * 从左向右迭代，一个个数字加过去，如果sum<0，重新开始找子序串
 */

class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        // if (nums.size() <= 0) return -1;
        int cur_sum = nums[0];
        int max_sum = nums[0];
        for (int i = 1; i < nums.size(); ++i){
            cur_sum = max(cur_sum+nums[i], nums[i]);    // 若当前指针所指元素之前的和小于0，则丢弃当前元素之前的序列
            max_sum = max(max_sum, cur_sum);
        }
        return max_sum;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    vector<int> nums = {-2,1,-3,4,-1,2,1,-5,4};
    
    cout << solu.maxSubArray(nums) << endl;

    return 0;
}