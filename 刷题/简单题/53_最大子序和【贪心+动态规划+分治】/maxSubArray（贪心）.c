#include <stdio.h>

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

/**
 * 核心思想：
 * 若当前指针所指元素之前的和小于0，则丢弃当前元素之前的序列
 * 
 * 从左向右迭代，一个个数字加过去，如果sum<0，重新开始找子序串
 */



// SElemType max_element(SElemType * arr, int arrSize)
// {
//     SElemType maxElement = INF;
//     for (int i = 0; i < arrSize; ++i){
//         maxElement = maxElement > arr[i] ? maxElement : arr[i];
//     }
//     return maxElement;
// }

SElemType max(SElemType a, SElemType b){
    return a>b?a:b;
}

int maxSubArray(int* nums, int numsSize)
{

    int i;
    SElemType cur_sum=nums[0], max_sum=nums[0];
    for (i = 1; i < numsSize; i++)
    {
        cur_sum = max((cur_sum + nums[i]), nums[i]);
        max_sum = max(cur_sum, max_sum);
    }
    return max_sum;
}


int main(int argc, char** argv){
    SElemType nums[] = {-2,1,-3,4,-1,2,1,-5,4};
    int numsSize = sizeof(nums)/sizeof(nums[0]);
    int ans = INF;
    ans = maxSubArray(nums, numsSize);

    printf("%d\n", ans);

    return 0;
}