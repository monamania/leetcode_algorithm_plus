/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <queue> 
#include <stack>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace boost;   // 支持string的操作
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

// 遍历
template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit_arr(vector<T>& arr, string const str){
    cout << str << ": ";
    for(auto it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}

// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2_arr(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



// 链表结点
struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

// 二叉树--孩子兄弟表示法
//Definition for a binary tree node.// 二叉树--孩子兄弟表示法
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};


class Solution {
public:
    int longestPalindrome(string s) {
        map<char,int> hash_map;
        for (int i = 0; i < s.length();++i) {
            hash_map[s[i]]++;
        }

        // 找出出现次数为偶数的求和，再找出所有奇数减一求和。因为奇数是可以拆成偶数使用的
        int ans = 0;
        for (auto it = hash_map.begin();it!=hash_map.end();++it) {
            ans += (it->second)/2*2; // 每次出现的次数，是奇数->剪一次再拿，是偶数直接拿，简练点的代码就是 v / 2 * 2 

            // 如果ans是偶数---->则最中间可以插入一个，那么假如有一个数是奇数，就可以拆分出来一个
            if ((it->second)%2 == 1 && ans%2 == 0)
                ans++;
        }
        return ans;   
    }
};

// class Solution {
//     public int longestPalindrome(String s) {
//         int count[] = new int[128];
//         for (char c : s.toCharArray()) {
//             count[c]++;
//         }
//         int ans = 0;
//         for (int v : count) {
//             ans += v / 2 * 2;
//             if (v % 2 == 1 && ans % 2 == 0) {
//                 ans++;
//             }
//         }
//         return ans;
//     }
// }


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // 常见边界值
    // cout << INT32_MAX << endl;
    // cout << INT32_MIN << endl;
    // cout << INT_MAX << endl;
    // cout << INT_MIN << endl;
    string s = "abccccdd";
    time_t start = time(NULL);
    cout << "start time is " << start << endl;

    cout << solu.longestPalindrome(s) << endl;
    
    time_t end = time(NULL);
    cout << "end time is " << start << endl;
    cout << "耗时：" << end*100-start*100 << " s." <<endl;


    return 0;
}