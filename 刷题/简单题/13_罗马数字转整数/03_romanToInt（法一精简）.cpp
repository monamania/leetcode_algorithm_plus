/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

/**
 * 核心思维：
 * 对字符串从左到右遍历，
 * 如果当前字符代表的值不小于其右边，就加上该值；
 * 否则就减去该值。以此类推到最左边的数，最终得到的结果即是答案
 * 
 */

class Solution {
public:
    int romanToInt(string s) {
        string::iterator it = s.begin();
        int sum = 0;
        int preNum = getData(*it);  // 前一个数

        for(it = s.begin()+1 ; it != s.end(); it++) {
            int num = getData(*it);
            if (preNum >= num) {
                sum += preNum;
            }
            else{
                sum -= preNum;
            }
            preNum = num;
        }
        // 要加上最后的一个数
        sum += preNum;
        return sum;
    }

    int getData(char ch){
        switch(ch){
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D': return 500;
            case 'M': return 1000;
            default:  return 0;
        }
    }
};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string str = "LVIII";  // 58
    string str = "MCMXCIV";   // 1994
    
    cout << solu.romanToInt(str) << endl;

    return 0;
}