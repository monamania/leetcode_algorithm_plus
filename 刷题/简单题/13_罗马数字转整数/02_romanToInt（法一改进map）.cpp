/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

/**
 * 核心思维：
 * 对字符串从左到右遍历，
 * 如果当前字符代表的值不小于其右边，就加上该值；
 * 否则就减去该值。以此类推到最左边的数，最终得到的结果即是答案
 * 
 */

// map<char,int> datampa = {
//     {'I',1},
//     {'V',5},
//     {'X',10},
//     {'L',50},
//     {'C',100},
//     {'D',500},
//     {'M',1000}
// };



class Solution {
public:
    int romanToInt(string s) {
        string::iterator it;
        int tmp = 0;
        int sum = 0;
        for(it = s.begin() ; it != s.end(); it++) {
            tmp = datampa[*it];
            if (*it == 'I' && (*(it+1) == 'V' || (*(it+1) == 'X')))
                tmp = -1*tmp;
            else if (*it == 'X' && (*(it+1) == 'L' || (*(it+1) == 'C')))
                tmp = -1*tmp;
            else if (*it == 'C' && (*(it+1) == 'D' || (*(it+1) == 'M')))
                tmp = -1*tmp;
            
            sum += tmp;
        }
        return sum;
    }

    // map<char,int> datampa;
private:
    map<char,int> datampa = {
        {'I',1},
        {'V',5},
        {'X',10},
        {'L',50},
        {'C',100},
        {'D',500},
        {'M',1000}
    };


    // Solution(){
    //     // 构造函数
    //     datampa['I'] = 1;
    //     datampa['V'] = 5;
    //     datampa['X'] = 10;
    //     datampa['L'] = 50;
    //     datampa['C'] = 100;
    //     datampa['D'] = 500;
    //     datampa['M'] = 1000;

    // }

};



int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string str = "LVIII";  // 58
    string str = "MCMXCIV";   // 1994
    
    cout << solu.romanToInt(str) << endl;

    return 0;
}