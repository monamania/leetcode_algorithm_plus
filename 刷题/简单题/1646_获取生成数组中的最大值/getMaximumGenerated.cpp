/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};


class Solution {
public:
    int getMaximumGenerated(int n) {
        int max = INF;
        vector<int> arr = {0,1};
        if(n<2) return arr[n];
        for (int i = 2; i <= n; i++){
            int tmp = i%2 == 0 ? arr[i/2] : arr[i/2] + arr[i/2+1];
            arr.push_back(tmp);
            max = max > tmp ? max : tmp;
        }
        return max;
    }
};




int main(int argc, const char** argv) {
    Solution solu = Solution();
    int n = 3;
    cout << solu.getMaximumGenerated(n) << endl;

    return 0;
}