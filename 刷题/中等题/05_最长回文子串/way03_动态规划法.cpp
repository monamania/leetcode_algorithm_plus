#include<iostream>
#include<string>
#include<vector>
using namespace std;

/**
 * @brief 利用动态规划的思想解决回文子串的问题
 * 1 dp[i][j]---即dp[left][right]的状态来表示子串s[i-j]是否为回文子串
 * 2 得到状态转方程：dp[left][right] = (s[left] == s[right]) && d[left+1][right-1]
 * 3 边界条件：right-1 - (left+1) + 1 < 2, 整理得 right-left < 3
 * 4 初始化：dp[i][i] = true
 * 5 输出：在得到一个状态的值为true的时候，记录起始位置和长度，填表完成以后再截取
 * 
 * 动态规划4步曲：（1）确定状态【最后一步、化为子问题】；（2）确定转移方程；（3）确定开始和边界条件；（4）计算顺序
 * 
 */


class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.size();
        if (n < 2) {
            return s;
        }

        int maxLen = 1;
        int begin = 0;
        // dp[i][j] 表示 s[i..j] 是否是回文串
        vector<vector<int>> dp(n, vector<int>(n));
        // 初始化：所有长度为 1 的子串都是回文串
        for (int i = 0; i < n; i++) {
            dp[i][i] = true;
        }
        // 递推开始
        // 先枚举子串长度
        for (int L = 2; L <= n; L++) {
            // 枚举左边界，左边界的上限设置可以宽松一些
            for (int i = 0; i < n; i++) {
                // 由 L 和 i 可以确定右边界，即 j - i + 1 = L 得
                int j = L + i - 1;
                // 如果右边界越界，就可以退出当前循环
                if (j >= n) {
                    break;
                }

                if (s[i] != s[j]) {
                    dp[i][j] = false;
                } else {
                    if (j - i < 3) {
                        dp[i][j] = true;
                    } else {
                        dp[i][j] = dp[i + 1][j - 1];
                    }
                }

                // 只要 dp[i][L] == true 成立，就表示子串 s[i..L] 是回文，此时记录回文长度和起始位置
                if (dp[i][j] && j - i + 1 > maxLen) {
                    maxLen = j - i + 1;
                    begin = i;
                }
            }
        }
        return s.substr(begin, maxLen);
    }
};





int main(int argc, char** argv) {
    string str = string("babad");
    // string str = string("abc");
    // string str = string("cbbd");
    // string str = string("baabad");
    string ans = string("");
    Solution solu = Solution();
    
    ans = solu.longestPalindrome(str);
    cout << ans<< endl;

    return 0;
}