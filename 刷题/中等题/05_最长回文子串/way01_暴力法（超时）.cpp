#include<iostream>
#include<string>
using namespace std;

class Solution {
public:
    string longestPalindrome(string s) {
        int start,end,k;
        int len = s.length();
        int maxLen = 1;
        int tmp = 0;
        int ans = 0;
        if (len==0 || len==1) return s;
        for (start = 0; start < len;start++){
            for(end = start+1; end < len; end++){
                tmp = end-start+1;
                for (k = 0; k < tmp/2;k++) {
                    if (s[start+k] != s[end-k]){
                        break;
                    }
                }
                if (k == tmp/2) 
                {
                    if (maxLen < tmp)
                    {
                        maxLen = tmp;
                        ans = start;
                    }
                }
            }
        }
        cout<< "start: " << ans << ", nums: " << maxLen << endl;
        return s.substr(ans, maxLen);
    }
};

int main(int argc, char** argv) {
    // string str = string("babad");
    string str = string("abc");
    // string str = string("cbbd");
    // string str = string("baabad");
    string ans = string("");
    Solution solu = Solution();
    
    ans = solu.longestPalindrome(str);
    cout << ans<< endl;

    return 0;
}