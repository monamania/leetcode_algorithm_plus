#include<iostream>
#include<string>
using namespace std;

class Solution {
public:
    int left = 0;
    int right = 0;
    int maxLength = 0;
    string longestPalindrome(string s) {
        int result = 0;
        for (int i = 0; i < s.size(); i++) {
            extend(s, i, i, s.size()); // 以i为中心
            extend(s, i, i + 1, s.size()); // 以i和i+1为中心
        }
        return s.substr(left, maxLength);
    }
    void extend(const string& s, int i, int j, int len) {
        while (i >= 0 && j < len && s[i] == s[j]) {
            if (j - i + 1 > maxLength) {
                left = i;
                right = j;
                maxLength = j - i + 1;
            }
            i--;
            j++;
        }
    }
};

int main(int argc, char** argv) {
    // string str = string("babad");
    string str = string("abc");
    // string str = string("cbbd");
    // string str = string("baabad");
    string ans = string("");
    Solution solu = Solution();
    
    ans = solu.longestPalindrome(str);
    cout << ans<< endl;

    return 0;
}