#include<iostream>
#include <vector>  
using namespace std;
/**
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * 
 */
#define OK 1
#define ERROR 0
typedef int Status;
typedef int ElemType;

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};


/**
 * @brief 解题思路---模拟法
 * 按照真正的加法进行
 */
class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* dummyHead = new ListNode(0); // 虚拟头节点，记得进位信息
        ListNode* result = dummyHead;
        int carry = 0; // 进位信息
        while(l1 || l2 || carry){
            carry += (l1 ? l1->val : 0) + (l2 ? l2->val : 0);
            dummyHead->next = new ListNode(carry%10);
            dummyHead = dummyHead->next;
            if (l1) l1 = l1->next;
            if (l2) l2 = l2->next;
            carry = carry/10;
        }
        return result->next;
    }
};


int main(int argc, char** argv){
    ListNode *L1 = new ListNode(2);
    L1->next = new ListNode(4);
    L1->next->next = new ListNode(3);

    ListNode *L2 = new ListNode(5);
    L2->next = new ListNode(6);
    L2->next->next = new ListNode(4);

    Solution solu;
    ListNode *result = solu.addTwoNumbers(L1, L2);
    // cout << result->val << " ";
    while(result)
    {
        cout << result->val << " ";
        result = result->next;
    }
    cout << endl;

    return 0;
}