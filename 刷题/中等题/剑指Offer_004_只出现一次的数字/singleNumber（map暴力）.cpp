/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

void visit_list(ListNode* head){
    ListNode* tmp = head;
    while(tmp != nullptr){
        cout << tmp->val << ' ';
        tmp = tmp->next;
    }
    cout << endl;
}

class Solution {
public:
    int singleNumber(vector<int>& nums) {
        map<int, int> num_id_map;
        for (int i = 0; i < nums.size(); i++) {
            if(num_id_map.find(nums[i]) == num_id_map.end())
            {
                num_id_map[nums[i]] = 0;
            }
            else{
                num_id_map[nums[i]]++;
            }
        }
        map<int, int>::iterator it;
        for (it = num_id_map.begin(); it != num_id_map.end(); ++it)
        {
        // cout << *it << " ";
            int key = it->first;
            char value = it->second;
            if (value == 0)
            return key;
        }
        return 0;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // vector<int> nums = {2,2,3,2};
    // vector<int> nums = {0,1,0,1,0,1,100};
    vector<int> nums = {30000,500,100,30000,100,30000,100};
    cout << solu.singleNumber(nums) << endl;

    return 0;
}