/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <algorithm> // std::minmax_element
// #include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        vector<vector<int>> answer;
        int len = nums.size();
        if (len<3)
            return answer;
        // 为了去重
        sort(nums.begin(), nums.end());

        for (int i = 0; i < len-2; i++){
            // 去重
            if (i > 0 && nums[i] == nums[i - 1]) {
                // cout << "in" << endl;
                continue;
            }
            // cout << i << endl;
            int tmp_target = -1 * nums[i];
            for(int j = i+1; j < len-1; ++j)
            {
                if (answer.size()>0 && nums[j] == nums[j - 1]){
                    continue;
                }
                for(int k=j+1; k < len; ++k)
                {
                    if (answer.size()>0 && nums[k] == nums[k - 1]){
                        continue;
                    }
                    if(tmp_target==nums[j] + nums[k])
                    {
                        vector<int> tmp;
                        tmp.push_back(nums[i]);
                        tmp.push_back(nums[j]);
                        tmp.push_back(nums[k]);
                        answer.push_back(tmp);
                    }
                }
            }
        }
        return answer;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // vector<int> nums = {-1,0,1,2,-1,-4};
    // vector<int> nums = {0, 0, 0, 0};
    vector<int> nums = {-1, 0, 1, 0};
    // vector<int> nums = {};
    // vector<int> nums = {0};
    vector<vector<int>> answer;
    answer = solu.threeSum(nums);
    for(int i = 0; i < answer.size(); ++i){
        for(int j = 0; j < answer[i].size(); ++j){
            cout << answer[i][j] << " ";
        }
        cout<<endl;
    }

    return 0;
}