/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include <sstream>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */



class Solution {
public:
    int myAtoi(string s) {
        long long ans=0, i=0,j=0;
        int negFlag = 1;   // 正负标记，默认为正
        for(i=0 ; i<s.size() && tranData(s[i]) == -2; i++);
        // cout << i << endl;
        negFlag = tranData(s[i]) == '-'? -1 : 1;
        if (tranData(s[i]) == '-'){
            negFlag = -1;
            i++;
        }
        else if(tranData(s[i]) == '+'){
            i++;
        }
        // cout << i << endl;

        while(i<s.size()){
            if (tranData(s[i]) < 0 || tranData(s[i]) > 9 || (int)ans != ans)
                break;
            ans *= 10;
            ans += tranData(s[i]);
            i++;
        }
        ans = negFlag * ans;
        if ((int)ans == ans)
        {
            return ans;
        }
        // return negFlag > 0 ? (int)((1 << 31) - 1): (int)(1 << 31);
        return negFlag < 0 ? -2147483648: 2147483647;
    }


    int tranData(char ch){
            if (ch >=48 && ch <=57)
                return (int)(ch - 48);
            else if (ch == '+' || ch == '-')
                return ch;
            else if (ch == ' ')  // 空格
                return -2;
            else
                return -1;
    }
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    // string s = "words and 987";
    // string s = "-91283472332";
    // string s = "21474836460";
    string s = " -4193 with words";
    

    // cout << (int)(1 << 31) << endl;
    // cout << ((1 << 31) - 1) << endl;
    // cout << (-1 * (1 << 31) - 1) << endl;
    cout << solu.myAtoi(s) << endl;
    return 0;
}

