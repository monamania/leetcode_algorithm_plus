/**
 * @file filename.cpp
 * @brief nullptr关键字用于标识空指针，与NULL不同，NULL为0
 * @author Monomania (1301519350@qq.com)
 * @version 0.1
 * @date 2021-09-23
 */
#include<iostream>
#include <vector> 
#include <string>
#include <list>
#include <map>
#include <algorithm> // std::minmax_element
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include <sstream>
// #define NDEBUG
#include <assert.h>
using namespace std;

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */



class Solution {
public:
    int myAtoi(string s) {
        stringstream ss(s);   // 初始化stringstream
        cout << ss.str() << endl;
        int x = 0; ss >> x;   // 把stringstream的内容输出给int类型的 x
        return x;
    }

    // int in
};


int main(int argc, const char** argv) {
    Solution solu = Solution();
    string s = " -4193 with words";
    
    cout << solu.myAtoi(s) << endl;
    return 0;
}

