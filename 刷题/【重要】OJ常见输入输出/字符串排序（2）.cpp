#include<iostream>
using namespace std;

// https://ac.nowcoder.com/acm/contest/5657/I
/**
 * @brief 描述
 * 多个测试用例，每个测试用例一行。
 * 每行通过空格隔开，有n个字符，n＜100
 * 
 * 输出：对于每组测试用例，输出一行排序过的字符串，每个字符串通过空格隔开
 * 
 * 示例1
 * 输入
 * a c bb
 * f dddd
 * nowcoder
 * 输出：
 * a bb c
 * dddd f
 * nowcoder
 */
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<algorithm>
int main(){
    string s;
    vector<string> ss;
    while(cin>>s){
        //if(s==""){
        //    return 0;
        //}
        ss.push_back(s);
        if(cin.get()=='\n'){
            sort(ss.begin(),ss.end());
            for(int i=0;i<ss.size()-1;i++){
            cout<<ss[i];
            cout<<' ';
        }
        cout<<ss[ss.size()-1]<<endl;
        ss.clear();
        }
        
    }
}