#include<iostream>
using namespace std;

// https://ac.nowcoder.com/acm/contest/5657/D
/**
 * @brief 描述
 * 输入：
 * 输入数据包括多组。
 * 每组数据一行,每行的第一个整数为整数的个数n(1 <= n <= 100), n为0的时候结束输入。
 * 接下来n个正整数,即需要求和的每个正整数。
 * 
 * 输出：每组数据输出求和的结果
 * 
 */

int main(){
    int n,a;
    int sum=0;
    while(cin >> n) {
        if(n==0) return 0;
        while(n--){
            cin>>a;
            sum+=a;
        }
        cout << sum << endl;
        sum=0;
    }
    return 0;
}
