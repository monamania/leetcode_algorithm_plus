#include<iostream>
using namespace std;

// https://ac.nowcoder.com/acm/contest/5657/B
/**
 * @brief 描述
 * 输入：
 * 第一行输入一个数据组个数t
 * 接下来两个整数a，b（[1,1000]），输入多组
 * 输出：a+b的结果
 * 
 */

int main(){
    int t,a,b;
    cin >> t;
    while(t--) {
        cin >> a >> b;
        cout << a+b << endl;
    }
    return 0;
}
