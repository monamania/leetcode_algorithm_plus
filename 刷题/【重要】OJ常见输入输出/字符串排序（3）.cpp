#include<iostream>
using namespace std;

// https://ac.nowcoder.com/acm/contest/5657#question
/**
 * @brief 描述
 * 多个测试用例，每个测试用例一行。
 * 每行通过空格隔开，有n个字符，n＜100
 * 
 * 输出：对于每组用例输出一行排序后的字符串，用','隔开，无结尾空格
 * 
 * 示例1
 * 输入
 * a c bb
 * f dddd
 * nowcoder
 * 输出：
 * a bb c
 * dddd f
 * nowcoder
 */
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>

using namespace std;
int main(void) {
    string line;
    while(getline(cin, line)) {
        stringstream tmpstringsdf(line);
        string word;
        vector<string> words;
        while(getline(tmpstringsdf, word, ',')){
            words.push_back(word);
        }
        line.clear();
        
        sort(words.begin(), words.end());
        for(int i = 1; i < words.size(); ++i)
            cout << words[i - 1] << ",";
        cout << words.back() << endl;
    }
    return 0;
}