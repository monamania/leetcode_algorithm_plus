#include<iostream>
using namespace std;

// https://www.nowcoder.com/questionTerminal/93bc96f6d19f4795a4b893ee16e97654
/**
 * @brief 描述
 * 输入：两个整数a，b（[1,1000]），输入多组
 * 输出：a+b的结果
 * 
 */

int main(){
    int a,b;
    while(cin >> a >> b) {
        cout << a+b << endl;
    }
    return 0;
}
