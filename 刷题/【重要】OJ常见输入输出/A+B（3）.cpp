#include<iostream>
using namespace std;

// https://ac.nowcoder.com/acm/contest/5657/C
/**
 * @brief 描述
 * 输入：
 * 输入包括两个正整数a,b(1 <= a, b <= 10^9),输入数据有多组, 如果输入为0 0则结束输入
 * 
 * 输出：a+b的结果
 * 
 */

int main(){
    int a,b;
    while(cin >> a >> b) {
        if(a==0&&b==0) return 0;
        cout << a+b << endl;
    }
    return 0;
}
