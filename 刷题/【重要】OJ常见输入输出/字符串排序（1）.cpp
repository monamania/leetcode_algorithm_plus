#include<iostream>
using namespace std;

// https://ac.nowcoder.com/acm/contest/5657/H
/**
 * @brief 描述
 * 输入有两行，第一行n
 * 第二行是n个字符串，字符串之间用空格隔开
 * 
 * 输出：输出一行排序后的字符串，空格隔开，无结尾空格
 * 
 * 示例1
 * 输入
 * 5
 * c d a bb e
 * 输出：
 * a bb c d e
 */

#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<algorithm>


int main(){
    int n;
    cin>>n;
    vector<string> ss;
    while(n--){
        string s;
        cin>>s;
        ss.push_back(s);
    }
    sort(ss.begin(),ss.end());
    for(int i=0;i<ss.size()-1;i++){
        cout<<ss[i];
        cout<<' ';
    }
    cout<<ss[ss.size()-1]<<endl;
}