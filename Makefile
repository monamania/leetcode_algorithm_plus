#main: main.o input.o calcu.o
#	gcc -o main main.o input.o calcu.o

#"#"表示的是注释
#Makefile 变量的使用

###############################################
objects = problem01.o
main: $(objects)
	gcc -o main $(objects)


%.o:%.c    #模式规则
	gcc -c $<    #自动化变量


.PHONY : clean
clean:
	rm *.o
	rm main



