[toc]

# 一、认识shell

## 1. `tty1~tty7`

- tty代表电传打字机（`teletypewriter`）
- 可以通过`Ctrl+Alt + (F1~F7)`来选择要进入的虚拟控制台
  - Ubuntu使用F7进去图形界面
  - RHEL使用F1进入图形界面（可自行尝试）
- 文本界面虚拟控制台修改文字与背景色
  - 反转使能指令：`setterm -inversescreen on  `--->背景变为白色，字体变为黑色
  - 背景颜色设置：`setterm -background white`
  - 字体颜色设置：`setterm -foreground black`
  - 共有8种颜色：`black，red，green，yellow，blue，magenta，cyan和white`    

![image-20210804162358566](https://gitee.com/monamania/picBed/raw/master/img/20210804162557.png)



## 2. 一些shell命令行

### 2.1 监测程序 

#### 探查进程ps

- 显示所有进程

   - Unix风格：`ps -ef`
   
   ```
     UID：启动这些进程的用户。
     PID：进程的进程ID。
     PPID：父进程的进程号（如果该进程是由另一个进程启动的）。
     C：进程生命周期中的CPU利用率。
     STIME：进程启动时的系统时间。
     TTY：进程启动时的终端设备。
     TIME：运行进程需要的累计CPU时间。
     CMD：启动的程序名称。
   ```

- `-l`参数产生一个长格式输出

  ```
     F：内核分配给进程的系统标记。
     S：进程的状态（ O代表正在运行； S代表在休眠； R代表可运行，正等待运行； Z代表僵
    化，进程已结束但父进程已不存在； T代表停止）。
     PRI：进程的优先级（越大的数字代表越低的优先级）。
     NI：谦让度值用来参与决定优先级。
     ADDR：进程的内存地址。
     SZ：假如进程被换出，所需交换空间的大致大小。
     WCHAN：进程休眠的内核函数的地址。
  ```

- 显示进程的层级信息，并用ASCII字符绘出图表：`--forest`

![image-20210804200429585](https://gitee.com/monamania/picBed/raw/master/img/20210804200441.png)

#### 实时检测进程top

#### 结束进程

- 强制结束进程：
  - `kill -9 PID`
  - `kill -s HUP PID`
- 通过进程名而不是PID结束进程【支持通配符，root时慎用】
  - `killall http*`

![image-20210804200752078](https://gitee.com/monamania/picBed2/raw/master/img/image-20210804200752078.png)

### 2.2 监测磁盘空间

#### 挂载存储媒体

**mount**

- 手动挂载媒体的基本命令：`mount -t type device directory`

  - type包含：vfat、ntfs、iso9660等文件系统类型
    - vfat：Windows长文件系统
    - ntfs：Windows NT、XP、Vista以及Windows 7中使用
    - iso9660：标准CD-ROM文件系统
  - 大多数U盘和软盘为vfat，CD则必须为iso9660
  - 例：`mount -t vfat /dev/sdbl /media/disk`

  ![image-20210804202434450](https://gitee.com/monamania/picBed/raw/master/img/20210804202455.png)

![image-20210804202451136](https://gitee.com/monamania/picBed/raw/master/img/20210804202457.png)

**umount**

从Linux系统上移除一个可移动设备时，不能直接从系统上移除，而应该先卸载。  

```
umount [directory | device ]
```

#### df命令（disk free）-- 发现哪个磁盘空间快没了

- 通过文件系统来快速获取空间大小的信息，当我们删除一个文件的时候，这个文件不是马上就在文件系统当中消失了，而是暂时消失了，当所有程序都不用时，才会根据OS的规则释放掉已经删除的文件， df记录的是通过文件系统获取到的文件的大小，他比du强的地方就是能够看到已经删除的文件，而且计算大小的时候，把这一部分的空间也加上了，更精确了。

- `df -h`：按照易读的形式显示

#### du命令（disk usage）-- 显示某个特定目录的使用情况

通过搜索文件来计算每个文件的大小然后累加，du能看到的文件只是一些当前存在的，没有被删除的。他计算的大小就是当前他认为存在的所有文件大小的累加和。

```
参数说明：
-c：显示所有已列出文件的总大小
-h：易读的方式罗列
-s：显示每个输出参数的总数
```

### 2.3 处理数据文件

#### 排序数据sort

#### 搜索数据grep



## 3. Linux环境变量

### 3.1 定位系统环境变量

登入Linux系统<u>启动一个bash shell</u>时，默认情况下bash会在几个文件中查找命令。这些文件叫作**启动文件**或环境文件  

- 启动bash shell的方式
  - 登录时作为默认登录shell
  - 作为非登录shell的交互式shell
  - 作为运行脚本的非交互式shell

#### 启动文件

【五大启动文件运行顺序】：`(主启动文件)/etc/profile`、`$HOME/.bash_profile`、`$HOME/.bashrc`、`$HOME/.bash_login`、`$HOME/.profile`

可以通过配置这些启动文件，实现开机启动程序的功能；

==环境变量`BASH_ENV`用来查看要执行的启动文件（默认情况下没有被设置）==

- ==**环境变量持久化**==
  - 对于所有用户都需要使用的变量：在 `/etc/profile.d`目录下创建一个`.sh`文件，把所有新的或修改的变量设置放这里
  - 存储个人需要用的永久性变量：修改`$HOME/.bashrc`文件即可（如`alias`命令）



### 3.2 数组变量（不常用）

- 定义

```sh
 mytest=(one two three four five)
```

- 获取数组变量（索引从0开始）

  - 默认得到索引为0的值

  ```sh
  $ echo $mytest
  one
  ```

  - 获取任意一个

  ```sh
  $ echo ${mytest[2]}
  three
  ```

  - 获取全部

  ```sh
  $ echo ${mytest[*]}
  one two three four five
  ```

- 修改

```sh
$ mytest[2]=seven
$
$ echo ${mytest[*]}
one two seven four five
```

- 删除（`unset`）

  - 删除某个值【注意删除后，原有索引位为空，但还是占着位（占着茅坑不拉屎）】

  ```sh
  $ unset mytest[2]
  $
  $ echo ${mytest[*]}
  one two four five
  $
  $ echo ${mytest[2]}
  $ echo ${mytest[3]}
  four
  ```

  - 删除整个数组

  ```sh
  $ unset mytest
  $
  $ echo ${mytest[*]}
  ```





# 二、shell脚本编程基础

> 能用双引号的地方，尽量使用双引号，可以避免很多空格之类的问题
>
> - 单引号：忽略所有
> - 双引号：变量等依旧有效，如$

## 1. 显示消息echo

**默认换行显示**

```shell
##test1.sh
#!/bin/bash
# This script displays the date 
echo The time and date are:
date

#-----------测试-------------#
$ ./test1
The time and date are:
Mon Feb 21 15:41:13 EST 2014
```

**-n参数**：把文本字符串和命令输出显示在同一行中【去掉末尾的换行符】

```shell
#!/bin/bash
# This script displays the date
echo -n "The time and date are: "
date

#-----------测试-------------#
$ ./test1
The time and date are: Mon Feb 21 15:42:23 EST 2014
```

**-e参数：** 处理特殊字符，类似print

若字符串中出现以下字符，则特别加以处理，而不会将它当成一般文字输出：

| 转义字符 | 描述                               |
| -------- | ---------------------------------- |
| \a       | 发出警告声                         |
| \b       | 删除前一个字符                     |
| \c       | 最后不加上换行符号                 |
| \f       | 换行但光标仍旧停留在原来的位置     |
| \n       | 换行且光标移动至行首               |
| \r       | 光标移动至行首，但不换行           |
| \t       | 插入tab                            |
| \v       | 换行但光标仍旧停留在原来的位置     |
| `\\`     | 插入`\`                            |
| `\nnn`   | 插入nnn（八进制）所代表的ASCII字符 |

```sh
$echo -e "a\bdddd"
dddd12
$echo -e "a\adddd" //输出同时会发出报警声音
adddd12

$echo -e "a\ndddd" //自动换行
a
dddd
```



## 2. 脚本内浮点计算

可以采用内建的bash计算器（bc）进行【非浮点运算时，可以使用`$[ 运算式 ]`的方式】

> 使用方括号时，两边都要有空格！！

- 较短的运算

```sh
#!/bin/bash
# 其中scale在bc中表示小数点的位数
var1=100
var2=45
var3=$(echo "scale=4; $var1 / $var2" | bc)
echo The answer for this is $var3


#-----------测试-------------#
$ chmod u+x test9
$ ./test9
The answer is 2.2222
```

- 较长的运算：采用内联重定向的方式

```sh
$ cat test12
#!/bin/bash
var1=10.46
var2=43.67
var3=33.2
var4=71
var5=$(bc << EOF
scale = 4
a1 = ( $var1 * $var2)
b1 = ($var3 * $var4)
a1 + b1
EOF
)
echo The final answer for this mess is $var5
```

## 3. 判断语句

### 3.1 if-then-else语句

if后面紧跟着命令，若命令的退出状态码为0，则位于then部分的命令将被执行

```sh
if command
then
	commands
else
	commands
fi

#或
if command; then
	commands
fi

```

- 嵌套，`elif`语句

```sh
if command1
then
	commands
elif command2
then
	more commands
fi
```

### 3.2 test命令【数字比较、字符串比较、==文件比较==】

```sh
test condition
condition是test命令要测试的一系列参数和值

#----使用----#
if test condition	 
then
	commands
fi
# 等价于
if [ condition ]
then
	commands
fi
```

- 测试变量是否存在

```sh
#!/bin/bash
# Testing the test command
#
my_variable=""
#
if test $my_variable
then
	echo "The $my_variable expression returns a True"
#
else
	echo "The $my_variable expression returns a False"
fi
```

- 数值比较【注意shell脚本==只能处理整数==，浮点数会出错】

|    比较     |          描述          |
| :---------: | :--------------------: |
| `n1 -eq n2` | 检查`n1`是否与`n2`相等 |
| `n1 -ge n2` |        大于等于        |
| `n1 -gt n2` |          大于          |
| `n1 -le n2` |        小于等于        |
| `n1 -lt n2` |          小于          |
| `n1 -ne n2` |          不等          |

- 字符串比较

  `> 和 <`比较进行转义，不然会被认为是输出重定向 或者 输入重定向

|      比较      |            描述            |
| :------------: | :------------------------: |
| `str1 = str2`  | 检查`str1`是否与`str2`相等 |
| `str1 != str2` |            不等            |
| `str1 \< str2` |            小于            |
| `str1 \> str2` |            大于            |
|   `-n str1`    |  检查`str1`的长度是否非0   |
|   `-z str1`    |  检查`str1`的长度是否为0   |

比较大小按照ASCII码，大写字母比小写字母小【与sort正好相反】

> 比较测试中使用的是标准的ASCII顺序，根据每个字符的ASCII数值来决定排序结果。 `sort`命令使用的是系统的本地化语言设置中定义的排序顺序。对于英语，本地化设置指定了在排序顺序中小写字母出现在大写字母前  

- **==文件比较==**

|       比较        |                   描述                   |
| :---------------: | :--------------------------------------: |
|     `-d file`     |         file是否存在并为一个目录         |
|     `-e file`     |      file是否存在（不论文件或目录）      |
|     `-f file`     |         file是否存在并为一个文件         |
|     `-r file`     |            file是否存在并可读            |
|     `-s file`     |            file是否存在并非空            |
|     `-w file`     |            file是否存在并可写            |
|     `-x file`     |           file是否存在并可执行           |
| `-O file`（大写） |       file是否存在并属当前用户所有       |
|     `-G file`     | file是否存在并且**默认组**与当前用户相同 |
| `file1 -nt file2` |          `file1`是否比`file2`新          |
| `file1 -ot file2` |          `file1`是否比`file2`旧          |

### 3.3 &&与||复合条件测试

注意这是在if-then-else中使用的。与命令行模式的&&、||不同

```sh
[ condition1 ] && [ condition2 ]  # condition1与condition2都为True才行
[ condition1 ] || [ condition2 ]  
```

```sh
#!/bin/bash
# testing compound comparisons
#
if [ -d $HOME ] && [ -w $HOME/testing ]
then
	echo "The file exists and you can write to it"
else
	echo "I cannot write to the file"
fi
```



> 命令行中的&&与||
>
> **一、&&运算符:**
>
> 如下：&&左边的命令（命令1）返回真(即返回0，成功被执行）后，&&右边的命令（命令2）才能够被执行；换句话说，“如果这个命令执行成功&&那么执行这个命令”。
>
> command1 && command2
>
> 语法格式如下：
>
> command1 && command2 [&& command3 ...]
>
> 详细说明：
>
> 1 命令之间使用 && 连接，实现逻辑与的功能。
>
> 2 只有在 && 左边的命令返回真（命令返回值 $? == 0），&& 右边的命令才会被执行。
>
> 3 只要有一个命令返回假（命令返回值 $? == 1），后面的命令就不会被执行。
>
> **二、||运算符:**
>
> 如下：||则与&&相反。如果||左边的命令（命令1）未执行成功，那么就执行||右边的命令（命令2）；或者换句话说，“如果这个命令执行失败了||那么就执行这个命令。
>
> command1 || command2
>
> 1 命令之间使用 || 连接，实现逻辑或的功能。
>
> 2 只有在 || 左边的命令返回假（命令返回值 $? == 1），|| 右边的命令才会被执行。
>
> 3 只要有一个命令返回真（命令返回值 $? == 0），后面的命令就不会被执行。
>
> **三、";" 运算符:**
>
> 为了在当前shell中执行一组命令，可以用命令分隔符(即";")隔开每一个命令，并可以所有的命令用圆括号()括起来。
>
> 它的一般形式为：
>
> （命令；命令；命令… ）
>
> 1 一条命令需要独占一个物理行，如果需要将多条命令放在同一行，命令之间使用命令分隔符";"分隔。执行的效果等同于多个独立的命令单独执行的效果。
>
> 2 () 表示在当前 shell 中将多个命令作为一个整体执行。需要注意的是，使用 () 括起来的命令在执行前面都不会切换当前工作目录，**也就是说命令组合都是在当前工作目录下被执行的，尽管命令中有切换目录的命令。**

### 3.4 if-then的高级特性【双括号与双方括号】

双括号：允许在比较过程中使用高级数学表达式

双方括号：用于高级字符串处理

- **双括号**

|  符号   |  描述  |
| :-----: | :----: |
| `val++` |  后增  |
| `val--` |  后减  |
| `++val` |  先增  |
| `--val` |  先减  |
|  `**`   | 幂运算 |
|  `<<`   | 左位移 |
|  `>>`   | 右位移 |
|   `~`   | 按位反 |
|   `&`   | 按位与 |
|   `|`   | 按位或 |
|   `!`   | 逻辑反 |
|  `&&`   | 逻辑与 |
|  `||`   | 逻辑或 |

```sh
$ cat test23.sh
#!/bin/bash
# using double parenthesis
#
val1=10
#
if (( $val1 ** 2 > 90 ))
then
	(( val2 = $val1 ** 2 ))
	echo "The square of $val1 is $val2"
fi

#-----------测试-------------#
$ ./test23.sh
The square of 10 is 100
$
```

==注意，不需要将双括号中表达式里的大于号转义。这是双括号命令提供的另一个高级特性==

- **双方括号**

可以直接定义一个正则表达式来匹配字符串值

```sh
$ cat test24.sh
#!/bin/bash
# using pattern matching
#
if [[ $USER == r* ]]
then
	echo "Hello $USER"
else
	echo "Sorry, I do not know you"
fi

#-----------测试-------------#
$ ./test24.sh
Hello rich
$
```

### 3.5 case命令

注意：两个`;;`作为结尾

```sh
case variable in
pattern1 | pattern2) commands1;;
pattern3) commands2;;
*) default commands;;
esac
```

```sh
$ cat test26.sh
#!/bin/bash
# using the case command
#
case $USER in
rich | barbara)
    echo "Welcome, $USER"
    echo "Please enjoy your visit";;
testing)
    echo "Special testing account";;
jessica)
    echo "Do not forget to log off when you're done";;
*)
    echo "Sorry, you are not allowed here";;
esac

#-----------测试-------------#
$ ./test26.sh
Welcome, rich
Please enjoy your visit
$
```



## 4. 循环语句

### 4.1 for命令【分隔符可改】

基本格式【for循环假定每个值都是用空格分割的  】

```sh
for var in list
do
    commands
done
```

例子

```sh
$ cat test1
#!/bin/bash
# basic for command

for test in Alabama Alaska Arizona Arkansas California Colorado
do
	echo The next state is $test
done
```

- 从变量中读取列表

```sh
$ cat test4
#!/bin/bash
# using a variable to hold the list

list="Alabama Alaska Arizona Arkansas Colorado"
#列表末尾再加一个值
list=$list" Connecticut"
for state in $list
do
	echo "Have you ever visited $state?"
done
```

- 从命令读取值

```sh
$ cat test5
#!/bin/bash
# reading values from a file

file="states"

for state in $(cat $file)
do
	echo "Visit beautiful $state"
done

$ cat states
Alabama
Alaska
Arizona
Arkansas
Colorado
Connecticut
Delaware
Florida
Georgia
#-----------测试-------------#
$ ./test5
Visit beautiful Alabama
Visit beautiful Alaska
Visit beautiful Arizona
Visit beautiful Arkansas
Visit beautiful Colorado
Visit beautiful Connecticut
Visit beautiful Delaware
Visit beautiful Florida
Visit beautiful Georgia
```

- **更改字段分隔符**【==`IFS=$'\n'`==，注意$表示的是行尾，不是取变量符】
  - 默认情况下的分隔符
    - 空格
    - 制表符
    - 换行符
  - 导致在处理含有空格的数据时，会比较麻烦
  - 可以通过在脚本中临时更改`IFS`环境变量的值在改变
  - `IFS=:;"$'\n'`  表示将换行符、冒号、分号和双引号作为字段分隔符  

```sh
$ cat test5b
#!/bin/bash
# reading values from a file

file="states"

#先保存旧的IFS值--即默认值
IFS.OLD=$IFS
#设置新的IFS值，其中$表示行尾
IFS=$'\n'
for state in $(cat $file)
do
	echo "Visit beautiful $state"
done
IFS=$IFS.OLD

#-----------测试-------------#
$ ./test5b
Visit beautiful Alabama
Visit beautiful Alaska
Visit beautiful Arizona
Visit beautiful Arkansas
Visit beautiful Colorado
Visit beautiful Connecticut
Visit beautiful Delaware
Visit beautiful Florida
Visit beautiful Georgia
Visit beautiful New York
Visit beautiful New Hampshire
Visit beautiful North Carolina
```

- 用通配符读取目录  

```sh
$ cat test6
#!/bin/bash
# iterate through all the files in a directory

for file in /home/rich/test/*
do
    if [ -d "$file" ]
    then
    	echo "$file is a directory"
    elif [ -f "$file" ]
    then
    	echo "$file is a file"
    fi
done
#-----------测试-------------#
$ ./test6
/home/rich/test/dir1 is a directory
/home/rich/test/myprog.c is a file
/home/rich/test/myprog is a file
/home/rich/test/myscript is a file
/home/rich/test/newdir is a directory
/home/rich/test/newfile is a file
/home/rich/test/newfile2 is a file
/home/rich/test/testdir is a directory
/home/rich/test/testing is a file
/home/rich/test/testprog is a file
/home/rich/test/testprog.c is a file
$
```

> 注意，应该将$file变量用双引号圈起来。如果不这么做，遇到含有空格的目录名或文件名时就会有错误产生  



### 4.2 C语言风格的for命令

```sh
for (( a = 1; a < 10; a++ ))
```

>  变量赋值可以有空格；
>  条件中的变量不以美元符开头；（就是中间的部分）
>  迭代过程的算式未用expr命令格式。  

例子

```sh
$ cat test8
#!/bin/bash
# testing the C-style for loop

for (( i=1; i <= 5; i++ ))
do
	echo "The next number is $i"
done
#-----------测试-------------#
$ ./test8
The next number is 1
The next number is 2
The next number is 3
The next number is 4
The next number is 5
$
```

- 使用多个变量
  - 注意，尽管可以使用多个变量，但只能定义一种条件

```sh
$ cat test9
#!/bin/bash
# multiple variables
for (( a=1, b=10; a <= 10; a++, b-- ))
do
	echo "$a - $b"
done
#-----------测试-------------#
$ ./test9
1 - 10
2 - 9
3 - 8
4 - 7
5 - 6
6 - 5
7 - 4
8 - 3
9 - 2
10 - 1
$
```

### 4.3 while命令

```sh
while test command
do
	other commands
done
```

例子

```sh
$ cat test10
#!/bin/bash
# while command test

var1=10
while [ $var1 -gt 0 ]
do
    echo $var1
    var1=$[ $var1 - 1 ]
done
```

注意：当有多个测试命令时，只有最后一个测试命令（类似运行条件）的退出状态码会被用来决定什么时候结束循环  



### 4.4 until命令

until命令和while命令工作的方式完全相反。 until命令要求你指定一个通常返回非零退出状态码的测试命令；只有测试命令的退出状态码不为0， bash shell才会执行循环中列出的命令。一旦测试命令返回了退出状态码0，循环就结束了。  

```sh
until test command
do
	other commands
done
```

例子

```sh
$ cat test12
#!/bin/bash
# using the until command

var1=100
until [ $var1 -eq 0 ]
do
    echo $var1
    var1=$[ $var1 - 25 ]
done
```



### 4.5 break和continue

**break**

- 直接使用break时跟C语言相同，表示跳出内部循环

- 跳出外部循环：
  - `break n`
  - 其中n默认为1表示跳出当前循环
  - n为2表示，跳出下一级的外部循环【外部和内部一起停止】

例子

```sh
$ cat test20
#!/bin/bash
# breaking out of an outer loop

for (( a = 1; a < 4; a++ ))
do
    echo "Outer loop: $a"
    for (( b = 1; b < 100; b++ ))
    do
        if [ $b -gt 4 ]
        then
            break 2
        fi
        echo " Inner loop: $b"
    done
done
#-----------测试-------------#
$ ./test20
Outer loop: 1
    Inner loop: 1
    Inner loop: 2
    Inner loop: 3
    Inner loop: 4
当执行了break命令后，外部和内部一起停止
```

**continue**

- 直接使用continue时跟C语言相同，表示跳出内部循环

- continue命令也允许通过命令行参数指定要继续执行哪一级循环：
  - `continue n`  

例子

```sh
$ cat test22
#!/bin/bash
# continuing an outer loop
	
for (( a = 1; a <= 5; a++ ))
do
    echo "Iteration $a:"
    for (( b = 1; b < 3; b++ ))
    do
        if [ $a -gt 2 ] && [ $a -lt 4 ]
        # 即值为3的时候
        then
        	continue 2
        fi
        var3=$[ $a * $b ]
        echo " The result of $a * $b is $var3"
	done
done
#-----------测试-------------#
$ ./test22
Iteration 1:
The result of 1 * 1 is 1
The result of 1 * 2 is 2
Iteration 2:
The result of 2 * 1 is 2
The result of 2 * 2 is 4
Iteration 3:
Iteration 4:
The result of 4 * 1 is 4
The result of 4 * 2 is 8
Iteration 5:
The result of 5 * 1 is 5
The result of 5 * 2 is 10
$
```



### 4.6 通过done处理循环的输出

- 可对输出使用管道或进行重定向

```sh
for file in /home/rich/*
do
    if [ -d "$file" ]
    then
        echo "$file is a directory"
    elif
        echo "$file is a file"
    fi
done > output.txt
#shell会将for命令的结果重定向到文件output.txt中，而不是显示在屏幕上
```

> 同理，要将文件从外部读入循环体中，则使用输入重定向即可【即`<`】，见后面的实例5.2



### 实例

#### 查找可执行文件

```sh
$ cat test25.sh
#!/bin/bash
# finding files in the PATH

#更改字段分隔符
IFS=:
for folder in $PATH
do
    echo "$folder:"
    for file in $folder/*
    do
        if [ -x $file ]
        then
        	echo " $file"
        fi
    done
done
$
```

####  创建多个用户账户

- 将需要创建的新用户ID和用户全名，放在一个.csv文件中；【格式：`userid,user name`】

- 使用while与read结合的方式读取文件的各行
  - `while IFS=’,’ read –r userid name  `
  - read命令会自动读取.csv文本文件的下一行
  - 当read命令返回FALSE时（读到文末），while命令自动退出

- **要想把数据从文件中送入while命令，只需在while命令尾部使用一个重定向符就可以了**  

users.csv文件如下

```sh
$ cat users.csv
rich,Richard Blum
christine,Christine Bresnahan
barbara,Barbara Blum
tim,Timothy Bresnahan
$
```

实例

```sh
$ cat test26
#!/bin/bash
# process new user accounts

input="users.csv"
while IFS=',' read -r userid name
do
    echo "adding $userid"
    useradd -c "$name" -m $userid
done < "$input"
$
```



## 5. 处理用户输入

### 5.1 命令行参数（位置参数）

> 在使用参数前，一定要检查其中是否存在数据
>
> 法一： ==`if [ -n "$1" ]  `==
>
> 法二：使用`$#`获取参数个数

- **位置参数**
  - `$0`：表示程序名
  - `$1`：表示第一个参数
  - 一直到`$9`
  - 大于9的参数，需要加上花括号。如：`${10}`
  - ==`$#`==表示参数个数

> 注意，要在参数值中包含空格，一定要记得用单引号或双引号

例子

```sh
$ cat test3.sh
#!/bin/bash
# testing string parameters
#
echo Hello $1, glad to meet you.
$

#-----------测试-------------#
$ ./test3.sh "Rich Blum"
Hello Rich Blum, glad to meet you.
$
```

- **读取脚本名**：==`name=$(basename $0)`==
  - 由于`$0`会根据执行时的方式，返回脚本名
  - 比如：`./test5.sh`  时`$0为./test5.s`，`bash /home/Christine/test5.sh `时`$0为/home/Christine/test5.sh`

==为了把路径剥离，采用`basename`命令==  

例子

```sh
$ cat test5b.sh
#!/bin/bash
# Using basename with the $0 parameter
#
name=$(basename $0)
echo
echo The script name is: $name
#

#-----------测试1-------------#
$ bash /home/Christine/test5b.sh
The script name is: test5b.sh
$
#-----------测试2-------------#
$ ./test5b.sh
The script name is: test5b.sh
$
```

- **特殊参数**
  - ==`$#`==：获取参数个数（不包括本身）
  - `${!#}  `：获取最后一个参数（当没有参数的时候，返回脚本名）
  - `$*`：获取所有命令行参数（不包括本身）【将所有参数当成一个单词保存，视为一个整体】
  - ==`$@`：获取所有命令行参数（不包括本身）【将所有参数当成同一个字符串中的多个独立单词】==

实例

```sh
$ cat test12.sh
#!/bin/bash
# testing $* and $@
#
echo
count=1
#
for param in "$*"
do
    echo "\$* Parameter #$count = $param"
    count=$[ $count + 1 ]
done
#
echo
count=1
#
for param in "$@"
do
    echo "\$@ Parameter #$count = $param"
    count=$[ $count + 1 ]
done

#-----------测试-------------#
$ ./test12.sh rich barbara katie jessica
$* Parameter #1 = rich barbara katie jessica
$@ Parameter #1 = rich
$@ Parameter #2 = barbara
$@ Parameter #3 = katie
$@ Parameter #4 = jessica
$
```

- **移动变量**：`shift n`命令
  - 当不知道有多少个变量时，可以只操作第一个变量，移动参数后，继续操作
  - 默认n为1

在使用shift命令时，默认情况下它会将每个参数变量向左移动一个位置。所以，变量`$3`的值会移到​`$2`中，变量`$2`的值会移到`$1`中，而变量​`$1`的值则会被删除（注意，变量​`$0`的值，也
就是程序名，不会改变）。  

```sh
$ cat test13.sh
#!/bin/bash
# demonstrating the shift command
echo
count=1
while [ -n "$1" ]
do
    echo "Parameter #$count = $1"
    count=$[ $count + 1 ]
    shift
done

#-----------测试-------------#
$ ./test13.sh rich barbara katie jessica
Parameter #1 = rich
Parameter #2 = barbara
Parameter #3 = katie
Parameter #4 = jessica
$
```

> 使用shift命令的时候要小心。如果某个参数被移出，它的值就被丢弃了，无法再恢复。  



### 5.2 处理选项（-a -f 之类的，推荐使用getopts）

#### 分离参数与选项

- 【建议配合getopt】可以采用双破折号`--`，来表示选项列表结束，剩下的参数就是命令行参数了

```sh
$ cat test16.sh
#!/bin/bash
# extracting options and parameters
echo
while [ -n "$1" ]
do
    case "$1" in
        -a) echo "Found the -a option" ;;
        -b) echo "Found the -b option";;
        -c) echo "Found the -c option" ;;
        --) shift
        	break ;;
        *) echo "$1 is not an option";;
    esac
    shift
done
#
count=1
for param in $@
do
    echo "Parameter #$count: $param"
    count=$[ $count + 1 ]
done
$

#-----------测试-------------#
$ ./test16.sh -c -a -b -- test1 test2 test3
Found the -c option
Found the -a option
Found the -b option
Parameter #1: test1
Parameter #2: test2
Parameter #3: test3
$
```

#### `getopt`命令

命令格式

```sh
getopt optstring parameters
# optstring是这个过程的关键所在。它定义了命令行有效的选项字母，还定义了哪些选项字母需要参数值。
# 在optstring中列出你要在脚本中用到的每个命令行选项字母。然后，在每个需要参数值的选项字母后加一个冒号。 
# getopt命令会基于你定义的optstring解析提供的参数。
```

例子

```sh
$ getopt ab:cd -a -b test1 -cd test2 test3
-a -b test1 -c -d -- test2 test3
```

> optstring定义了四个有效选项字母：a、b、c和d。冒号（：）被放在了字母b后面，因为b选项需要一个参数值。当getopt命令运行时，它会检查提供的参数列表（-a -b test1-cd test2 test3），并基于提供的optstring进行解析。注意，它会自动将-cd选项分成两个单独的选项，==并插入双破折线来分隔行中的额外参数==。  
>
> 
>
> 如果指定了一个不在optstring中的选项，默认情况下， getopt命令会产生一条错误消息。
>
> ```sh
> $ getopt ab:cd -a -b test1 -cde test2 test3
> getopt: invalid option -- e
> -a -b test1 -c -d -- test2 test3
> $
> ```
>
> ==如果想忽略这条错误消息，可以在命令后加-q选项==。
>
> ```sh
> $ getopt -q ab:cd -a -b test1 -cde test2 test3
> -a -b 'test1' -c -d -- 'test2' 'test3'  
> ```

#### 在脚本中使用getopt

- 利用set命令，将原始的命令行参数用getopt命令的输出替换`set -- $(getopt -q ab:cd "$@")`  

例子

```sh
$ cat test18.sh
#!/bin/bash
# Extract command line options & values with getopt
#
set -- $(getopt -q ab:cd "$@")
#
echo
while [ -n "$1" ]
do
    case "$1" in
    -a) echo "Found the -a option" ;;
    -b) param="$2"
        echo "Found the -b option, with parameter value $param"
        shift ;;
    -c) echo "Found the -c option" ;;
    --) shift
        break ;;
    *) echo "$1 is not an option";;
    esac
    shift
done
#
count=1
for param in "$@"
do
    echo "Parameter #$count: $param"
    count=$[ $count + 1 ]
done
#
$

#-----------测试1------------#
$ ./test18.sh -ac
Found the -a option
Found the -c option
$
#-----------测试2------------#
$ ./test18.sh -a -b test1 -cd test2 test3 test4
Found the -a option
Found the -b option, with parameter value 'test1'
Found the -c option
Parameter #1: 'test2'
Parameter #2: 'test3'
Parameter #3: 'test4'
$
```

==**缺点**==：getopt无法很好的处理戴空格和引号的参数值



#### 【推荐】使用更加高级的getopts

- 命令格式：`getopts optstring variable  `
  - `optstring`值类似于`getopt`命令中的那个。有效的选项字母都会列在`optstring`中，如果选项字母要求有个参数值，就加一个冒号
  - 要去掉错误消息的话，可以在`optstring`之前加一个冒号。 【`getopt`的话是加 `-q` 选项】
  - `getopts`命令将当前参数保存在命令行中定义的`variable`中。  
- 环境变量`OPTARG`会被用来保存选项后跟的参数值
- 环境变量`OPTIND`会在getopts处理每个选项时，自动增一 ，可以知道现在处理到第几个选项了

例子【case中不需要加--】

```sh
$ cat test19.sh
#!/bin/bash
# simple demonstration of the getopts command
#
echo
while getopts :ab:c opt
do
    case "$opt" in
        a) echo "Found the -a option" ;;
        b) echo "Found the -b option, with value $OPTARG";;
        c) echo "Found the -c option" ;;
        *) echo "Unknown option: $opt";;
    esac
done

#-----------测试1-------------#
$ ./test19.sh -ab test1 -c
Found the -a option
Found the -b option, with value test1
Found the -c option
$
#-----------测试2：包含空格-------------#
$ ./test19.sh -b "test1 test2" -a
Found the -b option, with value test1 test2
Found the -a option

#-----------测试3：选项与参数可不用空格-------------#
$ ./test19.sh -abtest1
Found the -a option
Found the -b option, with value test1

#-----------测试4：自动将未定义的选项统一输出层问号-------------#
$ ./test19.sh -d
Unknown option: ?
$
$ ./test19.sh -acde
Found the -a option
Found the -c option
Unknown option: ?
Unknown option: ?
```

在getopts处理每个选项时，它会将`OPTIND`环境变量值增一。在getopts完成处理时，你可以使用`shift`命令和`OPTIND`值来移动参数  

```sh
$ cat test20.sh
#!/bin/bash
# Processing options & parameters with getopts
#
echo
while getopts :ab:cd opt
do
    case "$opt" in
    a) echo "Found the -a option" ;;
    b) echo "Found the -b option, with value $OPTARG" ;;
    c) echo "Found the -c option" ;;
    d) echo "Found the -d option" ;;
    *) echo "Unknown option: $opt" ;;
    esac
done
#
shift $[ $OPTIND - 1 ]
#
echo
count=1
for param in "$@"
do
    echo "Parameter $count: $param"
    count=$[ $count + 1 ]
done
#

#-----------测试-------------#
$ ./test20.sh -a -b test1 -d test2 test3 test4
Found the -a option
Found the -b option, with value test1
Found the -d option
Parameter 1: test2
Parameter 2: test3
Parameter 3: test4
$
```



### 5.3 选项标准化

| 选       项 |                描述                |
| :---------: | :--------------------------------: |
|    `-a`     |            显示所有对象            |
|    `-c`     |            生成一个计数            |
|    `-d`     |            指定一个目录            |
|    `-e`     |            扩展一个对象            |
|    `-f`     |         指定读入数据的文件         |
|    `-h`     |         显示命令的帮助信息         |
|    `-i`     |           忽略文本大小写           |
|    `-l`     |        产生输出的长格式版本        |
|    `-n`     |      使用非交互模式（批处理）      |
|    `-o`     | 将所有输出重定向到的指定的输出文件 |
|    `-q`     |           以安静模式运行           |
|    `-r`     |        递归地处理目录和文件        |
|    `-s`     |           以安静模式运行           |
|    `-v`     |            生成详细输出            |
|    `-x`     |            排除某个对象            |
|    `-y`     |         对所有问题回答yes          |



### 5.4 获取用户输入  read

#### 基本读入

- 基本使用：将数据存入指定变量

```sh
$ cat test21.sh
#!/bin/bash
# testing the read command
#
echo -n "Enter your name: "
read name
echo "Hello $name, welcome to my program. "
#
#-----------测试-------------#
$ ./test21.sh
Enter your name: Rich Blum
Hello Rich Blum, welcome to my program.
$
```

> 注意：生成提示的echo命令使用了-n选项。该选项不会在字符串末尾输出换行符，允许脚本用户紧跟其后输入数据，而不是下一行。  
>
> 你会注意到，在第一个例子中当有名字输人时，read命令会将姓和名保存在同一个变量中。read命令会将提示符后输入的所有数据分配给单个变量，要么你就指定多个变量。输入的每个数据值都会分配给变量列表中的下一个变量。**如果变量数量不够，剩下的数据就全部分配给最后一个变量**。

- **-p选项**：直接在read命令行指定提示符，就不用echo了

```sh
$ cat test22.sh
#!/bin/bash
# testing the read -p option
#
read -p "Please enter your age: " age
days=$[ $age * 365 ]
echo "That makes you over $days days old! "
#
```

- 若不指定变量，则默认存入环境变量`REPLY`中

```sh
$ cat test24.sh
#!/bin/bash
# Testing the REPLY Environment variable
#
read -p "Enter your name: "
echo
echo Hello $REPLY, welcome to my program.
#
```



####  `-t`超时，`-n`指定字符数

- 超时等待

```sh
##延时等待5秒
if read -t 5 -p "Please enter your name: " name
then
    echo "Hello $name, welcome to my script"
else
    echo
    echo "Sorry, too slow! "
fi
```

- `-n1`：指定一个字符（不需要回车）

```sh
$ cat test26.sh
#!/bin/bash
# getting just one character of input
#
read -n1 -p "Do you want to continue [Y/N]? " answer
case $answer in
Y | y) echo
    echo "fine, continue on…";;
N | n) echo
    echo OK, goodbye
	exit;;
esac
```

#### `-s`隐藏方式读取（输入密码）

```sh
$ cat test27.sh
#!/bin/bash
# hiding input data from the monitor
#
read -s -p "Enter your password: " pass
echo
echo "Is your password really $pass? "
$
```

#### 从文件中读取

- 通过 `read -r 变量1 变量2`的方式

```sh
$ cat test26
#!/bin/bash
# process new user accounts

input="users.csv"
while IFS=',' read -r userid name
do
    echo "adding $userid"
    useradd -c "$name" -m $userid
done < "$input"
$

#其中users.csv的文件内容如下
$ cat users.csv
rich,Richard Blum
christine,Christine Bresnahan
barbara,Barbara Blum
tim,Timothy Bresnahan
$
```

- 通过 cat命令读取文件，并通过管道直接传给含有`read`命令的`while`命令

```sh
$ cat test28.sh
#!/bin/bash
# reading data from a file
#
count=1
cat testfile | while read line
do
    echo "Line $count: $line"
    count=$[ $count + 1]
done
echo "Finished processing the file"
$
#--------文件数据-------#
$ cat testfile
The quick brown dog jumps over the lazy fox.
This is a test, this is only a test.
O Romeo, Romeo! Wherefore art thou Romeo?
$
#--------测试---------#
$ ./test28.sh
Line 1: The quick brown dog jumps over the lazy fox.
Line 2: This is a test, this is only a test.
Line 3: O Romeo, Romeo! Wherefore art thou Romeo?
Finished processing the file
```



## 6. 呈现数据

### 6.1 标准输出与错误输出的重定向

- `>`默认只重定向标准输出
- `1>`只重定向标准输出
- `2>`只重定向标准错误输出
- `&>`将标准输出与标准错误输出一起重定向【且默认错误消息的优先级更高】

```sh
# 将标准错误输出重定向到文件test5中，标准输出依旧在屏幕中显示
ls -al test badtest test2 2> test5
# 将标准错误输出重定向到test6，标准输出重定向到test7
ls -al test test2 test3 badtest 2> test6 1> test7
# 将标准错误输出和标准输出都重新向到test7中【且标准错误输出的等级更高】
ls -al test test2 test3 badtest &> test7
```



### 6.2 脚本中重定向输出

可分为：临时重定向、永久重定向脚本中所有的命令

- **临时重定向**

```sh
$ cat test8
#!/bin/bash
# testing STDERR messages
echo "This is an error" >&2
echo "This is normal output"
$

#------测试1：像平常一样使用没有什么变化-------#
$ ./test8
This is an error
This is normal output
$
#------测试2：运行时重定向，则发生改变--------#
$ ./test8 2> test9
This is normal output
$ cat test9
This is an error
$
```

- **永久重定向**：使用`exec`命令

```sh
$ cat test11.sh
#!/bin/bash
# redirecting output to different locations
exec 2>testerror
echo "This is the start of the script"
echo "now redirecting all output to another location"
exec 1>testout
echo "This output should go to the testout file"
echo "but this should go to the testerror file" >&2
$

#-----------测试------------#
$ ./test11
This is the start of the script
now redirecting all output to another location
$ cat testout
This output should go to the testout file
$ cat testerror
but this should go to the testerror file
$
```

> 这个脚本用exec命令来将发给`STDERR`的输出重定向到文件`testerror`。接下来，脚本用echo语句向`STDOUT`显示了几行文本。随后再次使用exec命令来将`STDOUT`重定向到`testout`文件。注意，尽管`STDOUT`被重定向了，但你仍然可以将echo语句的输出发给`STDERR`，在本例中还是重定向到`testerror`文件。  



### 6.3 脚本中重定向输入

```sh
exec 0< testfile
# 只要在脚本需要输入的时候（如read），就会从testfile中获取输入
```

实例

```sh
$ cat test12
#!/bin/bash
# redirecting file input
exec 0< testfile
count=1

while read line
do
    echo "Line #$count: $line"
    count=$[ $count + 1 ]
done
#-----------测试------------#
$ ./test12
Line #1: This is the first line.
Line #2: This is the second line.
Line #3: This is the third line.
$
```



### 6.4 创建自己的重定向 `exec`

在shell中最多可以又9个打开的文件描述符，其中0为`STDIN`，1为`STDOUT`，2为`STDERR`

#### 创建输出文件描述符

分配文件描述符`3~8`

```sh
$ cat test13
#!/bin/bash
# using an alternative file descriptor

exec 3>test13out

echo "This should display on the monitor"
echo "and this should be stored in the file" >&3
echo "Then this should be back on the monitor"
$ ./test13
This should display on the monitor
Then this should be back on the monitor
$ cat test13out
and this should be stored in the file
$
```

#### 重定向文件描述符

```sh
$ cat test14
#!/bin/bash
# storing STDOUT, then coming back to it

# 将文件描述符3重定向到文件描述符1即STDOUT（显示器）
exec 3>&1
#将STDOUT重定向到文件
exec 1>test14out

echo "This should store in the output file"
echo "along with this line."

#将STDOUT重定向回显示器
exec 1>&3

echo "Now things should be back to normal"
#-----------测试------------#
$ ./test14
Now things should be back to normal
$ cat test14out
This should store in the output file
along with this line.
$
```

#### 创建输入文件描述符

```sh
#!/bin/bash
# redirecting input file descriptors

# 将文件描述符6重定向为标准输入
exec 6<&0

# 将标准输入重定向为testfile
exec 0< testfile

count=1
while read line
do 
	echo "Line #$count:$line"
	count=$[ $count + 1 ]
done

exec 0<&6
read -p "Please input something for test:" input
echo $input

#-----------测试------------#
$ sh test15.sh 
Line #1:This is the first line.
Line #2:This is the second line.
Line #3:This is the third line.
Line #4:This is the forth line.
Please input something for test:sadfad
sadfad

```

#### 关闭文件描述符

一般会在脚本退出时自动关闭，若想要提前关闭：

```sh
exec 3>&-
```



### 6.5 列出打开的文件描述符 lsof

由于lsof命令会列出整个Linux系统打开的所有文件描述符，故此一般不向普通用户开放；普通用户可通过如下方式使用：

```sh
$ /usr/sbin/lsof
常用：
/usr/sbin/lsof -a -p $$ -d 0,1,2,3……
#-p用于指定进程ID，$$表示当前进程PIP
#-d表示指定要显示的文件描述符编号
```



### 6.6 创建临时文件 `mktemp`

Linux的`/tmp`目录专门用来存放临时文件，且一般系统会在启动时自动删除`/tmp`目录所有文件。【不需要操心清理工作】

- 使用`mktemp`命令可以在`\tmp`目录中创建一个唯一的临时文件【且默认除了root和自己的账户外，其他人都不能访问】

#### 在当前文件夹下生成临时文件

需要自己清理

```sh
mktemp 文件名.XXXXXX
# mktemp命令会用6个字符码替换这6个X---->保证文件名在目录中唯一
# 返回文件名
```

脚本中使用

```sh
tempfile=$(mktemp test19.XXXXXX)
```

例子

```sh
$ cat test19
#!/bin/bash
# creating and using a temp file
tempfile=$(mktemp test19.XXXXXX)

exec 3>$tempfile

echo "This script writes to temp file $tempfile"
echo "This is the first line" >&3
echo "This is the second line." >&3
echo "This is the last line." >&3
# 关闭文件描述符
exec 3>&-

echo "Done creating temp file. The contents are:"
cat $tempfile
# 删除创建的临时文件
rm -f $tempfile 2> /dev/null

#-----------测试------------#
$ ./test19
This script writes to temp file test19.vCHoya
Done creating temp file. The contents are:
This is the first line
This is the second line.
This is the last line.
$ ls -al test19*
-rwxr--r-- 1 rich rich 356 Oct 29 22:03 test19*
$
```

#### 在`/tmp`文件夹下生成临时文件

好处：可在Linux系统上的任何位置都引用该文件，且启动的时候系统会自动帮你清理

```sh
mktemp -r 文件名.XXXXXX
# mktemp命令会用6个字符码替换这6个X---->保证文件名在目录中唯一
# 返回文件全路径
```

#### 创建临时目录

```sh
# 创建临时目录
tempdir=$(mktemp -d dir.XXXXXX)
# 进入临时目录
cd $tempdir
#创建临时文件
tempfile1=$(mktemp temp.XXXXXX)
tempfile2=$(mktemp temp.XXXXXX)
```

### 6.7 记录消息 `tee`

==`tee`命令会将输出同时发送到显示器和日志文件==

```sh
tee [-a] filename
参数说明：
-a：将数据追加到文件中，而不是覆盖
```



## 7. 控制脚本

### 7.1 处理信号

常见信号

| 信号 |    值     |               描述               |
| :--: | :-------: | :------------------------------: |
|  1   | `STGHUP`  |               挂起               |
|  2   | `SIGINT`  |         终止【`Ctrl+C`】         |
|  3   | `SIGQUIT` |               停止               |
|  9   | `SIGKILL` |            无条件终止            |
|  15  | `SIGTERM` |            尽可能停止            |
|  17  | `SIGSTOP` |     无条件停止进程，但不终止     |
|  18  | `SIGTSTP` | 停止或暂停，但不终止【`Ctrl+Z`】 |
|  19  | `SIGCONT` |        继续运行停止的进程        |

> 终止进程就是直接杀死了
>
> 停止进程只是暂停了，但要重启进程（恢复作业）需要发送一个`SIGCONT`信号，命令`bg 作业号`【参见7.4作业控制】
>
> - `kill PID`命令默认发送`SIGTERM`信号【尽可能终止】
> - `kill -9 PID`命令发送`SIGKILL`信号【无条件终止】

#### 捕获信号 `trap`

作用：指定监看并从shell中拦截的信号，并交由本低处理

命令格式：

```sh
trap commands signals

# 捕获 Ctrl+C信号
trap commands SIGINT
# 捕获退出信号----退出时，将会执行commands；提前退出也能捕获到EXIT
trap commands EXIT
```

> 修改捕获：要想在脚本中的不同位置进行不同的捕获处理，只需重新使用带有新选项的trap命令
>
> 删除捕获：trap -- signals（如：`trap -- SIGINT ） `  

例子（本例将无法用`Ctrl+C`关闭）

```sh
$ cat test1.sh
#!/bin/bash
# Testing signal trapping
#
trap "echo ' Sorry! I have trapped Ctrl-C'" SIGINT
#
echo This is a test script
#
count=1
while [ $count -le 10 ]
do
    echo "Loop #$count"
    sleep 1
    count=$[ $count + 1 ]
done
#
echo "This is the end of the test script"
#
```



### 7.2 后台模式运行脚本

在脚本命令后面加 `&`即进入后台模式；

```sh
./test1.sh &
```

可通过 `ps -l 或 ps`查看后台运行的作业；

> ==注意：当后台进程运行时，它仍然会使用终端显示器来显示`STDOUT`和`STDERR`消息==，且不能关闭当前的shell
> 解决办法：将`STDOUT`和`STDERR`重定向



### 7.3 在非控制台下运行脚本 `nohup`

- 解决后台运行不能关闭当前shell的问题，接触终端与进程间的关联
- **自动将`STDOUT`和`STDERR`消息重定向到 `nohup.out`文件中**

```sh
nohup ./test1.sh &
```

> 如果使用`nohup`运行了另一个命令，该命令的输出会被**追加**到已有的`nohup.out`文件中 【所以最好不要在同一个目录中运行多次`nohup`，文件内容会很乱】



### 7.4 作业控制 

> shell将shell中运行的每个进程称为作业  

#### 查看作业 `jobs`

`jobs`命令：可以查看分配给shell的作业，且默认会显示作业号、当前运行状态、作业名（`ps`命令主要显示进程）

| 参数 |                    描述                     |
| :--: | :-----------------------------------------: |
| `-l` |            列出进程`PID`及作业号            |
| `-n` | 只列出上次shell发出的通知后改变了状态的作业 |
| `-p` |               只列出作业`PID`               |
| `-r` |             只列出运行中的作业              |
| `-s` |             只列出已停止的作业              |

```sh
$ jobs
[1]+ 	Stopped 	./test10.sh
[2]- 	Running 	./test10.sh > test10.out &
```

```sh
$ jobs -l
[1] 	1950 	Running 	./test10.sh > test10a.out &
[2]- 	1952 	Running 	./test10.sh > test10b.out &
[3]+ 	1955 	Running 	./test10.sh > test10c.out &
```

> **注意**：
>
> - 上面的`+`号表示**默认作业**，在使用作业控制命令时，如果未在命令行指定任何作业号，改作业将被当成操作对象；
>
> - 当前默认作业完成后，带`-`号的作业成为下一个默认作业
> - 任何时候都只有**一个**带`+`号的作业 和 **一个**带`-`号的作业



#### 重启停止的作业 `bg` 或者 `fg`

后台模式重启作业：

```sh
bg 作业号
【注意】：是作业号，不是PID
```

前台模式重启作业：

```
fg 作业号
【注意】：是作业号，不是PID
```



### 7.5 调整优先级 `nice` 、`renice`

- 由shell启动的所有进程的调度优先级默认是相同的，默认0级
- 数值越小，优先级越高
- 调度优先级是个整数值，从-20（最高优先级）到+19（最低优先级）————即`[-20,19]`

#### `nice`命令

- 普通用户只能降低运行优先级
- 只能在开始运行时指定
- root用户可以任意调整优先级

```sh
nice -n 命令等级 运行脚本
```

例子

```sh
$ nice -n 10 ./test4.sh > test4.out &
[1] 4973
$
$ ps -p 4973 -o pid,ppid,ni,cmd
PID PPID NI CMD
4973 4721 10 /bin/bash ./test4.sh
$
```



#### `renice`命令 -- 改变正在运行命令的优先级

- 普通用户只能降低运行优先级
- 只能对属于你的进程执行`renice`
- root用户可以任意调整优先级

```sh
renice -n 命令等级 -p PID
```

例子

```sh
$ ./test11.sh &
[1] 5055
$
$ ps -p 5055 -o pid,ppid,ni,cmd
PID PPID NI CMD
5055 4721 0 /bin/bash ./test11.sh
$
$ renice -n 10 -p 5055
5055: old priority 0, new priority 10
$
$ ps -p 5055 -o pid,ppid,ni,cmd
PID PPID NI CMD
5055 4721 10 /bin/bash ./test11.sh
$
```



### 7.6 定时运行任务 `at`和`crontab`

文档：Day02-任务调度.note
链接：http://note.youdao.com/noteshare?id=949f715a1f44a47be5b3cc171a2f89c5&sub=45FE460351DB4F67B3A3AEF1718975AB



# 三、高级shell脚本编程

## 1. 创建函数

### 1.1 基本脚本函数

注意：要在函数使用前定义函数

- 法一

```sh
function name {
	commands
}
```

- 法二

```sh
name() {
	commands
}
```

> 函数名后的空括号表明正在定义的是一个函  



### 1.2 返回值

倘若没有return，则默认返回最后一条命令的退出状态码

- **return**：允许指定一个整数值来定义函数的退出状态码
  - 记住，函数一结束就取返回值
  - 记住，退出状态码必须是`0~255`

例子

```sh
$ cat test5
#!/bin/bash
# using the return command in a function

function dbl {
    read -p "Enter a value: " value
    echo "doubling the value"
    return $[ $value * 2 ]
}

dbl
echo "The new value is $?"

$
```

- **使用函数输出（注意与上面的不同）**
  - 这种方式没有大小限制（不需要在`0~255`）

```sh
$ cat test5b
#!/bin/bash
# using the echo to return a value
# 函数内的echo并不会输出显示
function dbl {
    read -p "Enter a value: " value
    echo $[ $value * 2 ]
}

result=$(dbl)  # 会将echo的输出显示，直接返回给result变量
echo "The new value is $result"

#-----------测试1------------#
$ ./test5b
Enter a value: 200
The new value is 400
#-----------测试2------------#
$ ./test5b
Enter a value: 1000
The new value is 2000
$
```



### 1.3 在函数中使用变量（传参）

#### 传参数

使用方式

```sh
func1 $value1 参数2 参数3 参数4……    #  参数和函数必须在同一行
```

同样，在函数内，参数会通过`$1`，`$2`，`$3`定义，`$#`表示参数个数，`$@`表示所有参数【所以函数不能直接获取到脚本在命令行中的参数】

例子

```sh
$ cat test6
#!/bin/bash
# passing parameters to a function
function addem {
    if [ $# -eq 0 ] || [ $# -gt 2 ]
    then
    	echo -1
    elif [ $# -eq 1 ]
    then
    	echo $[ $1 + $1 ]
    else
    	echo $[ $1 + $2 ]
    fi
}
echo -n "Adding 10 and 15: "
value=$(addem 10 15)
echo $value
echo -n "Let's try adding just one number: "
value=$(addem 10)
echo $value
echo -n "Now trying adding no numbers: "
value=$(addem)
echo $value
echo -n "Finally, try adding three numbers: "
value=$(addem 10 15 20)
echo $value

#-----------测试2------------#
$ ./test6
Adding 10 and 15: 25
Let's try adding just one number: 20
Now trying adding no numbers: -1
Finally, try adding three numbers: -1
$
```

#### 在函数中处理变量

- **全局变量**

在shell中**任何**地方（包括函数内）直接复制定义的变量，都是全局变量

容易出现问题，建议在函数内部赋值变量时，使用局部变量

- **局部变量（`local关键字`）**

例子（在变量声明的前面 或者 在变量赋值语句中都可以使用）

```sh
$ cat test9
#!/bin/bash
# demonstrating the local keyword

function func1 {
    local temp=$[ $value + 5 ]
    result=$[ $temp * 2 ]
}

temp=4
value=6

func1
echo "The result is $result"
if [ $temp -gt $value ]
then
    echo "temp is larger"
else
    echo "temp is smaller"
fi

#-----------测试------------#
$ ./test9
The result is 22
temp is smaller
$
```



### 1.4 数组变量和函数（不推荐）

在函数内部使用数组变量，必须重新组合成一个新的变量，否则无法获取到数组变量的值

例子

```sh
$ cat test10
#!/bin/bash
# array variable to function test

function testit {
    local newarray
    newarray=(;'echo "$@"')
    echo "The new array value is: ${newarray[*]}"
}

myarray=(1 2 3 4 5)
echo "The original array is ${myarray[*]}"
testit ${myarray[*]}

#-----------测试------------#
$ ./test10
The original array is 1 2 3 4 5
The new array value is: 1 2 3 4 5
$
```



### 1.5 函数递归（不推荐）

例子：

- 计算阶乘：5! = 1 * 2 * 3 * 4 * 5 = 120  
- 简化：x! = x * (x-1)!  
- 终止条件：当x=1时，返回1

```sh
function factorial {
    if [ $1 -eq 1 ]
    then
    	echo 1
    else
        local temp=$[ $1 - 1 ]
        local result='factorial $temp'
        echo $[ $result * $1 ]
    fi
}
```



### 1.6 创建库

- **创建**：创建库文件

```sh
$ cat myfuncs.sh
# my script functions
function addem {
    echo $[ $1 + $2 ]
}
function multem {
    echo $[ $1 * $2 ]
}
function divem {
    if [ $2 -ne 0 ]
    then
    	echo $[ $1 / $2 ]
    else
    	echo -1
    fi
}
$
```

- **使用1**：在需要的脚本文件中，包含库文件【使用source】

```sh
在#!/bin/bash的后面，最开始放置如下代码即可
source ./myfuncs.sh
或者
. ./myfuncs.sh
```

> **注意**：不仅可以在脚本内使用，在命令行内也可以这样干

- **使用2**：可以直接在~/.bashrc中source，这样在任何脚本内都能直接使用，命令行内也能直接使用



### ==实例（函数不一定要自己写，可以下载）==

推荐使用GNU shtool shell脚本函数库。 shtool库提供了一些简单的shell脚本函数，可以用来完成日常的shell功能。

#### 下载及安装

- **下载**

可以用FTP客户端、浏览器直接下载

下载地址：ftp://ftp.gnu.org/gnu/shtool/shtool-2.0.8.tar.gz  

- **提取**

```sh
$ tar -zxvf shtool-2.0.8.tar.gz
```

- **构建库**

```sh
# 执行configure脚本，检查环境
$ ./confifgure
# 构建
$ make
# 构建完毕，可以选择进行测试
$ make test
```

- **安装库**

```sh
# 使用root用户身份
$ su
# 安装
$ make install
```

- **shtool库函数列表**

|    函数    |                       描述                        |
| :--------: | :-----------------------------------------------: |
|   `Arx`    |           创建归档文件（包含扩展功能）            |
|   `Echo`   |          显示字符串，提供了一些扩展构件           |
| `fixperm`  |              改变目录树中的文件权限               |
| `install`  |                  安装脚本或文件                   |
|  `mdate`   |             显示文件或目录的修改时间              |
|  `mkdir`   |                创建一个或更多目录                 |
|   `Mkln`   |               使用相对路径创建链接                |
| `mkshadow` |                  创建一颗阴影树                   |
|   `move`   |              带有替换功能的文件移动               |
|   `Path`   |                   处理程序路径                    |
| `platform` |                 **显示平台标识**                  |
| **`Prop`** |         ==显示一个带有动画效果的进度条==          |
|  `rotate`  |                   转置日志文件                    |
|   `Scpp`   |                  共享的C预处理器                  |
|   `Slo`    |           根据库的类别，分离链接器选项            |
|  `Subst`   |                 使用sed的替换操作                 |
|  `Table`   | 以表格的形式显示字段分割（field-separated）的数据 |
| `tarball`  |             从文件和目录中创建tar文件             |
| `version`  |                 创建版本信息文件                  |



#### 使用库

使用格式

```sh
shtool [options] [function [options] [args]]
```

实例1：显示平台信息

```sh
$ cat test16
#!/bin/bash

shtool platform
#-----------测试------------#
$ ./test16
Ubuntu 14.04 (iX86)
$
```

实例2：显示进度条

只需要将希望监看的输出管接到shtool脚本即可

- `-p`选项允许你定制输出文本，这段文本会出现在进度条字符之前 

```sh
$ ls –al /usr/bin | shtool prop –p "waiting..."
waiting...
$
```



## 2. 图形化桌面中的脚本编程

有需要shell中出现图形界面的时候，查阅《Linux命令行玉shell脚本编程大全》  第三版P378

![image-20210819194731756](https://gitee.com/monamania/picBed/raw/master/img/20210819194747.png)

Dialog包的使用---制作窗口

![image-20210819194742977](https://gitee.com/monamania/picBed/raw/master/img/20210819194744.png)

![image-20210819194758564](https://gitee.com/monamania/picBed/raw/master/img/20210819194827.png)



## 3. sed 与 gawk 初级--文本处理（awk见鸟哥）

可以参考《鸟哥的Linux》，其中awk也非常强悍！可以参考鸟哥

### 3.1 文本处理入门

#### sed编辑器

——流编辑器（stream editor）

- 作用：可以根据命令处理数据流中的数据（命令可以从命令行输入，也可以是命令文件中获取）

- 流程

  - ==一次从输入中读取一行数据==
  - 根据提供的编辑器命令匹配数据
  - 按照命令修改流中的数据【只修改流中的数据，不会修改源文件】
  - 将新的数据输出到STDOUT

- 命令格式

  ```sh
  sed options 'script命令' [file]     或者  通过管道命令传入，而不用file
  ```

  | 选项(options)                      | 描述                                                 |
  | :--------------------------------- | :--------------------------------------------------- |
  | -e  'script'【使用多个命令时使用】 | 在处理输入时，将script中指定的命令添加道已有的命令中 |
  | -f file                            | 在处理输入时，将file中指定的命令添加到已有的命令中   |
  | -n                                 | 静默模式。不产生命令输出，使用print命令来完成输出    |

  > script命令 参数指定了应用于流数据上的单个命令  

- 简单的例子，**使用多个命令**【-e充当分号的作用，当没有`;`时，用-e组合成一个命令】

  ```sh
  # 用big test替换test
  echo "This is a test" | sed 's/test/big test/'
  This is a big test
  
  # 当有多个命令时，命令间需要用;号间隔。（在命令末尾和;号之间不能用空格），或者使用-e命令
  sed -e 's/brown/green/; s/dog/cat/' data1.txt
  等价于：sed -e 's/brown/green/' -e' s/dog/cat/' data1.txt
  或者利用次提示符——不需要分号
  $ sed -e '
  > s/brown/green/
  > s/fox/elephant/
  > s/dog/cat/' data1.txt
  
  
  # 从文件中读取编辑器命令——不需要分号
  $ cat script1.sed
  s/brown/green/
  s/fox/elephant/
  s/dog/cat/
  $ sed -f script1.sed data1.txt
  ```

#### gawk程序--与awk类似-数据分割

> 可用于提取数据元素  
>
> 【与awk类似，除了细微差距，基本一致（awk可能处理的数据量相对较小）】
>
> 需要额外安装
>
> Ubuntu：`sudo apt-get install gawk`

- 特点：

  - 支持定义变量来保存数据；
  - 支持使用算术和字符串操作符来处理数据；
  - 支持使用结构化编程概念（比如if-then语句和循环）来为数据处理增加处理逻辑；
  - 支持通过提取数据文件中的数据元素，将其重新排列或格式化，生成格式化报告  

- 命令格式

  ```sh
  gawk options '{program}' file
  ```

  | 选项         | 描述                                 |
  | ------------ | ------------------------------------ |
  | -F fs        | **指定行中划分数据字段的字段分隔符** |
  | -f file      | 从指定的文件中读取程序               |
  | -v ver=value | 定义gawk程序中的一个变量及其默认值   |
  | -mf N        | 指定要处理的数据文件中的最大字段数   |
  | -mr N        | 指定数据文件中的最大数据行数         |
  | -W keyword   | 指定gawk的兼容模式或警告等级         |

- **使用数据字段变量**【默认字段分隔符为 **空格** 或者 **制表符**，可通过**-F选项**修改分隔符】

  - $0：代表整个文本**行**

  - $1：代表文本行中的第1个数据字段

  - $2：代表文本行中的第2个数据字段

  - $n：代表文本行中的第n个数据字段

  - 例子

    ```sh
    $ cat data2.txt
    One line of test text.
    Two lines of test text.
    Three lines of test text.
    $
    $ gawk '{print $1}' data2.txt
    One
    Two
    Three
    $
    ```

- **修改分隔符**

  - 在gawk程序文件中：类似shell脚本中的关键字`IFS`，==这里使用`FS`==
  - 命令行中：使用-F选项——例如：`-F;`

- **使用多个命令**

  - 命令行中：与sed类似，使用`;`分号分开即可

    ```sh
    $ echo "My name is Rich" | gawk '{$4="Christine"; print $0}'
    My name is Christine
    ```

  - 也可以使用次提示符——不用分号`;`

    ```sh
    $ gawk '{
    > $4="Christine"
    > print $0}'
    My name is Rich
    My name is Christine
    
    # 通过 Ctrl+D退出
    ```

- 从**文件中读取程序**——使用-f 选项

  ```sh
  $ cat script3.gawk   【注意：引用变量时，并不需类似sh中一样使用美元符$】
  {
  text = "'s home directory is "
  print $1 text $6
  }
  
  $ gawk -F: -f script3.gawk /etc/passwd
  root's home directory is /root
  bin's home directory is /bin
  …………省略
  ```

- 在处理数据**前**运行脚本

  - 可用于创建标题

  - 使用：添加BEGIN关键字即可

  - ```sh
    $ gawk 'BEGIN {print "The data3 File Contents:"}
    > {print $0}' data3.txt
    The data3 File Contents:
    Line 1
    Line 2
    Line 3
    ```

- 在处理数据**后**运行脚本

  - 可用于标识是否执行完毕

  - 使用：添加END关键字即可

  - ```sh
    $ gawk 'BEGIN {print "The data3 File Contents:"}
    > {print $0}
    > END {print "End of File"}' data3.txt
    The data3 File Contents:
    Line 1
    Line 2
    Line 3
    End of File
    ```



### 3.2 sed 编辑器基础

都是以每行为单位的

只修改流中的内容，不会修改原文件

#### q命令：退出编辑器

#### s命令：替换-相关标记

> **s命令（substitute  ）**：用于在行中替换文本

基本使用

```sh
sed 's/未替换的原始文本/用于替换的新文本/[标记]' date.txt
不一定要用/来分割。sed允许用其他的比如! —— 这样在用到地址时，会看起来更加美观
sed 's!/bin/bash!/bin/csh!' /etc/passwd
```

- 各种标记的含义
  - 默认没有标记时：表示只替换**每行**第一处
  - 数字：表示新文本将替换**每行中第几处**模式匹配的地方
  - g：表示替换所有匹配的文本
  - p：打印与替换命令中指定的模式匹配的行【==**与-n选项配合**，可只打印被替换命令修改过的行==】
  - w file：将替换的结果写道文件中



#### 行寻址：限制命令作用的行

默认情况下，在sed编辑器中使用的命令会作用于文本数据的所有行。如果只想将命令**作用于特定行或某些行**，则必须用**行寻址**（ line addressing）  

**行寻址分类**：数字形式表示行区间、文本模式过滤出行

**基本使用格式**

```
[address]command
或者
address {
	command1
    command2
    command3
}
或者
address {command1; command2; command3}
```

- **数字寻址形式**

  - 指定某一行：`sed '2s/dog/cat/' data1.txt  `
  - 指定行区间：`sed '2,3s/dog/cat/' data1.txt `
  - 指定某行开始到**末尾**：`sed '2,$s/dog/cat/' data1.txt  `

- **文本模式过滤器**（正则寻址、模式匹配）

  - 格式：`/pattern（可以是正则表达式）/command  `

  - 例子：只修改用户monomania的默认shell  

    - ```sh
      $ grep monomania /etc/passwd
      monomania:x:1000:1000:monomania:/home/monomania:/bin/bash
      $
      # -n静默输出，-n选项配合p标识，可以只打印修改过的内容
      $ sed -n '/monomania/s/bash/csh/p' /etc/passwdmonomania:x:1000:1000:monomania:/home/monomania:/bin/csh$
      ```



#### d命令：删除行

**注意**：一定要加上行寻址，不然默认删除**流中**所有行

**数字寻址形式**

- 删除某一行：`sed '3d' data6.txt  `
- 删除特定区间的行：`sed '2,3d' data6.txt  `
- 删除某行开始到末尾：`sed '3,$d' data6.txt  `

**文本过滤形式**

- `sed '/number 1/d' data6.txt`  



#### 插入(i)和附加(a)文本--（反斜杠`\`）

- `i`命令：在指定行**前面**插入（insert）新行
- `a`命令：在指定行**后面**附加（append）新行

不能在单个命令行上使用，必须指定是要将行插入还是附加

格式【注意是反斜杠`\`跟前面的不同】

```sh
sed '[address]command\new line'    【不加行寻址的话，会在每一行前面都加这个新行】
```

> 注意：
>
> - 不加行寻址的话，会在每一行前面都执行这个命令
> - 同时支持 数字形式 和 文本过滤模式 的行寻址

例子：每一行前都插入

```sh
$ echo "Test Line 2" | sed 'i\Test Line 1'Test Line 1Test Line 2
```

例子：第一行后面附加

```sh
$ echo "Test Line 2" | sed '1a\>Test Line 1'Test Line 2Test Line 1
```



#### c命令：修改行--（反斜杠`\`）

概念：修改（ change）命令允许修改数据流中整行文本的内容 

格式：与插入、附加相同

> 它跟插入和附加命令的工作机制一样，你必须在sed命令中单独指定新行
>
> - 不加行寻址的话，会在每一行前面都执行这个命令
> - 同时支持 数字形式 和 文本过滤模式 的行寻址

**例子**：修改3行的内容

```sh
$ sed '3c\> This is a changed line of text.' data6.txt
This is line number 1.
This is line number 2.
This is a changed line of text.
This is line number 4.
```

**例子**：当使用地址区间寻址时，会用新行替换区间内的所有行，而**不是逐一修改**

```sh
$ sed '2,3c\> This is a new line of text.' data6.txt
This is line number 1.
This is a new line of text.
This is line number 4.
```



#### y命令：转换命令

概念：转换（ transform）命令（ y）是唯一可以处理单个字符的sed编辑器命令。  

格式【inchars和outchars的长度一定要相同】

```sh
[address]y/inchars/outchars/ 
```

> - 转换命令会对inchars和outchars值进行==一对一==的映射。
> - inchars中的第一个字符会被转换为outchars中的第一个字符，第二个字符会被转换成outchars中的第二个字符。
> - 这个映射过程会一直持续到处理完指定字符。**如果inchars和outchars的长度不同，则sed编辑器会产生一条错误消息** 
>
> 注意：转换是全局性的命令，它会将==文本行==中找到的==所有==指定字符自动进行转换，而不会考虑它们出现的位置。【可以用行寻址指定特定行，但是某一行中的哪个位置无法指定】  

例子：

```sh
$ sed 'y/123/789/' data8.txt    # 即所有的1-->7，2-->8,3-->9
This is line number 7.
This is line number 8.
This is line number 9.
This is line number 4.
This is line number 7 again.    # 原本为1---现替换为了7
This is yet another line.
This is the last line in the file.
$
```



#### 打印相关（如行号）

| 命令           | 描述                                             |
| :------------- | ------------------------------------------------ |
| `p`            | 打印文本行（与s命令的p标记类似，建议搭配-n选项） |
| `=`            | 打印行号                                         |
| `l`（小写的L） | 列出行，打印数据流中的文本和不可打印的ASCII字符  |

- **p命令**：打印行

  - 建议配合-n选项（静默模式）——只打印指定内容

  - 例子1：只打印第2行

    ```sh
    $ echo 'this is a test> this the second test' | sed -n '2p'this the second test
    ```

  - 例子2：只打印正则匹配的行（文本过滤模式）

    ```sh
    $ echo 'this is a test> this the second test' | sed -n '/second/p' 
    ```

  - 例子3：配合替换或修改命令。修改修改前显示本行

    ```sh
    $ cat data6.txt 
    This is line number 1.
    This is line number 2.
    This is line number 3.
    This is line number 4.
    This is line number 5.
    $ sed -n '/3/{> p> s/line/test/p> }' data6.txt
    This is line number 3.
    This is test number 3.
    $
    ```
    
    > 注意：这里的{}里这么多命令，为啥不用-e选项——这是行寻址的一个命令格式，可参考行寻址部分

**=命令**：打印行号

- 例子1：

  ```sh
  $ sed '=' data6.txt 
  This is line number 1.
  This is line number 2.
  This is line number 3.
  This is line number 4.
  This is line number 5.
  ```

- 例子2：利用-n选项，让sed编辑器只显示包含匹配文本模式的行的行号和文本  

  - 适合：要在数据流中查找特定文本模式，并获取所在行。  

  ```sh
  $ sed -n '/number 4/{=p}' data6.txt
  This is line number 4.
  ```

- **`l`命令**：列出行

  - **功能**：列出（ list）命令（ l）可以打印数据流中的文本和不可打印的ASCII字符，任何不可打印字符要么在其八进制值前加一个反斜线，要么使用标准C风格的命名法，如`\t`制表符

    例子1：==**类似cat -M或-E或-e选项**==

    ```sh
    $ cat data9.txt
    This line contains tabs.
    $
    $ cat -A data9.txt
    This^Iline^Icontains^Itabs. \a
    $
    $ sed -n 'l' data9.txt
    This\tline\tcontains\ttabs.
    $
    ```
    
    > cat [选项] [文件]...
    >
    > 选项
    > -A, --show-all      等价于 -vET
    > -b, --number-nonblank  对非空输出行编号
    > -e            等价于 -vE
    > -E, --show-ends     在每行结束处显示 $
    > -n, --number       对输出的所有行编号
    > -s, --squeeze-blank   不输出多行空行
    > -t            与 -vT 等价
    > -T, --show-tabs     将跳格字符显示为 ^I
    > -u            (被忽略)
    > -v, --show-nonprinting  使用 ^ 和 M- 引用，除了 LFD 和 TAB 之外
    > --help   显示此帮助信息并离开



#### 使用 sed 处理文件

> **w命令**：写入文件，将处理后的数据流，写入到指定文件中（没有则创建）
>
> **r命令**：从文件读取数据，将一个独立文件中的数据插入到数据流中

- **w写入文件**---类似shell中的数据重定向

  - 格式

    ```sh
    [address]w filename
    ```

    - filename可以使用相对路径或绝对路径，但不管是哪种
    - address地址可以是sed中支持的==**任意类型**==的寻址方式，例如单个行号、文本模式、行区间

  - 例子：从指定文件中找出特定数据，并输出到文件中

    ```sh
    $ cat data11.txt
    Blum, R Browncoat
    McGuiness, A Alliance
    Bresnahan, C Browncoat
    Harken, C Alliance
    $
    $ sed -n '/Browncoat/w Browncoats.txt' data11.txt
    $
    $ cat Browncoats.txt
    Blum, R Browncoat
    Bresnahan, C Browncoat
    ```

- **r从文件中读取**--将一个独立文件中的数据插入到数据流中

  - 格式

    ```
    [address]r filename
    ```

    - filename 参数指定了数据文件的绝对路径或相对路径。你在读取命令中使用地址区间，==**只能指定单独一个行号或文本模式**==地址。 sed 编辑器会将文件中的文本插入到指定地址后。【相当于将filename里的所有数据，**附加**到当前读取数据流的**指定行寻址的后面**】

  - 例子1：数字形式寻址

    ```sh
    $ cat data12.txt
    This is an added line.
    This is the second added line.
    $
    $ sed '3r data12.txt' data6.txt    # 注意：要加入最后一行，将3改为$符即可
    This is line number 1.
    This is line number 2.
    This is line number 3.
    This is an added line.
    This is the second added line.
    This is line number 4.
    $
    ```
  
  - 例子2：文本模式寻址
  
    ```sh
    $ sed '/number 2/r data12.txt' data6.txt
    This is line number 1.
    This is line number 2.
    This is an added line.
    This is the second added line.
    This is line number 3.
    This is line number 4.
    $
    ```
  
  - **一个很酷的用法**：和删除命令配合使用——利用另一个文件中的数据来**替换**文件中的**占位文本**  
  
    ```sh
    $ cat notice.std     # 这个文件为模板文件，其中LTST为其中的占位文本
    Would the following people:
    LIST
    please report to the ship's captain.
    $
    $ cat data11.txt    # 将用于替换LTST占位文本的内容
    Blum, R Browncoat
    McGuiness, A Alliance
    Bresnahan, C Browncoat
    Harken, C Alliance
    $
    $ sed '/LIST/{
    > r data11.txt
    > d
    > }' notice.std
    Would the following people:
    Blum, R Browncoat
    McGuiness, A Alliance
    Bresnahan, C Browncoat
    Harken, C Alliance
    please report to the ship's captain.
    ```
    
    

## 4. sed进阶

### 4.1 多行命令N、D、P

- n：跳过本行，对下一行执行相应操作
- N：将数据流中的下一行加进来创建一个多行组（ multiline group）来处理。
- D：删除多行组中的一行。
- P：打印多行组中的一行  

**模式空间**：是一块活跃的缓冲区，在sed编辑器执行命令时它会保存待检查的文本【单行next命令会将数据流中的下一文本行移动到sed编辑器的工作空间，即模式空间】



#### n和N命令：Next

**n命令：跳过本行，读取下一行**

例子：删除掉匹配到header后面的那行

```sh
$ cat data1.txt
This is the header line.

This is a data line.

This is the last line.
$ sed '/header/{n ; d}' data1.txt
This is the header line.
This is a data line.
This is the last line.
```

**N命令：合并下一行（将下一行附加到本行）**

例子1：利用正则表达式中的`.`通配符，同时匹配空格和换行【原有换行消失】

```sh
$ cat data3.txt
On Tuesday, the Linux System
Administrator's group meeting will be held.
All System Administrators should attend.
Thank you for your attendance.

$ sed 'N ; s/System.Administrator/Desktop User/' data3.txt
On Tuesday, the Linux Desktop User's group meeting will be held.
All Desktop Users should attend.
Thank you for your attendance.
```

例子2：不想将原有换行去掉时【但，这个脚本<u>总是在执行sed编辑器命令前</u>将下一行文本读入到模式空间。当它到了最后一行文本时，就没有下一行可读了，所以N命令会叫sed编辑器停止。如果要匹配的文本正好在数据流的最后一行上，命令就不会发现要匹配的数据。  】

```sh
$ sed 'N
> s/System\nAdministrator/Desktop\nUser/
> s/System Administrator/Desktop User/
> ' data3.txt
On Tuesday, the Linux Desktop
User's group meeting will be held.
All Desktop Users should attend.
Thank you for your attendance.
```

例子3：你可以轻松地解决这个问题——将单行命令放到N命令前面，并将多行命令放到N命令后面 【成功解决上述问题】

```sh
cat data4.txt
On Tuesday, the Linux System
Administrator's group meeting will be held.
All System Administrators should attend

$ sed '
> s/System Administrator/Desktop User/
> N
> s/System\nAdministrator/Desktop\nUser/
> ' data4.txt
```



#### D命令：多行删除命令

**d命令的弊端**：sed编辑器用d来删除模式空间中的当前行。但和N命令一起使用时，使用单行删除命令就要小心了。

```sh
cat data4.txt
On Tuesday, the Linux System
Administrator's group meeting will be held.
All System Administrators should attend

$ sed 'N ; /System\nAdministrator/d' data4.txt
All System Administrators should attend.
$
```

删除命令会在不同的行中查找单词System和Administrator，然后在模式空间中<u>将两行都删掉</u>。

**D命令：只删除多行组合中的==第一行==，即删除到换行符就截止，并强制sed编辑器返回到脚本的顶部，而不读取新的行  **

sed编辑器提供了多行删除命令D，它只删除模式空间中的第一行。该命令会删除到换行符（含换行符）为止的所有字符。

```sh
$ sed 'N ; /System\nAdministrator/D' data4.txt
Administrator's group meeting will be held.
All System Administrators should attend.
$
```

**例子：删除目标所在的前一行文本或空白行**

```sh
$ cat data5.txt

This is the header line.
This is a data line.

This is the last line.
$
$ sed '/^$/{N ; /header/D}' data5.txt
This is the header line.
This is a data line.

This is the last line
```

> sed编辑器脚本会查找空白行，然后用N命令来将下一文本行添加到模式空间。
>
> 如果新的模式空间内容含有单词header，则D命令会删除模式空间中的第一行。如果不结合使用N命令和D命令，就不可能在不删除其他空白行的情况下只删除第一个空白行。  



#### P命令：多行打印命令

**P命令作用**：只打印多行组合中的==第一行==

```sh
$ cat data3.txt
On Tuesday, the Linux System
Administrator's group meeting will be held.
All System Administrators should attend.
Thank you for your attendance.

$ sed -n 'N ; /System\nAdministrator/P' data3.txt
On Tuesday, the Linux System
```



### 4.2 保持空间

**模式空间（ pattern space）**：是一块活跃的缓冲区，在sed编辑器执行命令时它会保存待检查的文本  

sed编辑器有另一块称作**保持空间（ hold space）**的缓冲区域。在处理模式空间中的某些行时，**可以用保持空间来临时保存一些行**。有5条命令可用来操作保持空间

| 命令 | 描述                             |
| :--: | -------------------------------- |
|  h   | 将模式空间**复制**到保持空间     |
|  H   | 将模式空间**附加**到保持空间     |
|  g   | 将保持空间**复制**到模式空间     |
|  G   | 将保持空间**附加**到模式空间     |
|  x   | **交换**模式空间和保持空间的内容 |

例子：

```sh
$ cat data2.txt
This is the header line.
This is the first data line.
This is the second data line.
This is the last line.
$
$ sed -n '/first/ {h ; p ; n ; p ; g ; p }' data2.txt
This is the first data line.
This is the second data line.
This is the first data line.
$
#=========可以用于文档内容反转=====
$ sed -n '/first/ {h ; n ; p ; g ; p }' data2.txt
This is the second data line.
This is the first data line.
```



### 4.3 `!`命令：排除命令

**作用**：让原本在当前匹配模式空间中会起作用的命令不起作用

简单例子

```sh
$ cat data2.txt
This is the header line.
This is the first data line.
This is the second data line.
This is the last line.

$ sed -n '/header/p' data2.txt
This is the header line.

$ sed -n '/header/!p' data2.txt
This is the first data line.
This is the second data line.
This is the last line.

sed -n 'p' data2.txt
This is the header line.
This is the first data line.
This is the second data line.
This is the last line.
```

例子：保持空间实现tac命令（即cat的反命令，反向文件输出）

```sh
$ cat data2.txt
This is the header line.
This is the first data line.
This is the second data line.
This is the last line.
$
$ sed -n '{1!G ; h ; $p }' data2.txt
This is the last line.
This is the second data line.
This is the first data line.
This is the header line.
$
```

![image-20210823143011193](https://gitee.com/monamania/picBed/raw/master/img/20210823143013.png)



### 4.4 改变流

通常， sed编辑器会从脚本的顶部开始，一直执行到脚本的结尾（ D命令是个例外，它会强制sed编辑器返回到脚本的顶部，而不读取新的行）。 sed编辑器提供了一个方法来改变命令脚本的执行流程，其结果与结构化编程类似  

#### b命令：分支，根据地址跳转

**作用**：根据地址跳转

格式

```sh
[address]b [label]
address参数决定了哪些行的数据会触发分支命令。 
label参数定义了要跳转到的位置【默认跳转到脚本结尾，最多可以是7个字符长度】
```

**例子1**：不使用标签。当寻址到2，3两行时跳过替换命令，默认执行完毕

```sh
$ cat data2.txt
This is the header line.
This is the first data line.
This is the second data line.
This is the last line.
$
$ sed '{2,3b ; s/This is/Is this/ ; s/line./test?/}' data2.txt
Is this the header test?
This is the first data line.
This is the second data line.
Is this the last test?
$
```

**例子2**：**使用标签**。当匹配成功，跳转到标签处（跳过中间的命令），执行后面的命令。【标签以冒号开始，最多可以是7个字符长度 `:label2  `】

```sh
$ sed '{/first/b jump1 ; s/This is the/No jump on/
> :jump1
> s/This is the/Jump here on/}' data2.txt
No jump on header line
Jump here on first data line
No jump on second data line
No jump on last line
$
```

> 如果命令的模式没有匹配， sed编辑器会继续执行脚本中的命令，包括分支标签后的命令（因此，所有的替换命令都会在不匹配分支模式的行上执行）  。这里由于第一个替换命令的存在，所以在没有匹配的行，第二个替换命令也正好不起作用了

**例子3**：循环

```sh
$ echo "This, is, a, test, to, remove, commas." | sed -n '{
> :start
> s/,//1p
> /,/b start
> }'	
This is, a, test, to, remove, commas.
This is a, test, to, remove, commas.
This is a test, to, remove, commas.
This is a test to, remove, commas.
This is a test to remove, commas.
This is a test to remove commas.
```



#### t命令：测试，根据结果跳转

**作用**：<u>根据</u>替换命令的<u>结果跳转</u>到某个标签，而不是根据地址进行跳转  

格式

```sh
[address]t [label]
```

测试命令提供了对数据流中的文本执行基本的if-then语句的一个低成本办法。

**例子1**：举个例子，如果已经做了一个替换，不需要再做另一个替换  

```sh
$ sed '{
> s/first/matched/
> t
> s/This is the/No match on/
> }' data2.txt
No match on header line
This is the matched data line
No match on second data line
No match on last line
```

**例子2**：使用标签

```sh
$ echo "This, is, a, test, to, remove, commas. " | sed -n '{
> :start
> s/,//1p
> t start
> }'
This is, a, test, to, remove, commas.
This is a, test, to, remove, commas.
This is a test, to, remove, commas.
This is a test to, remove, commas.
This is a test to remove, commas.
This is a test to remove commas.
```



### 4.5 模式替代（提取匹配模式中的部分）

问题描述

```sh
# 当不使用通配符时，没问题
$ echo "The cat sleeps in his hat." | sed 's/cat/"cat"/'
The "cat" sleeps in his hat.

# 当使用通配符，匹配多个单词时---发现用于替代的字符串无法匹配已匹配字符的中的通配符字符
$ echo "The cat sleeps in his hat." | sed 's/.at/".at"/g'
The ".at" sleeps in his ".at".
```

#### &符号

**作用：**提取匹配替换命令中指定模式的**整个**字符串。  

例子

```sh
$ echo "The cat sleeps in his hat." | sed 's/.at/"&"/g'
The "cat" sleeps in his "hat".
```



#### 提取匹配模式中的子模式`\1、\2`

sed编辑器用**圆括号**来**定义**替换模式中的**子模式**。你可以在替代模式中**使用特殊字符**来**引用每个子模式**。替代字符由反斜线和数字组成。数字表明子模式的位置。 sed编辑器会给第一个子模式分配字符`\1`，给第二个子模式分配字符`\2`……

**作用：**选择将模式中的某**部分**作为替代模式  

> ==**警告**==：当在替换命令中使用圆括号时，**必须用转义字符**将它们标示为分组字符而不是普通的圆括号。这跟转义其他特殊字符正好相反  

简单例子

```sh
$ echo "That furry cat is pretty" | sed 's/furry \(.at\)/\1/'
That cat is pretty
$
$ echo "That furry hat is pretty" | sed 's/furry \(.at\)/\1/'
That hat is pretty
$
```

> 上面的例子中`(.at)`即是一个子模式，含有通配符。这时并不能用`$`（`$`表示提取匹配模式中的整个字符串）



### 4.6 在脚本中使用sed

#### a.包装脚本（wrapper）

将命令行参数作为sed脚本的输入

```sh
$ cat reverse.sh
#!/bin/bash
# Shell wrapper for sed editor script.
# to reverse text file lines.
# 翻转文件内容，类似于tac命令
sed -n '{ 1!G ; h ; $p }' $1
#
```



#### b.重定向sed的输出到变量

可以在脚本中用`$()`将sed编辑器命令的输出重定向到一个变量中，以备后用  

例子：用sed脚本来向数值计算结果添加逗号  

```sh
$ cat fact.sh
#!/bin/bash
# Add commas to number in factorial answer
#
factorial=1
counter=1
number=$1
#
while [ $counter -le $number ]
do
	factorial=$[ $factorial * $counter ]
	counter=$[ $counter + 1 ]
done
#
result=$(echo $factorial | sed '{
:start
s/\(.*[0-9]\)\([0-9]\{3\}\)/\1,\2/
t start
}')
#
echo "The result is $result"
#
$
$ ./fact.sh 20
The result is 2,432,902,008,176,640,000
$
```



## 附加：创建sed实用工具

- **加倍行间距【下面的更好】**：除最后一行外，每一行都增加一个空行
  - `sed '$!G' file.txt` 
    - `$`：表示匹配到最后一行
  
- **对可能含有空白行的文件加倍行间距**：对于已经有空行的，用上面的可能会导致空行过多
  - `sed '/^$/d ; $!G' data6.txt  `
  - `/^$/d `：用于将已有空白行删除

- **给文件中的行编号 **

  - `sed '=' data2.txt | sed 'N; s/\n/ /'  `
    - 其中`=`命令用于生成行号——但是行号在单独的一行，不好
    - `sed 'N; s/\n/ /`：用于将换行替换为空格
  - ==类似的命令==
    - `nl data2.txt  `
    - `cat -n data2.txt  `

- **打印末尾行**

  - 打印最后一行：`sed -n '$p' data2.txt  `

  - 打印末尾若干行：显示末尾10行

    - ```sh
      $ sed '{
      \> :start
      \> $q ; N ; 11,$D
      \> b start
      \> }' data7.txt
      ```

      > 这个脚本会首先检查这行是不是数据流中最后一行。如果是，退出（ q）命令会停止循环 
      >
      > N命令会将下一行附加到模式空间中当前行之后。
      >
      > 如果当前行在第10行后面， `11,$D`命令会删除模式空间中的第一行。
      >
      > 这就会在模式空间中创建出滑动窗口效果。因此，这个sed程序脚本只会显示出data7.txt文件最后10行。  

- **删除空白行**
  - 删除连续多的空白行，留下一行空白行：`sed '/./,/^$/!d' data8.txt  `
  
    - `/./,/^$/`：为匹配区间，从通配符`.`开始到第一个空白行为止
  
  - 删除开头的空白行：`sed '/./,$!d' data9.txt  `
  
  - 删除结尾的空白行：
  
    - ```sh
      sed '{
      :start
      /^\n*$/{$d; N; b start }
      }'
      
      
      $ sed '{
      > :start
      > /^\n*$/{$d ; N ; b start }
      > }' data10.txt
      ```
  
- **删除 HTML 标签** 

  - 不删除多余空白行：`sed 's/<[^>]*>//g' data11.txt  `
  - 删除多余空白行：`sed 's/<[^>]*>//g ; /^$/d' data11.txt  `


## 5. gawk进阶

### 5.1 使用变量

- 内建变量
- 自定义变量

#### a. 内建变量：字段和记录分隔符变量

- **数据字段变量**【默认字段分隔符为 **空格** 或者 **制表符**，可通过-F选项修改分隔符】
  - $0：代表整个文本**行**
  - $1：代表文本行中的第1个数据字段
  - $2：代表文本行中的第2个数据字段
  - $n：代表文本行中的第n个数据字段
  
  > **修改分隔符**
  >
  > - 在gawk程序文件中：类似shell脚本中的关键字`IFS`，==这里使用`FS`==
  > - 命令行中：使用-F选项——例如：`-F;`
  
- **记录变量**

  | 变量        | 描述                                                         |
  | ----------- | ------------------------------------------------------------ |
  | FIELDWIDTHS | 由空格分隔的一列数字，**定义**了每个数据字段**确切宽度**，awk没有，gawk有 |
  | FS          | 输入**字段分隔符**【默认一个空格】                           |
  | RS          | 输入**记录分隔符**【默认一个换行符】                         |
  | OFS         | **输出**字段分隔符——用在print命令的输出上【默认一个空格】    |
  | ORS         | **输出**记录分隔符——用在print命令的输出上【默认一个换行符】  |

- OFS变量的使用

  - 默认gawk将OFS设置为一个空格

  ```sh
  $ cat data1.txt
  data11,data12,data13,data14,data15
  data21,data22,data23,data24,data25
  data31,data32,data33,data34,data35
  #========= 修改 FS ===============
  $ gawk 'BEGIN{FS=","} {print $1,$2,$3}' data1.txt   # 注意一定要用""，不能用''
  data11 data12 data13
  data21 data22 data23
  data31 data32 data33
  #========= 修改 OFS ===============
  $ gawk 'BEGIN{FS=","; OFS="<-->"} {print $1,$2,$3}' data1.txt   # 注意一定要用""，不能用''因为外面用了''
  data11<-->data12<-->data13
  data21<-->data22<-->data23
  data31<-->data32<-->data33
  ```

- FIELDWIDTHS变量：允许你不依靠字段分隔符来读取记录 【gawk有，awk没有】

  - **适用**：在一些应用程序中，数据并没有使用字段分隔符，而是被放置在了记录中的特定列  

  - 一旦设置了FIELDWIDTHS，gawk会自动忽略FS变量，并根据提供的字段宽度来计算**字段**  

    ```sh
    $ cat data1b.txt
    1005.3247596.37
    115-2.349194.00
    05810.1298100.1
    $ gawk 'BEGIN{FIELDWIDTHS="3 5 2 5"}{print $1,$2,$3,$4}' data1b.txt
    100 5.324 75 96.37
    115 -2.34 91 94.00
    058 10.12 98 100.1
    $
    ```

  - 【==警告==】：一旦设定了FIELDWIDTHS变量的值，就不能再改变了。这种方法并不适用于变长的字段！ 

- RS变量和ORS变量

  - 默认为一个换行符
  - 默认的RS值表明，**输入数据流中的每行新文本就是一条新纪录**  
  - **适用**：数据流中碰到占据多行的字段。典型的例子是包含地址和电话号码的数据，其中地址和电话号码各占一行
  - **使用**：使用FS变量设置分割符为换行符，然后设置RS变量为空字符串。然后再数据记录间留一个空白行。

  ```sh
  $ cat data2
  Riley Mullen
  123 Main Street
  Chicago, IL 60601
  (312)555-1234
  Frank Williams
  456 Oak Street
  Indianapolis, IN 46201
  (317)555-9876
  Haley Snell
  4231 Elm Street
  Detroit, MI 48201
  (313)555-4938
  $ gawk 'BEGIN{FS="\n"; RS=""} {print $1,$4}' data2   # 对每条记录执行一次print $1,$4 #注意不能用^$代替空白行
  Riley Mullen (312)555-1234
  Frank Williams (317)555-9876
  Haley Snell (313)555-4938
  $
  ```

#### b. 内建变量：数据变量

| 变量       | 描述                                                         |
| ---------- | ------------------------------------------------------------ |
| ARGC       | 当前命令行参数个数【不会将程序脚本当成命令行参数的一部分】   |
| ARGIND     | 当前文件在ARGV中的位置                                       |
| ARGV       | 包含命令行参数的数组【不会将程序脚本当成命令行参数的一部分】 |
| CONVFMT    | 数字的转换格式（参见printf语句），默认为%.6g                 |
| ENVIRON    | 当前shell环境变量及其值组成的**关联数组（以变量名文本作为索引）** |
| ERRNO      | 当读取或关闭输入文件发生错误时的系统错误号                   |
| FILENAME   | 用作gawk输入数据的数据文件的文件名                           |
| FNR        | 当前数据文件中的**数据行数**                                 |
| IGNORECASE | 设成非零值时，忽略gawk命令行中出行的字符串的字符大小写       |
| NF         | 数据文件中的**字段总数**                                     |
| NR         | 已处理的输入**记录数**                                       |
| OFMT       | 数字的输出格式，默认为%.6g                                   |
| RLENGTH    | 由match函数所匹配的字符串的长度                              |
| RSTART     | 由match函数所匹配的字符串的起始位置                          |

- ARGC和ARGV变量允许从shell中获得命令行参数的总数以及它们的值。但这可能有点麻烦，因为gawk并不会将程序脚本当成命令
  行参数的一部分

```sh
$ gawk 'BEGIN{print ARGC,ARGV[0],ARGV[1]}' data1.txt 
2 gawk data1.txt
```

> 说明：跟shell变量不同，在脚本中引用gawk变量时，变量名前不加美元符  

- ENVIRON说明

  - 使用关联数组来提取shell环境变量 

  ```sh
  $ gawk '
  BEGIN{
  print ENVIRON["HOME"]
  print ENVIRON["PATH"]
  }'
  
  /home/monomania
  /home/monomania/bin:/home/monomania/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/arm/gcc-linaro-4.9.4-2017.01-x86_64_arm-linux-gnueabihf/bin
  ```

- 当要在gawk程序中跟踪数据字段和记录时，变量FNR、 NF和NR用起来就非常方便

  - NF变量可以让你**在不知道具体位置的情况下指定记录中的最后一个数据字段**  

    ```sh
    $ gawk 'BEGIN{FS=":"; OFS=":"} {print $1,$NF}' /etc/passwd
    root:/bin/bash
    daemon:/usr/sbin/nologin
    bin:/usr/sbin/nologin
    sys:/usr/sbin/nologin
    sync:/bin/sync
    games:/usr/sbin/nologin
    man:/usr/sbin/nologin
    lp:/usr/sbin/nologin
    mail:/usr/sbin/nologin
    ……
    ```

  - NR和FNR的区别

    ```sh
    $ gawk '
    > BEGIN {FS=","}
    > {print $1,"FNR="FNR,"NR="NR}
    > END{print "There were",NR,"records processed"}' data1 data1
    data11 FNR=1 NR=1
    data21 FNR=2 NR=2
    data31 FNR=3 NR=3
    data11 FNR=1 NR=4
    data21 FNR=2 NR=5
    data31 FNR=3 NR=6
    There were 6 records processed
    $
    FNR变量的值在gawk处理第二个数据文件时被重置了，而NR变量则在处理第二个数据文件时继续计数。
    
    结果就是：如果只使用一个数据文件作为输入， FNR和NR的值是相同的；如果使用多个数据文件作为输入， FNR的值会在处理每个数据文件时被重置，而NR的值则会继续计数直到处理完所有的数据文件。
    ```



#### c. 自定义变量

- **在脚本定义变量**

  - 与shell相同
  - 甚至可以处理简单的数值计算【取余%，幂运算^或**】

  ```sh
  $ gawk 'BEGIN{x=4; x= x * 2 + 3; print x}'
  11
  ```

- **在命令行中定义变量**

  - 允许你在正常的代码之外赋值  

  ```sh
  $ cat script1
  BEGIN{FS=","}
  {print $n}
  $ gawk -f script1 n=2 data1
  data12
  data22
  data32
  $ gawk -f script1 n=3 data1
  data13
  data23
  data33
  ```

  > **注意**：使用命令行参数来定义变量值会有一个问题。在你设置了变量后，这个值在代码的**BEGIN部分不可用**。  
  >
  > ```sh
  > $ cat script2
  > BEGIN{print "The starting value is",n; FS=","}
  > {print $n}
  > $ gawk -f script2 n=3 data1
  > The starting value is
  > data13
  > data23
  > data33
  > ```
  >
  > 可以用-v命令行参数来解决这个问题。它允许你在BEGIN代码之前设定变量。在命令行上，-v命令行参数必须放在脚本代码之前  
  >
  > ```sh
  > $ gawk -v n=3 -f script2 data1
  > The starting value is 3
  > data13
  > data23
  > data33
  > $
  > ```



### 5.2 处理数组

gawk支持关联数组（即键值对的形式），索引值可以时任意文本字符串

**定义与使用**

```sh
$ gawk 'BEGIN{
> capital["Illinois"] = "Springfield"
> print capital["Illinois"]
> }'
Springfield

$ gawk 'BEGIN{
> var[1] = 34
> var[2] = 3
> total = var[1] + var[2]
> print total
> }'
37
```

#### a. 遍历数组

```sh
！！重要的是记住这个var变量中存储的是索引值而不是数组元素值！！
for (var in array)
{
statements
}
```
注意：遍历并不是顺序执行的
```sh
$ gawk 'BEGIN{
> var["a"] = 1
> var["g"] = 2
> var["m"] = 3
> var["u"] = 4
> for (test in var)
> {
> print "Index:",test," - Value:",var[test]
> }
> }'
Index: u - Value: 4
Index: m - Value: 3
Index: a - Value: 1
Index: g - Value: 2
```

#### b. 删除数组变量

```sh
delete array[index]
```

删除命令会从数组中删除关联索引值和相关的数据元素值



### 5.3 使用模式

**==模式都是放在程序脚本的左花括号前==**

#### a. 正则表达式

正则表达式必须出现在它要控制的程序脚本的左花括号前  

```sh
$ gawk 'BEGIN{FS=","} /11/{print $1}' data1
data11
```

#### b. 匹配操作符

- **匹配操作符（ matching operator）**：允许将正则表达式限定在记录中的特定数据字段 

- 匹配操作符是波浪线（ ~）。可以指定匹配操作符、数据字段变量以及要匹配的正则表达式。  

- **作用**：gawk程序脚本中经常用它在数据文件中搜索特定的数据元素  

  ```sh
  $ gawk -F: '$1 ~ /rich/{print $1,$NF}' /etc/passwd
  rich /bin/bash
  $
  ```

- 排除匹配（!~）

  ```sh
  $ gawk –F: '$1 !~ /rich/{print $1,$NF}' /etc/passwd
  root /bin/bash
  daemon /bin/sh
  bin /bin/sh
  sys /bin/sh
  --- output truncated ---
  ```

#### c. 数学比较表达式

- x == y：值x等于y

- x <= y：值x小于等于y
- x < y：值x小于y
- x >= y：值x大于等于y
- x > y：值x大于y  

例子：显示所有属于root用户组（组ID为0的系统用户）

```sh
$ gawk -F: '$4 == 0{print $1}' /etc/passwd
root
sync
shutdown
halt
operator
$
```



### 5.4 结构化命令

#### a. if 语句

与C语言类似

```sh
monomania@linux-learn:~/Shell_program$ cat data4
10
5
13
50
34
monomania@linux-learn:~/Shell_program$ gawk '{
if ($1 > 20)
{
    x = $1 * 2
    print x
} else{
    x = $1 / 2
    print x
}}' data4
5
2.5
6.5
100
68
```



#### b. while 语句

```sh
while (condition)
{
	statements
}
```

例子

```sh
$ gawk '{
> total = 0
> i = 1
> while (i < 4)
> {
>     total += $i
>     if (i == 2)
>         break
>     i++
> }
> avg = total / 2
> print "The average of the first two data elements is:",avg
> }' data5
The average of the first two data elements is: 125
The average of the first two data elements is: 136.5
The average of the first two data elements is: 157.5
$
break语句用来在i变量的值为2时从while循环中跳出
```



#### c. do-while 语句

```sh
do
{
	statements
} while (condition)
```



#### d. for 语句

```sh
$ gawk '{
> total = 0
> for (i = 1; i < 4; i++)
> {
>     total += $i
> }
> avg = total / 3
> print "Average:",avg
> }' data5
Average: 128.333
Average: 137.667
Average: 176.667
$
```



### 5.5 格式化打印

gawk中print语句只能通过改OFS分隔符。

解决办法：使用printf

**格式**（与C语言唯一的区别：少了括号）

```sh
printf "format string", var1, var2 . . .
```



### 5.6 内建函数

#### a. 数学函数

| 函数        | 描述                                                         |
| ----------- | ------------------------------------------------------------ |
| atan2(x, y) | x/y的反正切， x和y以**弧度为单位**                           |
| cos(x)      | x的余弦， x以弧度为单位                                      |
| exp()       | x的指数函数                                                  |
| int(x)      | x的整数部分，取靠近零一侧的值【不会四舍五入，类似floor函数】 |
| log(x)      | x的自然对数                                                  |
| rand()      | 比0大比1小的随机浮点值                                       |
| sin(x)      | x的正弦， x以弧度为单位                                      |
| sqrt(x)     | x的平方根                                                    |
| srand(x)    | 为计算随机数指定一个种子值                                   |

#### b. 按位操作

| 函数               | 描述            |
| ------------------ | --------------- |
| and(v1, v2)        | 按位与          |
| compl(val)         | 执行val的补运算 |
| lshift(val, count) | 左移count位     |
| or(v1, v2)         | 按位或          |
| rshift(val, count) | 右移count位     |
| xor(v1, v2)        | 按位异或        |

#### c. 字符串函数

| 函数                       | 描述                                                         |
| -------------------------- | ------------------------------------------------------------ |
| asort(s[, d])              | 将数组s**按数据元素值排序**。索引值会被替换成表示新的排序顺序的连续数字。另外，如果指定了d，则排序后的数组会存储在数组d中 |
| asorti(s[, d])             | 将数组s**按索引值排序**。生成的数组会将索引值作为数据元素值，用连续数字索引来表明排序顺序。另外如果指定了d，排序后的数组会存储在数组d中 |
| gensub(r, s, h[, t])       | 查找变量$0或目标字符串t（如果提供了的话）来匹配**正则表达式r**。如果h是一个以g或G开头的字符串，就用s替换掉匹配的文本。如果h是一个数字，它表示要替换掉第h处r匹配的地方 |
| gsub(r, s [, t])           | 查找变量$0或目标字符串t（如果提供了的话）来匹配正则表达式r。如果找到了，就**全部替换成字符串s** |
| index(s, t)                | **返回字符串t在字符串s中的索引值**，如果没找到的话返回0      |
| length([s])                | **返回字符串s的长度；如果没有指定的话，返回$0的长度**        |
| match(s, r [, a])          | **返回字符串s中正则表达式r出现位置的索引**。如果指定了数组a，它会存储s中匹配正则表达式的那部分 |
| split(s, a [, r])          | **将s用FS字符或正则表达式r（如果指定了的话）分开放到数组a中**。返回字段的总数 |
| sprintf(format, variables) | 用提供的format和variables返回一个类似于printf输出的字符串    |
| sub(r, s [,t])             | 在变量$0或目标字符串t中查找正则表达式r的匹配。如果找到了，就用字符串s**替换掉第一处匹配** |
| substr(s, i [,n])          | 返回s中从索引值i开始的n个字符组成的子字符串。如果未提供n，则返回s剩下的部分 |
| tolower(s)                 | 将s中的所有字符转换成小写                                    |
| toupper(s)                 | 将s中的所有字符转换成大写                                    |



#### d. 时间函数

| 函数                          | 描述                                                         |
| ----------------------------- | ------------------------------------------------------------ |
| mktime(datespec)              | 将一个按YYYY MM DD HH MM SS [DST]格式指定的日期转换成时间戳值 |
| strftime(format [,timestamp]) | 将当前时间的时间戳或timestamp（如果提供了的话）转化格式化日期（采用shell函数date()的格式） |
| systime( )                    | 返回当前时间的时间戳                                         |

```sh
monomania@linux-learn:~/Shell_program$ gawk 'BEGIN{
> date = systime()
> day = strftime("%A, %B %d, %Y", date)
> print day
> }'
星期一, 八月 30, 2021

```



### 5.7 自定义函数

#### 定义与使用

类似shell脚本中的函数

- 要定义自己的函数，必须用function关键字
- 必须在所有代码块之前定义，包括BEGIN代码块  

```sh
function name([variables])
{
	statements
	return value
}
```

return的值，可以作为gawk程序中的一个变量

#### 创建函数库

将函数放置指定文件内，在使用gawk命令时，使用-f选项加载即可



### 5.8 实例







## 6. 使用其他shell





