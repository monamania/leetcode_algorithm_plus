#include <iostream>
#include <string>
#include <cstring>
// https://blog.csdn.net/qq_40123329/article/details/86603466
using namespace std;
// 不存在重复字符返回true

//字符集是ASCII字符
/*
法一
我们还可以通过位运算来减少空间的使用量。 用每一位表征相应位置字符的出现。
对于ASCII字符，我们需要256位，即一个长度为8的int 数组a即可。
这里的关键是要把字符对应的数字，映射到正确的位上去。
比如字符’b’对应的 代码是98，那么我们应该将数组中的哪一位置为1呢？
用98除以32，得到对应数组a的下标： 3。
98对32取模得到相应的位：2。
*/
//----- 力扣不适用
bool isunique_1(const string &str)
{
    int a[8];
    memset(a,0,sizeof(a));
    int length=str.length();
    for(int i=0;i<length;i++)
    {
        int index_of_array=((int)str[i])/32;
        int bit=((int)str[i])%32;
        if(bit==0)
            index_of_array-=1;
        else
            bit-=1;
 
        if(a[index_of_array]&(1<<bit))
            return false;
        a[index_of_array]|=(1<<bit);
    }
    return true;
}
 

/*
法二
如果我们假设字符集是ASCII字符，那么我们可以开一个大小为256的bool数组来表征每个字 符的出现。
数组初始化为false，遍历一遍字符串中的字符，当bool数组对应位置的值为真， 表明该字符在之前已经出现过，
即可得出该字符串中有重复字符。否则将该位置的bool数组 值置为true。
*/
bool isunique_2(const string &str)
{
    bool a[256];
    memset(a,0,sizeof(a));
    int length=str.length();
    for(int i=0;i<length;i++)
    {
        int index=(int)str[i];
        if(a[index])
            return false;
        a[index]=true;
    }
    return true;
}

int main()
{
    string s1="@#$dwfbytj\\)(";
    string s2="@#ghjkl^*)(@@";
    bool unique=isunique_1(s1);
    if(unique)
        cout<<"方法一：s1不存在重复的字符"<<endl;
    else
        cout<<"方法一：s1存在重复的字符"<<endl;
 
    unique=isunique_2(s1);
    if(unique)
        cout<<"方法二：s1不存在重复的字符"<<endl;
    else
        cout<<"方法二：s1存在重复的字符"<<endl;
 
    unique=isunique_1(s2);
    if(unique)
        cout<<"方法一：s2不存在重复的字符"<<endl;
    else
        cout<<"方法一：s2存在重复的字符"<<endl;
 
    unique=isunique_2(s2);
    if(unique)
        cout<<"方法二：s2不存在重复的字符"<<endl;
    else
        cout<<"方法二：s2存在重复的字符"<<endl;
 
    return 0;
}
 