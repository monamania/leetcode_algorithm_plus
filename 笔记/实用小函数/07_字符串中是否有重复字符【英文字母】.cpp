#include <iostream>
#include <string>
#include <cstring>
// https://blog.csdn.net/qq_40123329/article/details/86603466
using namespace std;
// 不存在重复字符返回true
 // 数组方式
bool isunique_1(const string &str)
{
    bool a[26];
    memset(a,0,sizeof(a));
    int length=str.length();
    for(int i=0;i<length;i++)
    {
        int index=str[i]-'a';
        // cout << index << " " << endl;
        if(a[index])
            return false;
        a[index]=true;
    }
    return true;
}
// 另一种写法
bool isunique(const string &str) {
    int hash[26]={0};
    for(int i=0;i<str.size();i++){
        hash[str[i]-'a']++;
        if(hash[str[i]-'a']>=2) return true;
    }
    return false;
}


// （位运算只需要一个整数即可）----- 力扣不适用
bool isunique_2(const string &str)
{
    int check=0;
    int length=str.length();
    for(int i=0;i<length;i++)
    {
        int index=str[i]-'0';
        // cout << index << " " << endl;
        if(check&(1<<index))
            return false;
        check |= (1<<index);
    }
    return true;
}
 
int main()
{
    string s1="abcdef";
    string s2="abcddee";
 
    bool unique=isunique_1(s1);
    if(unique)
        cout<<"方法一：s1不存在重复的字符"<<endl;
    else
        cout<<"方法一：s1存在重复的字符"<<endl;
 
    unique=isunique_2(s1);
    if(unique)
        cout<<"方法二：s1不存在重复的字符"<<endl;
    else
        cout<<"方法二：s1存在重复的字符"<<endl;
 
    unique=isunique_1(s2);
    if(unique)
        cout<<"方法一：s2不存在重复的字符"<<endl;
    else
        cout<<"方法一：s2存在重复的字符"<<endl;
 
    unique=isunique_2(s2);
    if(unique)
        cout<<"方法二：s2不存在重复的字符"<<endl;
    else
        cout<<"方法二：s2存在重复的字符"<<endl;
 
    return 0;
}