// show_bytes.c
#include <stdio.h>
typedef unsigned char * byte_pointer;

// 十六进制的方式显示数据在内存中的存储，从地址低位开始输出
void show_bytes(byte_pointer start, size_t len){
    size_t i;
    for (i = 0; i<len; i++)
        printf(" %.2x", start[i]);
    printf("\r\n");
}

void show_int(int x){
    show_bytes((byte_pointer) &x, sizeof(int));
}

void show_float(float x){
    show_bytes((byte_pointer) &x, sizeof(float));
}
// 打印指针
void show_pointer(void* x){
    show_bytes((byte_pointer) &x, sizeof(void*));
}


int main(int argc, char const *argv[])
{
    int x = 12345;  // 0x00 0x00 0x30 0x39

    show_int(x);    // 输出：0x39 0x30 0x00 0x00----最低有效位先输出，故而此为小端
    return 0;
}