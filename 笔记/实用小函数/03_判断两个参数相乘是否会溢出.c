// show_bytes.c
#include <stdio.h>
typedef unsigned char * byte_pointer;

// 参考《深入理解计算机系统》
int tmult_ok(int x, int y){
    int p = x*y;
    /* Either x is zero, or dividing p by x gives y */
    return !x || p/x == y;
}



int main(int argc, char const *argv[])
{
    int x = 1234545345;  // 0x00 0x00 0x30 0x39
    int y = 1234534534; 

    if (tmult_ok(x, y)){
        printf("ok 不会溢出\n");
    }
    else{
        printf("erro 溢出\n");
    }
    return 0;
}