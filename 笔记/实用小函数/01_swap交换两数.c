#include <stdio.h>
// #define NDEBUG
#include <assert.h>

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


/* 常规手段 */
void swap(SElemType *a,SElemType *b) //交換兩個變數
{
    SElemType temp = *a;
    *a = *b;
    *b = temp;
}

/** 利用C 语言的按位操作 
 * 原理：
 * a = a ^ b;  //a = a0 ^ b0;
 * b = b ^ a;  //b = b0 ^ a0 ^ b0 = a0 ——>交换成功
 * a = a ^ b;  //a = a0 ^ b0 ^ a0 = b0 ——>交换成功
 * 
 * 利用的性质：x ^ x ^ y = y , x ^ y ^ x = y
*/
void inplace_swap(int *x, int *y){
    *y = *x ^ *y;
    *x = *x ^ *y;
    *y = *x ^ *y;
}


int main(){
    int a = 1,b = 2;
    printf("swap before》a: %d, b: %d\n",a,b);

    swap(&a,&b);

    printf("swap after》a: %d, b: %d\n",a,b);


    printf("inplace_swap before》a: %d, b: %d\n",a,b);

    inplace_swap(&a,&b);

    printf("inplace_swap after》a: %d, b: %d\n",a,b);
    return 0;
}

