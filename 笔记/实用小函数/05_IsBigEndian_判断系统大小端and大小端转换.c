#include <stdio.h>

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */
typedef unsigned char BOOL;

/**
 *判断大小端
 */
BOOL IsBigEndian()
{
    union NUM
    {
        int a;
        char b;
    }num;
    num.a = 0x1234;
    if( num.b == 0x12 )
    {
        return TRUE;
    }
    return FALSE;
}

/**
 * 64位小端转换为大端---由于网络序为大端
 */
long unsigned int hton_ll(long unsigned int data)
{
    long host_h;
    long host_l;
    long unsigned int temp;
 
    host_l = data & 0xffffffff;
    host_h = (data >> 32) & 0xffffffff;
 
    temp = htonl(host_l);
    temp = (temp << 32) | htonl(host_h);
 
    return temp;
}


int main(int argc, char** argv){
    if (IsBigEndian()){
        printf("It is big endian!----高字节存入低地址，低字节存入高地址\r\n");
        printf("对于0x12345678正读：0x12 34 56 78 对应地址 0x1000 0x1001 0x1002 0x1003\r\n");
    }
    else{
        printf("It is little endian!----低字节存入低地址，高字节存入高地址\r\n");
        printf("对于0x12345678逆读：0x78 56 34 12 对应地址 0x1001 0x1001 0x1002 0x1003\r\n");
    }
}