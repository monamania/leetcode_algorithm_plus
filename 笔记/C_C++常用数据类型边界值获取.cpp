#include <iostream>
#include <limits>
#include <limits.h>
// #include <Integer.h>

using namespace std;

int main()
{
	int a;
	a = 1 << 31; 
	cout << a << endl; //  -2147483648  int 的最小
	a = 1 << 31 - 1; 
	cout << a << endl; //  2147483647   int 的最大
	// cout << Integer.MAX_VALUE << endl;
	// cout << Integer.MIN_VALUE << endl;

	cout << "\n\n*************** C ****************" << endl;
	cout << "整型：" << endl;
	cout << "short_min:" << SHRT_MIN << endl;
	cout << "int_min:" << INT_MIN << endl;
	cout << "int32_max:" << INT32_MAX << endl;  // 异曲同工
	cout << "long_min:" << LONG_MIN << endl;
	cout << "long long_min:" << LLONG_MIN << endl;
	// unsigned 类型没有MIN下界定义（均为0）
	cout << "unsigned short_max:" << USHRT_MAX << endl;
	cout << "unsigned int_max:" << UINT_MAX << endl;
	cout << "unsigned long_max:" << ULONG_MAX << endl;
	cout << "unsigned long long_max:" << ULLONG_MAX << endl;
	cout << "浮点型：" << endl;
	cout << "float_min:" << FLT_MIN << endl;
	cout << "float_max:" << FLT_MAX << endl;
	cout << "double_min:" << DBL_MIN << endl;
	cout << "double_max:" << DBL_MAX << endl;
	cout << "long double_min:" << LDBL_MIN << endl;
	cout << "long double_max:" << LDBL_MAX << endl;

	cout << "\n\n*************** C++ ****************" << endl;
	cout << "整型：" << endl;
	cout << "int_min:" << numeric_limits<short>::min() << endl;
	cout << "int_min:" << numeric_limits<int>::min() << endl;
	cout << "long_min:" << numeric_limits<long>::min() << endl;
	cout << "long long_min:" << numeric_limits<long long>::min() << endl;
	// unsigned 类型，有上下界函数max()和min()，这里仍旧用上界表示，下界min()均返回0
	cout << "unsigned short_max:" << numeric_limits<unsigned short>::max() << endl;
	cout << "unsigned int_max:" << numeric_limits<unsigned int>::max() << endl;
	cout << "unsigned long_max:" << numeric_limits<unsigned long>::max() << endl;
	cout << "unsigned long long_max:" << numeric_limits<unsigned long long>::max() << endl;
	cout << "浮点型：" << endl;
	cout << "float_min:" << numeric_limits<float>::min() << endl;
	cout << "float_max:" << numeric_limits<float>::max() << endl;
	cout << "double_min:" << numeric_limits<double>::min() << endl;
	cout << "double_max:" << numeric_limits<double>::max() << endl;
	cout << "long double_min:" << numeric_limits<long double>::min() << endl;
	cout << "long double_max:" << numeric_limits<long double>::max() << endl;
    return 0;
}

