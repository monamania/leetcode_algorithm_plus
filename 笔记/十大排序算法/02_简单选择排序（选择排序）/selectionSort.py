# !/usr/bin/env python
# -*- coding: utf-8 -*-

def selectionSort(arr):
    for i in range(len(arr) - 1):
        # 记录最小数的索引，最后一个数就不用了，所以减1
        minIndex = i
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[minIndex]:
                minIndex = j
        # 如果i不是最小数时，i与minIndex的值进行交换
        if i != minIndex:
            # 交换
            arr[i], arr[minIndex] = arr[minIndex], arr[i]
    return arr


if __name__ == '__main__':
    arr = [22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70]
    arr = selectionSort(arr)
    print(arr)