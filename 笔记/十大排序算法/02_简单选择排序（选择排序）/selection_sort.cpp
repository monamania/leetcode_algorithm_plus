#include <iostream>
using namespace std;

template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void selection_sort(T arr[], int len) {
        // i为有序区的末尾元素，j为无序区的起始元素，minIndex为无序区的最小元素位置
    // len-1就好，最后只剩一个的时候，其实不用排序了
        for (int i = 0; i < len-1; i++){
            int minIndex = i;
            for (int j = i+1; j < len; ++j)  //走访未排序的元素
            {
                if (arr[j] < arr[minIndex])  //找到最小值
                    minIndex = j; 
            }
            if (i != minIndex)
                swap(arr[minIndex], arr[i]);
        }
}

int main() {
        int arr[] = { 61, 17, 29, 22, 34, 60, 72, 21, 50, 1, 62 };
        int len = (int) sizeof(arr) / sizeof(*arr);
        selection_sort(arr, len);
        for (int i = 0; i < len; i++)
                cout << arr[i] << ' ';
        cout << endl;

        float arrf[] = { 17.5, 19.1, 0.6, 1.9, 10.5, 12.4, 3.8, 19.7, 1.5, 25.4, 28.6, 4.4, 23.8, 5.4 };
        len = (float) sizeof(arrf) / sizeof(*arrf);
        selection_sort(arrf, len);
        for (int i = 0; i < len; i++)
                cout << arrf[i] << ' '<<endl;
        return 0;
}
