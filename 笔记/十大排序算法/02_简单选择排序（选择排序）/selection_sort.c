#include <stdio.h>
// #define NDEBUG
#include <assert.h>

#define OK 1
#define ERROR 0
#define TRUE true
#define FALSE false
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

void swap(SElemType *a,SElemType *b){
    SElemType tmp = *a;
    *a = *b;
    *b = tmp;
}

Status selection_sort(SElemType arr[], int len){
    int i,j;
    int minIndex;

    for(i=0;i<len-1;i++){
        minIndex = i;
        for(j = i+1; j < len; j++){
            if (arr[j] < arr[minIndex])
                minIndex = j;
        }
        if (minIndex != i)
            swap(&arr[i], &arr[minIndex]);
    }
}


int main(){
    int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
    int len = (int) sizeof(arr) / sizeof(*arr);
    selection_sort(arr, len);
    int i;
    for (i = 0; i < len; i++)
            printf("%d ", arr[i]);
    return 0;
}
