#include <stdio.h>
// #define NDEBUG
#include <assert.h>

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


void swap(SElemType *a,SElemType *b) //交換兩個變數
{
    SElemType temp = *a;
    *a = *b;
    *b = temp;
}

/**
 * @brief 冒泡排序   由小到大
 */
Status bubble_sort(SElemType arr[], int len){
    int i, j;  // i表示已经进行过排序的元素个数；j 表示进行排序时的元素
    short flag;
    // int tmp;   //交换数据时候使用

    for (i = 0; i < len - 1; i++)
        flag = FALSE;  // 表示本趟冒泡是否法身交换的标志
        for (j = 0; j < len-1-i ; j++){
            if (arr[j] > arr[j+1]){
                swap(&arr[j], &arr[j+1]);
                flag = TRUE;
            }
        }
    if(flag == FALSE){
		return OK;  //本趙遍历后没有发生交换，说明表已经有序
	}
    return OK;
}



int main(){
    int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
    int len = (int) sizeof(arr) / sizeof(*arr);
    bubble_sort(arr, len);
    int i;
    for (i = 0; i < len; i++)
            printf("%d ", arr[i]);
    return 0;
}
