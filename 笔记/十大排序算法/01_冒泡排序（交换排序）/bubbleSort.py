# !/usr/bin/env python
# -*- coding: utf-8 -*-

def bubbleSort(arr):
    # i表示已经进行过排序的元素个数；j 表示进行排序时的元素
    for i in range(1, len(arr)):
        for j in range(0, len(arr)-i):
            if arr[j] > arr[j+1]:
                # 交换
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr



if __name__ == '__main__':
    arr = [22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70]
    arr = bubbleSort(arr)
    print(arr)