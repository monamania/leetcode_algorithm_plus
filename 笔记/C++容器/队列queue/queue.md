[参考资料](https://zhuanlan.zhihu.com/p/138612411)

[参考代码](../../../刷题/简单题/104_二叉树的最大深度/maxDepth（广度优先BFS）.cpp)

--- 
# 准备
> 队列（Queue）--先进先出FIFO，后面进，前面出

**包含头文件**
```C++
#include <queue>
std::queue<int> myQueue;
```

**常见操作**
- empty
- size
- front
- back
- push_back
- pop_front

# 使用

## 1 队列初始化

```C++
std::deque<int> mydeck(3, 100);        // 双端队列里初始化3个元素，都是100
std::list<int> mylist(2, 200);         // list 容器里初始化2个元素，都是200
std::queue<int> first;                 // 初始化一个空队列
std::queue<int> second(mydeck);        // 复制 mydeck 的内容初始化队列
std::queue<int, std::list<int> > third; // 初始化空队列，底层使用 list 容器
std::queue<int, std::list<int> > fourth(mylist);    // 复制 mylist 的内容初始化队列，底层使用 list 容器
std::cout << "size of first: " << first.size() << std::endl; // 0
std::cout << "size of second: " << second.size() << std::endl; // 3
std::cout << "size of third: " << third.size() << std::endl; // 0
std::cout << "size of fourth: " << fourth.size() << std::endl; // 2
```

## 2 判断队列是否为空

```C++
std::queue<int> myqueue1;
bool empty1 = myqueue1.empty(); // true
std::queue<int> myqueue2({100,100});
bool empty2 = myqueue2.empty(); // false
```

## 3 获得元素个数

```C++
std::queue<int> myints;
std::cout << "0. size: " << myints.size() << std::endl; // 输出：0
for (int i = 0; i < 5; i++) myints.push(i);
std::cout << "1. size: " << myints.size() << std::endl; // 输出：5
myints.pop();
std::cout << "2. size: " << myints.size() << std::endl; // 输出：4
```

## 4 返回头元素引用
头元素就是最先加入队列的元素，这个元素也是下次pop出队的元素

```C++
std::queue<int> myqueue3;
myqueue3.push(77);
myqueue3.push(66);
int& a1 = myqueue3.front(); // 77
int a2 = myqueue3.front(); // 77
myqueue3.front() = 88; // 给头元素77赋值为88
std::cout << "front:" << myqueue3.front() << std::endl; // 输出：88
```

## 5 返回末尾元素引用
末尾元素就是最后加入队列的元素，这个元素也是最新push入队的元素。
```C++
std::queue<int> myqueue4;
myqueue4.push(77);
myqueue4.push(66);
int& b1 = myqueue4.back(); // 66
int b2 = myqueue4.back(); // 66
myqueue4.back() = 33; // 给末尾元素66赋值为33
std::cout << "front:" << myqueue4.front() << std::endl; // 输出：33
```

## 6 入队/出队
```C++
std::queue<int> myqueue5;
myqueue5.push(55); // 无返回值，入队了一个55，size()==1
myqueue5.push(45); // size()==2
myqueue5.pop(); // 无返回值，出队了一个55，size()==1
```

## 7 (C++11)另一种入队，其底层容器调用了emplace_back方法。
```C++
myqueue5.emplace(45);
```

## 8 (C++11)交换
```C++
std::queue<int> teeth;
teeth.emplace(4); teeth.emplace(7);
std::queue<int> bags;
bags.emplace(4); bags.emplace(7); bags.emplace(7);
bags.swap(teeth);
std::cout << teeth.size() << std::endl; //输出：3
std::cout << bags.size() << std::endl; //输出：2
```

## 9 运算符 = != > >= < <=
```C++
// [==]当两个队列front()内容一致，返回true
std::queue<int> q1, q2;
bool ret = q1 == q2; // ret为true

// [!=]当两个队列元素front()不相等，返回true
std::queue<int> q3, q4;
q3.push(1);
bool ret2 = q3 != q4; // ret2为true

// [>]左边队列的front()的元素大于右边队列pop的元素，则返回true.
std::queue<int> q5, q6;
q5.push(1); q5.push(2); q5.push(2);
q6.push(0); q6.push(2);
bool ret3 = q5 >= q6; // ret3为true，因为1大于0

```