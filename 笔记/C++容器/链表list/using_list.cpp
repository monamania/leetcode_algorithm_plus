#include<iostream>
#include<list>
using namespace std;
///https://www.bilibili.com/video/BV1KJ411k7fX?p=51&spm_id_from=pageDriver

// 遍历
void visit(list<int>& arr, string const str){
    list<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历--不支持
// void visit2(list<int>& arr, string const str){
//     cout << str << ": ";
//     for(int i; i < arr.size(); ++i){
//         cout << arr[i] << " ";
//     }
//     cout<<endl;
// }



void showMenu(string const str){
    cout<<"============= "<<str<<" ============="<<endl;
}

int main(int argc, char** argv){
    /*初始化-----双向链表*/
    showMenu("初始化");
    int szint[5] = {2,3,7,8,6};
    list<int> list_int;
    list<int> list_int1 = {2,3,7,8};
    visit(list_int1,"list_int1");
    list<int> list_int2({2,3,7,8});
    visit(list_int2, "list_int2");
    // 复制拷贝操作
    list<int> list_int3(list_int2);
    visit(list_int3, "list_int3");
    list<int> list_int4 = list_int2;
    visit(list_int4, "list_int4");
    // 用静态数组的值来初始化
    list<int> list_int5(szint, szint + (int)sizeof(szint)/sizeof(*szint));
    visit(list_int5, "list_int5");

    list<int> list_int6(4, 100);
    visit(list_int6, "list_int6");

     /*基本操作*/
    showMenu("基本操作 针对list_int5");
    //1 元素个数
    cout<<list_int5.size()<<endl;

    //2 获取第一个和最后一个元素
    cout<<list_int5.front()<<endl;
    cout<<list_int5.back()<<endl;

    //3 判断是否为空
    bool emptyFlag = list_int5.empty();

    //4 数据逆序
    list_int5.reverse();
    visit(list_int5, "list_int4-reverse");

    //5 排序
    list_int5.sort();
    visit(list_int5, "list_int4-sort");

    //6 交换数据
    list_int5.swap(list_int4);
    visit(list_int4, "list_int4_swap");
    visit(list_int5, "list_int5_swap");

    //7 两个list合并
    list_int5.splice(list_int5.begin(), list_int4);
    visit(list_int5, "list_int5_splice");

    //8 去除附近的重复元素
    list_int5.sort();
    list_int5.unique();
    visit(list_int5, "list_int5_unique");
    // visit(list_int5.splice(list_int5.begin(), list_int4), "list_int5_swap");



    /*数据插入*/
    showMenu("数据插入");
    visit(list_int, "list_int初始");
    list_int.push_back(1);
    list_int.push_front(1);
    visit(list_int, "list_int-push_back(1)");
    list_int.insert(list_int.begin(),3);
    visit(list_int, "list_int-insert()");
    list_int.insert(list_int.begin(),3, 666);
    visit(list_int, "list_int-insert()2");
    int szint2[] = {15, 25, 35};
    list_int.insert(list_int.end(),szint2, szint2 + (int)sizeof(szint2)/sizeof(szint2[0]));
    visit(list_int, "list_int+普通数组");

    /*数据删除*/
    showMenu("数据删除");
    visit(list_int, "list_int初始");
    list_int.pop_back();
    visit(list_int, "list_int-pop_back");
    list_int.pop_front();
    visit(list_int, "list_int-pop_front");
    list_int.erase(list_int.begin());
    visit(list_int, "list_int-erase");
    // list_int.erase(list_int.begin(), list_int.end());
    // visit(list_int, "list_int-erase2");
    list_int.clear();
    visit(list_int, "list_int-clear");


    return 0;
}