#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include <curses.h>   // 清屏  sudo apt-get install libncurses5-dev  sudo apt-get install ncurses-dev
#include <term.h>
//https://blog.csdn.net/qq_14821541/article/details/54706999

using namespace std;

int main() {
    // setupterm(NULL, STDOUT_FILENO, NULL);    // 这个函数只在初始化的时候调用一次
    // char const* clear_seq = tigetstr("clear");
    // fputs(clear_seq, stdout);
    system("clear");

    int x = 5, y = 5;
    // 初始化x*y的二维数组，并赋值为0---虽然默认初始化就是0
    vector<vector<int>> A(x, vector<int>(y, 0));
    for (int i = 0; i <5; i++) {
        for (int j = 0; j <5; j++) {
            cout << A[i][j] << " ";
        }
        cout << endl;
    }

    A = {{1,4,7,11,15},{2,5,  8, 12, 19},{3,  6,  9, 16, 22},{10, 13, 14, 17, 24},{18, 21, 23, 26, 30} };
    cout << "Size:" << A.size() << endl;
    for (int i = 0; i <5; i++) {
        for (int j = 0; j <5; j++) {
            cout << A[i][j] << " ";
        }
        cout << endl;
    }

    // 其他操作同一维数组
    A.push_back({0,2,2,2,2});
    cout << "Size:" << A.size() << endl;
    for (int i = 0; i <A.size(); i++) {
        for (int j = 0; j <A[0].size(); j++) {
            cout << A[i][j] << " ";
        }
        cout << endl;
    }
    // system("pause");
    return 0;
}



