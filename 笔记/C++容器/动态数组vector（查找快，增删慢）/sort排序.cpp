#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

/*******************************************/
/****************** 从小到大 ****************/
/*******************************************/
int main() {
	int a[] = {8,6,2,9,3,5,4,1,7,10};
 	vector<int> arr(a, a+5);
	sort(arr.begin(),arr.end()); 
 	for(int i = 0; i <arr.size(); ++i){
 		cout <<arr[i] << " ";
	}		 
	return 0 ;
}


/*******************************************/
/****************** 从大到小 ****************/
/*******************************************/
// sort默认排序从小到大，但可以自己写函数或者使用greater<int>()，或者自定义排序规则
// 方法一：使用greater<int>()
int main() {
	int a[] = {8,6,2,9,3,5,4,1,7,10};
 	vector<int> arr(a, a+5);
	sort(arr.begin(),arr.end(),greater<int>()); 
 	for(int i = 0; i <arr.size(); ++i){
 		cout <<arr[i] << " ";
	}		 
	return 0 ;
}
//方法二： 或者自己定义函数，大的写在前面
bool cmp_max(int x,int y){
	return x > y;
}

int main() {
	int a[] = {8,6,2,9,3,5,4,1,7,10};
 	vector<int> arr(a, a+5);
	sort(arr.begin(),arr.end(),cmp_max); 
 	for(int i = 0; i <arr.size(); ++i){
 		cout <<arr[i] << " ";
	}		 
	return 0 ;
 }

 //方法三：使用sort排序后，使用reverse()  注：reverse() 的将元素倒置，但不排列

int main() {
	int a[] = {8,6,2,9,3,5,4,1,7,10};
 	vector<int> arr(a, a+5);
 	sort(arr.begin(),arr.end());
	reverse(arr.begin(),arr.end());
 	for(int i = 0; i <arr.size(); ++i){
 		cout <<arr[i] << " ";
	}		 
	return 0 ;
}