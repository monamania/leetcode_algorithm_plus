#include<iostream>
#include<vector>
using namespace std;
//https://www.bilibili.com/video/BV1KJ411k7fX?p=50&spm_id_from=pageDriver


// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit(vector<int>& arr, string const str){
    vector<int>::iterator it;
    cout << str << ": ";
    for(it=arr.begin();it!=arr.end();++it){
        cout << *it <<" ";
    }
    cout<<endl;
}
// 遍历
// template<typename T> //整数或浮点数皆可使用,若要使用类(class)或结构体(struct)时必须重载大于(>)运算符
void visit2(vector<int>& arr, string const str){
    cout << str << ": ";
    for(int i; i < arr.size(); ++i){
        cout << arr[i] << " ";
    }
    cout<<endl;
}



void showMenu(string const str){
    cout<<"============= "<<str<<" ============="<<endl;
}

int main(int argc, char** argv){
    /*初始化*/
    showMenu("初始化");
    int szint[5] = {2,3,7,8,6};
    vector<int> arr_int;
    vector<int> arr_int1 = {2,3,7,8};
    visit(arr_int1,"arr_int1");
    vector<int> arr_int2({2,3,7,8});
    visit(arr_int2, "arr_int2");
    // 复制拷贝操作---以下都是一样的
    vector<int> arr_int3(arr_int2);
    vector<int> arr_int3(arr_int2.begin(), arr_int2.end());
    visit(arr_int3, "arr_int3");
    vector<int> arr_int4 = arr_int2;
    visit(arr_int4, "arr_int4");
    // 用静态数组的值来初始化
    vector<int> arr_int5(szint, szint + (int)sizeof(szint)/sizeof(*szint));
    // vector<int> arr_int5(begin(szint), end(szint));
    visit(arr_int5, "arr_int5");

    vector<int> arr_int6(4, 100);
    visit(arr_int6, "arr_int6");
    


    /*基本操作*/
    showMenu("基本操作");
    //1 获取指定位置元素
    cout<<arr_int1[0]<<endl;
    cout<<arr_int1.at(1)<<endl;

    //2 元素个数
    cout<<arr_int5.size()<<endl;

    //3 获取第一个和最后一个元素
    cout<<arr_int5.front()<<endl;
    cout<<arr_int5.back()<<endl;

    //4 判断是否为空
    bool emptyFlag = arr_int5.empty();

    //5 交换数据
    arr_int5.swap(arr_int4);
    visit(arr_int4, "arr_int4_swap");
    visit(arr_int5, "arr_int5_swap");
    


    /*数据插入*/
    showMenu("数据插入");
    visit(arr_int, "arr_int初始");
    arr_int.push_back(1);
    visit(arr_int, "arr_int-push_back(1)");
    arr_int.insert(arr_int.begin(),3);
    visit(arr_int, "arr_int-insert()");
    arr_int.insert(arr_int.begin(),3, 666);
    visit(arr_int, "arr_int-insert()2");
    int szint2[] = {15, 25, 35};
    arr_int.insert(arr_int.end(),szint2, szint2 + (int)sizeof(szint2)/sizeof(szint2[0]));
    visit(arr_int, "arr_int+普通数组");

    /*数据删除*/
    showMenu("数据删除");
    visit(arr_int, "arr_int初始");
    arr_int.pop_back();
    visit(arr_int, "arr_int-pop_back");
    arr_int.erase(arr_int.begin());
    visit(arr_int, "arr_int-erase");
    arr_int.erase(arr_int.begin(), arr_int.end()-2);
    visit(arr_int, "arr_int-erase2");
    arr_int.clear();
    visit(arr_int, "arr_int-clear");

    /* 数据截取 */
    //1. 初始化截取
    vector<int> Arrs {1,2,3,4,5,6,7,8,9}; // 假设有这么个数组, 要截取中间第二个元素到第四个元素：2，3，4
    vector<int>::const_iterator First = Arrs.begin() + 1; // 找到第二个迭代器
    vector<int>::const_iterator Second = Arrs.begin() + 3; // 找到第三个迭代器
    vector<int> Arrs2(First, Second); // 将值直接初始化到Arrs2
    //2. assign()功能函数实现截取
    vector<int> Arrs {1,2,3,4,5,6,7,8,9}; // 假设有这么个数组, 要截取中间第二个元素到第四个元素：2，3，4
    vector<int>::const_iterator Fist = Arrs.begin() + 1; // 找到第二个迭代器
    vector<int>::const_iterator Second = Arrs.begin() + 3; // 找到第三个迭代器
    vector<int> Arr2;
    Arr2.assign(First, Second); //使用assign() 成员函数将Arrs对应位置的值存入Arrs2数组中

    /* 数组合并 */
    vector<int> nums0 = {1,2,3};
    vector<int> nums1 = {4,5,6,7};
    vector<int> nums2 = {8,9};
    nums0.insert(nums0.end(),nums1.begin(),nums1.end());// 将nums1数组合并到nums0数组末尾
    nums2.insert(nums2.begin(),nums0.begin(),nums0.end());// 将nums0数组合并到nums2数组前面

    return 0;

}