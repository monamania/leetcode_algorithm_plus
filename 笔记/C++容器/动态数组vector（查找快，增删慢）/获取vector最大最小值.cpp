// Visual Studio 2015编译通过
// 以std::vector容器为例
 
#include <iostream>
#include <stdlib.h>
#include <algorithm> // std::minmax_element
#include <vector>    // std::vector
 
int main(int argc, char *argv[])
{
	std::vector<double> vec{ 2.1, 1.1, 4.1, 3.1 };
 
	// 同时求最小最大值
	// 返回std::pair<std::vector<double>::iterator, std::vector<double>::iterator>类型的值，直接用auto代替
	auto v = std::minmax_element(vec.begin(), vec.end());
	std::cout << *v.first << std::endl;  // 1.1 - 最小值
	std::cout << *v.second << std::endl; // 4.1 - 最大值
 
	// 单求最小值
	// 返回std::vector<double>::iterator类型的值
	auto min = std::min_element(vec.begin(), vec.end());
	std::cout << *min << std::endl; // 1.1
 
	// 单求最大值
	// 返回std::vector<double>::iterator类型的值
	auto max = std::max_element(vec.begin(), vec.end());
	std::cout << *max << std::endl; // 4.1
 
	system("pause");
	return 0;
}