[C++ STL unordered_set容器完全攻略](http://c.biancheng.net/view/7250.html)
[C++ STL 之 unordered_set 使用（包括unordersd_map）](https://blog.csdn.net/qq_32172673/article/details/85160180)

注意，由于哈希容器直到 C++ 11 才被正式纳入 C++ 标准程序库，而在此之前，“民间”流传着 hash_set、hash_multiset、hash_map、hash_multimap 版本，不过该版本只能在某些支持 C++ 11 的编译器下使用（如 VS），有些编译器（如 gcc/g++）是不支持的。
![](https://img-blog.csdnimg.cn/20210104235134572.png)