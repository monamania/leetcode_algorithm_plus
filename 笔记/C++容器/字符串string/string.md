https://www.bilibili.com/video/BV1KJ411k7fX?p=53&spm_id_from=pageDriver


# [C++判断一个字符是否为字母或者数字的方法](https://blog.csdn.net/snowcatvia/article/details/96862939)
## 普通判断
1、范围确定

判断一个字符是否为：

小写字母：字符大于等于a，小于等于z；

大写字母：字符大于等于A，小于等于Z；

数字：字符大于等于0，小于等于9；
```c++
#include<iostream>
using namespace std;
 
int main()
{
    char c;
    cin >> c;
    if(c <= 'z' && c >= 'a')
        cout << "c是小写字母" << c << endl;
    else if(c <= 'Z' && c >= 'A')
        cout << "c是大写字母" << c << endl;
    else if(c <= '9' && c >= '0')
        cout << "c是数字" << c << endl;
    return 0;
}
```
## stl库函数判断

字母（不区分大小写）：isalpha();

大写字母：isupper();

小写字母：islower();

数字：isdigit();

字母和数字：isalnum();

## 大小写字母转化：

（1）转化为大写：toupper();

（2）转化为小写：tolower();