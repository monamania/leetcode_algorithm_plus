#include<iostream>
#include <string>
// #define NDEBUG
#include <assert.h>
#include <boost/algorithm/string.hpp>
#include <functional>
#include <iterator>
#include <algorithm> // std::minmax_element
using namespace boost;
using namespace std;


void removeExtraSpaces(string& s){
    // 利用快慢指针实现移除空格操作----慢的用来记录，快的指针用来遍历
    int slow=0,fast=0;
    // 过滤掉字符串前面的空格
    while(s.length()>0 && fast < s.length() && s[fast] == ' ') {
        fast++;
    }
    // 后续重复的空格都删掉
    for(;s.length()>0 && fast < s.length();fast++) {
        if(fast - 1 > 0 && s[fast-1] == s[fast] && s[fast] == ' ') {
            continue;
        } else {
            s[slow++] = s[fast];
        }
    }
    // 删除后面的空格---为什么不用循环，是由于上一步已经去掉重复的空格了
    if(slow - 1 > 0 && s[slow-1] == ' ') {
        s.resize(slow - 1);
    } else {
        s.resize(slow); 
    }
}

int main(int argc, char* argv[]){
    string s = "  13 2   24 1   ";
    cout << s.length() << endl;
    removeExtraSpaces(s);
    cout << s << ":" << s.length() << endl;
}