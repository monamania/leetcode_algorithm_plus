<!-- TOC -->

- [1. 什么是优先级队列呢？](#1-什么是优先级队列呢)
  - [1.1. 那么它是如何有序排列的呢？](#11-那么它是如何有序排列的呢)
  - [1.2. 什么是堆呢？](#12-什么是堆呢)
- [2. c++优先队列(priority_queue)的使用](#2-c优先队列priority_queue的使用)
- [3. 例子](#3-例子)
  - [3.1. 基本例子](#31-基本例子)
  - [3.2. pari的比较，先比较第一个元素，第一个相等比较第二个](#32-pari的比较先比较第一个元素第一个相等比较第二个)
  - [3.3. 对于自定义类型](#33-对于自定义类型)

<!-- /TOC -->
# 1. 什么是优先级队列呢？

其实就是一个披着队列外衣的堆，因为优先级队列对外接口只是从队头取元素，从队尾添加元素，再无其他取元素的方式，看起来就是一个队列。而且优先级队列内部元素是自动依照元素的权值排列。

## 1.1. 那么它是如何有序排列的呢？

缺省情况下priority_queue利用max-heap（大顶堆）完成对元素的排序，这个大顶堆是以vector为表现形式的complete binary tree（完全二叉树）。

## 1.2. 什么是堆呢？

堆是一棵完全二叉树，树中每个结点的值都不小于（或不大于）其左右孩子的值。 如果父亲结点是大于等于左右孩子就是大顶堆，小于等于左右孩子就是小顶堆。

所以大家经常说的大顶堆（堆头是最大元素），小顶堆（堆头是最小元素），如果懒得自己实现的话，就直接用priority_queue（优先级队列）就可以了，底层实现都是一样的，从小到大排就是小顶堆，从大到小排就是大顶堆。



# 2. c++优先队列(priority_queue)的使用

既然是队列那么先要包含头文件#include <queue>, 他和queue不同的就在于我们可以自定义其中数据的优先级, 让优先级高的排在队列前面,优先出队。

优先队列具有队列的所有特性，包括基本操作，只是在这基础上添加了内部的一个排序，它本质是一个堆实现的

```
和队列基本操作相同:

    top     访问队头元素
    empty   队列是否为空
    size    返回队列内元素个数
    push    插入元素到队尾 (并排序)
    emplace 原地构造一个元素并插入队列
    pop     弹出队头元素
    swap    交换内容

```

定义：`priority_queue<Type, Container, Functional>`

Type 就是数据类型，Container 就是容器类型（Container必须是用数组实现的容器，比如vector,deque等等，但不能用 list。STL里面默认用的是vector），Functional 就是比较的方式，当需要用自定义的数据类型时才需要传入这三个参数，使用基本数据类型时，只需要传入数据类型，默认是大顶堆

一般情况：
```C++
//升序队列---小根堆
priority_queue <int,vector<int>,greater<int> > q;
//降序队列---大根堆
priority_queue <int,vector<int>,less<int> >q;

//greater和less是std实现的两个仿函数（就是使一个类的使用看上去像一个函数。其实现就是类中实现一个operator()，这个类就有了类似函数的行为，就是一个仿函数类了）

```
参考力扣题：[347_前k个高频元素](https://leetcode-cn.com/problems/top-k-frequent-elements/comments/)


# 3. 例子
## 3.1. 基本例子
```C++
#include<iostream>
#include <queue>
using namespace std;
int main() 
{
    //对于基础类型 默认是大顶堆
    priority_queue<int> a; 
    //等同于 priority_queue<int, vector<int>, less<int> > a;
    
  
    priority_queue<int, vector<int>, greater<int> > c;  //这样就是小顶堆
    priority_queue<string> b;

    for (int i = 0; i < 5; i++) 
    {
        a.push(i);
        c.push(i);
    }
    cou << "大根堆：" << endl; 
    while (!a.empty()) 
    {
        cout << a.top() << ' ';
        a.pop();
    } 
    cout << endl;

    cou << "小根堆：" << endl; 
    while (!c.empty()) 
    {
        cout << c.top() << ' ';
        c.pop();
    }
    cout << endl;

    b.push("abc");
    b.push("abcd");
    b.push("cbd");
    while (!b.empty()) 
    {
        cout << b.top() << ' ';
        b.pop();
    } 
    cout << endl;
    return 0;
}

```
输出：
```
大根堆：
4 3 2 1 0
小根堆：
0 1 2 3 4
cbd abcd abc
```

## 3.2. pari的比较，先比较第一个元素，第一个相等比较第二个
```C++
#include <iostream>
#include <queue>
#include <vector>
using namespace std;
int main() 
{
    priority_queue<pair<int, int> > a;
    pair<int, int> b(1, 2);
    pair<int, int> c(1, 3);
    pair<int, int> d(2, 5);
    a.push(d);
    a.push(c);
    a.push(b);
    while (!a.empty()) 
    {
        cout << a.top().first << ' ' << a.top().second << '\n';
        a.pop();
    }
}
```
输出：
```
2 5
1 3
1 2
```

## 3.3. 对于自定义类型
```C++
#include <iostream>
#include <queue>
using namespace std;

//方法1
struct tmp1 //运算符重载<
{
    int x;
    tmp1(int a) {x = a;}
    bool operator<(const tmp1& a) const
    {
        return x < a.x; //大顶堆
    }
};

//方法2
struct tmp2 //重写仿函数
{
    bool operator() (tmp1 a, tmp1 b) 
    {
        return a.x < b.x; //大顶堆----统一记忆：如果<成立，就要进行交换，是的大的在前面
    }
};

int main() 
{
    tmp1 a(1);
    tmp1 b(2);
    tmp1 c(3);
    priority_queue<tmp1> d;
    d.push(b);
    d.push(c);
    d.push(a);
    while (!d.empty()) 
    {
        cout << d.top().x << '\n';
        d.pop();
    }
    cout << endl;

    priority_queue<tmp1, vector<tmp1>, tmp2> f;
    f.push(c);
    f.push(b);
    f.push(a);
    while (!f.empty()) 
    {
        cout << f.top().x << '\n';
        f.pop();
    }
}
```
输出：
```C++
3
2
1

3
2
1

```

