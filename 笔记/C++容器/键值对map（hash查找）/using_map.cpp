#include <iostream>
#include <map>
using namespace std;
/**
 * 教程：
 * https://blog.csdn.net/sevenjoin/article/details/81943864
 * https://www.w3cschool.cn/cpp/cpp-fu8l2ppt.html
 * 判断key是否存在
 */

// 遍历
void visit(map<int, char> &arr, string const str)
{
    map<int, char>::iterator it;
    cout << str << ": ";
    for (it = arr.begin(); it != arr.end(); ++it)
    {
        // cout << *it << " ";
        int key = it->first;  // 取出key
        char value = it->second;  // 取出value
        cout << key << ": "<<value<<" ";
    }
    cout << endl;
}

void showMenu(string const str)
{
    cout << "============= " << str << " =============" << endl;
}

int main(int argc, char **)
{
    // id, 性别：m(male男), f(女)

    /*初始化*/
    map<int, char> stud_sex_map;
    cout << stud_sex_map[1] << endl;

    /*// 1. 直接赋值法
    map<string, int> m1;
    m1["def"] = 2;
    // 2. 用insert添加
    map<string, int> m2;
    m2.insert({ "abc", 1 });    //使用这种就可以了
    //其他形式和方式
    m2.insert(make_pair(string("def"), 2));
    m2.insert(pair<string, int>(string("ghi"), 3));
    
    //3. 列表初始化
    map<string,int> m3 = {
    {"string",1}, {"sec",2}, {"trd",3}
    };
 
    map<string,string> m4 = {
    {"first","second"}, {"third","fourth"},
    {"fifth","sixth"}, {"begin","end"}
    };
    */


    /* 新数据插入 */
    // 1 直接赋值
    stud_sex_map[10010] = 'm';
    stud_sex_map[10011] = 'f';
    visit(stud_sex_map, "stud_sex_map");
    // 数据取值
    char sex = stud_sex_map[10010];
    char key = stud_sex_map.begin()->first;
    char value = stud_sex_map.begin()->second;

    // 判断是否存在key；存在则大于0，不存在则<=0
    if (stud_sex_map.count(10012) > 0)
    {
    }

    // 清除
    stud_sex_map.erase(10010);
    visit(stud_sex_map, "stud_sex_map");
    stud_sex_map.erase(stud_sex_map.begin()); // end 不行
    visit(stud_sex_map, "stud_sex_map");

    return 0;
}