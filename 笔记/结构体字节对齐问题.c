#include <stdio.h>

#pragma pack(8)
struct s1{
    short a;  // 8字节
    long  b;  // 8字节
};  // 16字节
struct s2{
    char c;
    struct s1   d;   // 到这24字节
    long long e;  //VC6.0下可能要用__int64代替双long
};
#pragma pack()


int main(int argc, char* argv[])
{
    char *p = NULL;//根据指针长度来辨别编译器和操作系统位数
    printf("操作系统位数：sizeof(p)*8  = %d 位\n", (int)sizeof(p)*8);  //8

    struct s1 s01;
    struct s2 s02;
    printf("sizeof(s01)= %ld 字节\r\n", sizeof(s01));
    printf("sizeof(s02)= %ld 字节\r\n", sizeof(s02));

    return 0;
}