
# 查看IP
> Ubuntu18.04
## 源码
```sh
使用以下指令查看IP
ifconfig ens33 | grep 'inet ' | sed 's/inet//g' | sed 's/ *netmask.*$//g' | sed 's/ *//g'

可以建立一个新的命令，并用变量MYIP存储自己的IP，以后都能用【放到.bashrc中】
alias myip="ifconfig ens33 | grep 'inet ' | sed 's/inet//g' | sed 's/ *netmask.*$//g' | sed 's/ *//g'"
MYIP=$(myip)
```

## 原理详解
查看ens33的网络信息
```sh
ifconfig ens33
```

过滤出“inet”部分的内容，可以查看到有IP地址的那一行
```sh
 grep 'inet '
 ```

将“inet”替换为空白【相当于删除】
```sh
sed 's/inet//g'
```

使用正则表达式，将IP地址后面的空格开始到行末的所有内容都替换为空白【 *所有空格】【$表示行末】
```sh
sed 's/ *netmask.*$//g'
```

将剩下的多的空格替换成空白【相当于删除】
```sh
sed 's/ *//g'
```


## 版本不匹配时--自编
### 备用1：
```sh
alias myip="ifconfig ens33 | sed -n '2p' | sed 's/ *inet //' | awk '{print $1}' | awk -F: '{print $2}'"
MYIP=$(myip)
```

### 备用2：
```sh
alias myip="ifconfig ens33 | sed -n '2p' | sed 's/ *inet //' | sed 's/[^0-9\.]/ /g' | awk '{print $1}'"
MYIP=$(myip)
```

### 备用3：【尝试先给变量赋值，然后再打印的方式！】
```sh
MYIP=`ifconfig ens33 | sed -n '2p' | sed 's/ *inet //' | sed 's/[^0-9\.]/ /g' | awk '{print $1}'`
alias myip="echo $MYIP"
```

