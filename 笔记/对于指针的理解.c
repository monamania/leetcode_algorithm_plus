#include <stdio.h>

/** 
 * 补充知识1：
 * - 32位系统--32根地址总线--所以指针大小也是32位--最大支持4GB内存（2的32次方）
 * - 64位系统--64根地址总线--所以指针大小也是64位--最大支持1TB内存（2的64次方）
 * 
 * 补充知识2：
 * - 内存中的每一个地址对应一个存储单元
 * - 由于现代计算机通常按照字节编址，故每个字节对应1个地址
 *   （也有按字、半字、字节寻址的）
 * 
 * - 假设存储字长为32位，这1个字=32bit，半字=16bit。每次访问只能读/写1个字
 * 
 */

int main(int argc, char** argv){
    char *p = "abcdef";
    char a[] = "123456";
    printf("%d \r\n", (int)sizeof(p));
    printf("%d \r\n", (int)sizeof(*p));
    printf("%c \r\n", *p);
    printf("%x %x %x %x %x %x %x\n",*p,&a[0],&a[1],&a[2], &a[3], &a[4], &a[5]);
    p++;
    printf("%c \r\n", *p);
    printf("%x \r\n", *p);
    // a++;  // 数组名指代的类似指针，的值不允许修改


    char arr[20] = "Hello World!";
    printf("%c", *arr);
    // arr++;  // 不允许
    printf("%c", *arr);


    return 0;
}