#include "stdio.h"

#include "stdlib.h"
#include "io.h"
#include "math.h"
#include "time.h"
/**
 * 
 * 要对数据本体操作的就用指针
 * 
 * 优点：可以快速的存取
 * 缺点：
 *      插入和删除效率低
 *      长度不好确定
 *      容易造成存储空间的“碎片”
 */

/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求修改初始分配量MAXSIZE
 * 3. 根据需求，修改元素类型ElemType
 * 4. 根据数据元素，修改visit函数————自定义数据输出格式
 * 
 */

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status;   /* Status是函数的类型,其值是函数结果状态代码，如OK等 */
typedef int ElemType; /* ElemType类型根据实际情况而定，这里假设为int */

// 用于打印数据
Status visit(ElemType c)
{
    printf("%d ", c);
    return OK;
}

// 定义线性表
typedef struct
{
    ElemType data[MAXSIZE]; /* 数组，存储数据元素 */
    int length;             /* 线性表当前长度 */
} SqList;


/**
 * @brief 初始化线性表
 * 
 * @param L 线性表的指针
 * @return Status 
 */
Status InitList(SqList *L)
{
    L->length = 0;
    return OK;
}


/**
 * @brief 判断是否为空
 * 
 * @param L 线性表的指针
 * @return 若L为空表，则返回TRUE，否则返回FALSE
 */
Status ListEmpty(SqList L)
{
    if (L.length == 0)
        return TRUE;
    else
        return FALSE;
}

/**
 * @brief 将线性表置为空表
 * 
 * @param L 线性表的指针
 * @return Status 
 */
Status ClearList(SqList *L)
{
    L->length = 0;
    return OK;
}


/**
 * @brief 返回线性表中元素个数
 * 
 * @param L 线性表
 * @return 返回元素个数
 */
int ListLength(SqList L)
{
    return L.length;
}


/**
 * @brief 用e返回L中第i个数据元素的值,注意i是指位置，第1个位置的数组是从0开始
 * 
 * @param L 线性表
 * @param i 第i个元素
 * @param e 
 * @return Status 成功返回TRUE，否则返回FALSE
 */
Status GetElem(SqList L, int i, ElemType *e)
{
    if (L.length == 0 || i < 1 || i > L.length)
        return ERROR;
    *e = L.data[i - 1];

    return OK;
}


/**
 * @brief 定位元素在线性表中的位置（返回第一个）
 * 
 * @param L 线性表
 * @param e 要查找的元素
 * @return int 若存在返回位序，不存在返回0。。注意！位序从1开始
 */
int LocateElem(SqList L, ElemType e)
{
    int i;
    if (L.length == 0)
        return 0;
    for (i = 0; i < L.length; i++)
    {
        if (L.data[i] == e)
            break;
    }
    if (i >= L.length)
        return 0;

    return i + 1;
}

/* 初始条件：顺序线性表L已存在,1≤i≤ListLength(L)， */
/* 操作结果：在L中第i个位置之前插入新的数据元素e，L的长度加1 */
/**
 * @brief 在L中第i个位置之前插入新的数据元素e，L的长度加1
 * 
 * @param L 顺序线性表L已存在。L的指针
 * @param i 要放在第几个元素的前面
 * @param e 要存放的内容
 * @return Status 
 */
Status ListInsert(SqList *L, int i, ElemType e)
{
    int k;
    if (L->length == MAXSIZE) /* 顺序线性表已经满 */
        return ERROR;
    if (i < 1 || i > L->length + 1) /* 当i比第一位置小或者比最后一位置后一位置还要大时 */
        return ERROR;

    if (i <= L->length) /* 若插入数据位置不在表尾 */
    {
        for (k = L->length - 1; k >= i - 1; k--) /* 将要插入位置之后的数据元素向后移动一位 */
            L->data[k + 1] = L->data[k];
    }
    L->data[i - 1] = e; /* 将新元素插入 */
    L->length++;

    return OK;
}

/* 初始条件：顺序线性表L已存在，1≤i≤ListLength(L) */
/* 操作结果：删除L的第i个数据元素，并用e返回其值，L的长度减1 */
/**
 * @brief 删除第i个元素
 * 
 * @param L 
 * @param i 
 * @param e 
 * @return Status 
 */
Status ListDelete(SqList *L, int i, ElemType *e)
{
    int k;
    if (L->length == 0) /* 线性表为空 */
        return ERROR;
    if (i < 1 || i > L->length) /* 删除位置不正确 */
        return ERROR;
    *e = L->data[i - 1];
    if (i < L->length) /* 如果删除不是最后位置 */
    {
        for (k = i; k < L->length; k++) /* 将删除位置后继元素前移 */
            L->data[k - 1] = L->data[k];
    }
    L->length--;
    return OK;
}

/* 初始条件：顺序线性表L已存在 */
/* 操作结果：依次对L的每个数据元素输出 */
/**
 * @brief 遍历输出线性表元素
 * 
 * @param L 
 * @return Status 
 */
Status ListTraverse(SqList L)
{
    int i;
    for (i = 0; i < L.length; i++)
        visit(L.data[i]);
    printf("\n");
    return OK;
}

/**
 * @brief 将所有的在线性表Lb中但不在La中的数据元素插入到La中
 * 
 * @param La 由于要对La有数据操作，故而使用指针
 * @param Lb 由于不需要对Lb有数据操作，防止误操作，采用拷贝赋值
 */
void unionL(SqList *La, SqList Lb)
{
    int La_len, Lb_len, i;
    ElemType e;
    La_len = ListLength(*La);
    Lb_len = ListLength(Lb);
    for (i = 1; i <= Lb_len; i++)
    {
        GetElem(Lb, i, &e);
        if (!LocateElem(*La, e))
            ListInsert(La, ++La_len, e);
    }
}


void show_menu(char* tips)
{
    printf("================================================================\n");
    printf("\t\t\t %s \t\t\t\t\n", tips);
    printf("----------------------------------------------------------------\n");
}

int main()
{

    SqList L;
    SqList Lb;

    ElemType e;
    Status status;
    int j, k;

    /*初始化操作*/
    /***************************************************************/
    show_menu("初始化");
    status = InitList(&L);
    printf("初始化L后：L.length=%d\n", L.length);

    /*头插*/
    /***************************************************************/
    show_menu("头插");
    for (j = 1; j <= 5; j++)
        status = ListInsert(&L, 1, j);
    printf("在L的表头依次插入1～5后：L.data=");
    ListTraverse(L);

    /***************************************************************/
    show_menu("判断是否为空");
    printf("L.length=%d \n", L.length);
    status = ListEmpty(L);
    printf("L是否空：i=%d(1:是 0:否)\n", status);

    /***************************************************************/
    show_menu("清除线性表");
    status = ClearList(&L);
    printf("清空L后：L.length=%d\n", L.length);
    status = ListEmpty(L);
    printf("L是否空：i=%d(1:是 0:否)\n", status);


    /***************************************************************/
    show_menu("尾插");
    for (j = 1; j <= 10; j++)
        ListInsert(&L, j, j);
    printf("在L的表尾依次插入1～10后：L.data=");
    ListTraverse(L);

    printf("L.length=%d \n", L.length);

    /***************************************************************/
    show_menu("在表头插入0");
    ListInsert(&L, 1, 0);
    printf("在L的表头插入0后：L.data=");
    ListTraverse(L);
    printf("L.length=%d \n", L.length);

    /***************************************************************/
    show_menu("获取元素");
    GetElem(L, 5, &e);
    printf("第5个元素的值为：%d\n", e);
    for (j = 3; j <= 4; j++)
    {
        k = LocateElem(L, j);
        if (k)
            printf("第%d个元素的值为%d\n", k, j);
        else
            printf("没有值为%d的元素\n", j);
    }


    /***************************************************************/
    show_menu("删除数据");
    k = ListLength(L); /* k为表长 */
    for (j = k + 1; j >= k; j--)
    {
        status = ListDelete(&L, j, &e); /* 删除第j个数据 */
        if (status == ERROR)
            printf("删除第%d个数据失败\n", j);
        else
            printf("删除第%d个的元素值为：%d\n", j, e);
    }
    printf("依次输出L的元素：");
    ListTraverse(L);

    j = 5;
    ListDelete(&L, j, &e); /* 删除第5个数据 */
    printf("删除第%d个的元素值为：%d\n", j, e);

    printf("依次输出L的元素：");
    ListTraverse(L);

    /***************************************************************/
    show_menu("unionL操作");
    //构造一个有10个数的Lb
    status = InitList(&Lb);
    for (j = 6; j <= 15; j++)
        status = ListInsert(&Lb, 1, j);

    unionL(&L, Lb);

    printf("依次输出合并了Lb的L的元素：");
    ListTraverse(L);

    return 0;
}
