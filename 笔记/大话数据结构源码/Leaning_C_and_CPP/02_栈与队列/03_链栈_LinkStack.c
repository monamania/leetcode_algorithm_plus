/**
 * @file LinkStack.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是堆栈的链式存储结构
 *          ————即使用链表存储
 * @version 0.1
 * @date 2021-08-30
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include "stdio.h"    
#include "stdlib.h"  
#include "io.h"  
#include "math.h"  
#include "time.h"
// #define NDEBUG
#include "assert.h"

// #include <conio.h> //清除命令行

/**
 *  * 抽象数据类型

ADT  栈（Stack）
Data
	栈的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除第一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitStack(*S):      初始化操作，建立一个空栈S。
	StackEmpty(S):      若栈为空，返回true，否则返回false。
	ClearStack(*S):     将栈清空。
    DestroyStack(*S):   若栈存在，则销毁它  【动态分配的空间时使用】
	GetTop(S, *e):      若栈存在且非空，用e返回S的栈顶元素
    Push(*S, e):        若栈S存在，插入新元素e到栈S中并成为栈顶元素
    Pop(*S, *e):        删除栈S中栈顶元素，并用e返回其值
	StackLength(S):     返回栈S的元素个数。
endADT
*/
/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 4. 修改INF的数值，确保为一个不可能存在的数
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  


typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


/****************************************************************/
/* 链栈结构 */
typedef struct StackNode
{
        SElemType data;
        struct StackNode *next;
}StackNode,*LinkStackPtr;

typedef struct
{
        LinkStackPtr top;   // 指针，用于指向链表的节点
        int count;
}LinkStack;
/****************************************************************/


Status visit(SElemType c)
{
        printf("%d ",c);
        return OK;
}

/**
 * @brief 初始化堆栈
 * 
 * @param S 
 * @return Status 
 */
Status InitStack(LinkStack *S)
{
    S->count = 0;
    S->top = NULL;
    return OK;
}


/**
 * @brief 若栈为空，返回true，否则返回false。
 * 
 * @param S 
 * @return Status 
 */
Status StackEmpty(LinkStack S)
{
    if (S.top == NULL)
        return TRUE;
    return FALSE;
}


/**
 * @brief 将栈清空。
 * 
 * @param S 
 * @return Status 
 */
Status ClearStack(LinkStack *S)
{
    LinkStackPtr tmp_s;
    if (S->top != NULL) // 没空
    {
        tmp_s = S->top;
        S->top = S->top->next;
        free(tmp_s);
    }
    S->count = 0;
    return OK;
}

/**
 * @brief Get the Top object
 * 
 * @param S 
 * @param e 
 * @return Status 
 */
Status GetTop(LinkStack S, SElemType *e)
{
    if (S.top != NULL)
    {
        *e = S.top->data;
        return OK;
    }
    return ERROR;
}

/**
 * @brief 若栈S存在，插入新元素e到栈S中并成为栈顶元素
 * 
 * @param S 
 * @param e 
 * @return Status 
 */
Status Push(LinkStack *S, SElemType e)
{
    LinkStackPtr new_s = (LinkStackPtr)malloc(sizeof(StackNode));
    assert(new_s != NULL);
    if (new_s == NULL)  //内存分配失败
        return ERROR;  
    new_s->data = e;
    new_s->next = S->top;  /* 把当前的栈顶元素赋值给新结点的直接后继，见图中① */
    S->top = new_s;   /* 将新的结点s赋值给栈顶指针，见图中② */
    S->count++;
    return OK;
}


/**
 * @brief 若栈不空，则删除S的栈顶元素，用e返回其值，并返回OK；否则返回ERROR
 * 
 * @param S 
 * @param e 
 * @return Status 
 */
Status Pop(LinkStack *S,SElemType *e)
{ 
        LinkStackPtr p;
        if(StackEmpty(*S))
                return ERROR;
        *e=S->top->data;
        p=S->top;					/* 将栈顶结点赋值给p，见图中③ */
        S->top=S->top->next;    /* 使得栈顶指针下移一位，指向后一结点，见图中④ */
        free(p);                    /* 释放结点p */        
        S->count--;
        return OK;
}


/**
 * @brief 返回S的元素个数，即栈的长度
 * 
 * @param S 
 * @return int 
 */
int StackLength(LinkStack S)
{ 
        return S.count;
}


Status StackTraverse(LinkStack S)
{
        LinkStackPtr p;
        p=S.top;
        while(p)
        {
                 visit(p->data);
                 p=p->next;
        }
        printf("\n");
        return OK;
}


int main(int argc, char *argv[])
{
    int j;
    LinkStack s;
    int e;
    system("cls");//清除命令行
	// clrscr();//清除命令行
	Sleep(500);
    printf("=============================== 程序输出 ===============================\r\n");
    if(InitStack(&s)==OK)
        for(j=1;j<=10;j++)
            Push(&s,j);
    printf("栈中元素依次为：");
    StackTraverse(s);
    Pop(&s,&e);
    printf("弹出的栈顶元素 e=%d\n",e);
    printf("栈空否：%d(1:空 0:否)\n",StackEmpty(s));
    GetTop(s,&e);
    printf("栈顶元素 e=%d 栈的长度为%d\n",e,StackLength(s));
    ClearStack(&s);
    printf("清空栈后，栈空否：%d(1:空 0:否)\n",StackEmpty(s));
    return 0;
}