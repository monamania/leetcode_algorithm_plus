/**
 * @file LinkQueue.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是队列的顺序存储结构------实现循环队列    rear尾  front头
 *          --首先用两个指针表示队头队尾，且不允许数组全部存满（队列满时，数组也会还存有一个空间）
 *          --队列满的条件为：(rear+1) %QueueSize == front
 *          --计算队列长度公式：(rear - front + QueueSize) %QueueSize
 * @version 0.1
 * @date 2021-08-30
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include "stdio.h"    
#include "stdlib.h"  
#include "io.h"  
#include "math.h"  
#include "time.h"
// #define NDEBUG
#include "assert.h"
// #include <conio.h> //清除命令行

/**
 *  * 抽象数据类型

ADT  队列（Queue）
Data
	队列的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除第一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitQueue(*Q):      初始化操作，建立一个空队列S。
	QueueEmpty(Q):      若队列为空，返回true，否则返回false。
	ClearQueue(*Q):     将队列清空。
    DestroyQueue(*Q):   若队列存在，则销毁它  【动态分配的空间时使用】
	GetHead(Q, *e):     若队列存在且非空，用e返回队列Q的队头元素
    EnQueue(*Q, e):     若队列Q存在，插入新元素e到队列Q中并成为队尾元素
    DeQueue(*Q, *e):    删除队列Q中队头元素，并用e返回其值
	QueueLength(S):     返回队列S的元素个数。
endADT
*/
/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 4. 修改INF的数值，确保为一个不可能存在的数
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */



#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int QElemType; /* QElemType类型根据实际情况而定，这里假设为int */

/* 循环队列的顺序存储结构 */
typedef struct
{
	QElemType data[MAXSIZE];
	int front;    	/* 头指针 */
	int rear;		/* 尾指针，若队列不空，指向队列尾元素的下一个位置 */
}SqQueue;

Status visit(QElemType c)
{
	printf("%d ",c);
	return OK;
}

/* 初始化一个空队列Q */
Status InitQueue(SqQueue *Q)
{
	Q->front=0;
	Q->rear=0;
	return  OK;
}

/* 将Q清为空队列 */
Status ClearQueue(SqQueue *Q)
{
	Q->front=Q->rear=0;
	return OK;
}

/* 若队列Q为空队列,则返回TRUE,否则返回FALSE */
Status QueueEmpty(SqQueue Q)
{ 
	if(Q.front==Q.rear) /* 队列空的标志 */
		return TRUE;
	else
		return FALSE;
}

/* 返回Q的元素个数，也就是队列的当前长度 */
int QueueLength(SqQueue Q)
{
	return  (Q.rear-Q.front+MAXSIZE)%MAXSIZE;
}

/* 若队列不空,则用e返回Q的队头元素,并返回OK,否则返回ERROR */
Status GetHead(SqQueue Q,QElemType *e)
{
	if(Q.front==Q.rear) /* 队列空 */
		return ERROR;
	*e=Q.data[Q.front];
	return OK;
}

/* 若队列未满，则插入元素e为Q新的队尾元素 */
Status EnQueue(SqQueue *Q,QElemType e)
{
	if ((Q->rear+1)%MAXSIZE == Q->front)	/* 队列满的判断 */
		return ERROR;
	Q->data[Q->rear]=e;			/* 将元素e赋值给队尾 */
	Q->rear=(Q->rear+1)%MAXSIZE;/* rear指针向后移一位置， */
								/* 若到最后则转到数组头部 */
	return  OK;
}

/* 若队列不空，则删除Q中队头元素，用e返回其值 */
Status DeQueue(SqQueue *Q,QElemType *e)
{
	if (Q->front == Q->rear)			/* 队列空的判断 */
		return ERROR;
	*e=Q->data[Q->front];				/* 将队头元素赋值给e */
	Q->front=(Q->front+1)%MAXSIZE;	/* front指针向后移一位置， */
									/* 若到最后则转到数组头部 */
	return  OK;
}

/* 从队头到队尾依次对队列Q中每个元素输出 */
Status QueueTraverse(SqQueue Q)
{ 
	int i;
	i=Q.front;
	while((i+Q.front)!=Q.rear)
	{
		visit(Q.data[i]);
		i=(i+1)%MAXSIZE;
	}
	printf("\n");
	return OK;
}

int main()
{
	Status j;
	int i=0,l;
	QElemType d;
	SqQueue Q;
	InitQueue(&Q);
	printf("初始化队列后，队列空否？%u(1:空 0:否)\n",QueueEmpty(Q));

	printf("请输入整型队列元素(不超过%d个),-1为提前结束符: ",MAXSIZE-1);
	do
	{
		/* scanf("%d",&d); */
		d=i+100;
		if(d==-1)
			break;
		i++;
		EnQueue(&Q,d);
	}while(i<MAXSIZE-1);

	printf("队列长度为: %d\n",QueueLength(Q));
	printf("现在队列空否？%u(1:空 0:否)\n",QueueEmpty(Q));
	printf("连续%d次由队头删除元素,队尾插入元素:\n",MAXSIZE);
	for(l=1;l<=MAXSIZE;l++)
	{
		DeQueue(&Q,&d);
		printf("删除的元素是%d,插入的元素:%d \n",d,l+1000);
		/* scanf("%d",&d); */
		d=l+1000;
		EnQueue(&Q,d);
	}
	l=QueueLength(Q);

	printf("现在队列中的元素为: \n");
	QueueTraverse(Q);
	printf("共向队尾插入了%d个元素\n",i+MAXSIZE);
	if(l-2>0)
		printf("现在由队头删除%d个元素:\n",l-2);
	while(QueueLength(Q)>2)
	{
		DeQueue(&Q,&d);
		printf("删除的元素值为%d\n",d);
	}

	j=GetHead(Q,&d);
	if(j)
		printf("现在队头元素为: %d\n",d);
	ClearQueue(&Q);
	printf("清空队列后, 队列空否？%u(1:空 0:否)\n",QueueEmpty(Q));
	return 0;
}

