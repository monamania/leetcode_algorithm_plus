#include <stdio.h>
#include <stdlib.h>
// #define NDEBUG
#include <assert.h>
#include <string.h>
#include <windows.h>

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  


typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

int Fbi(int n)
{
    assert(n >= 0);
    if (n < 2)
    {
        return n == 0 ? 0 : 1;
    }
    return Fbi(n - 1) + Fbi(n - 2);
}

int main(int argc, char *argv[])
{
    int i = 0;
    system("cls");//清除命令行
	// clrscr();//清除命令行
	Sleep(500);
    printf("斐波那契数列：");
    for (i = 1; i < 10; i++)
    {
        printf("%d ", Fbi(i));
    }
    printf("\r\n");
    return 0;
}