/**
 * @file DoubleStack.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是堆栈的顺序存储结构  两栈共享空间----[用一个数组实现两个栈]
 *          ————即使用数组存储
 * @version 0.1
 * @date 2021-08-30
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include "stdio.h"    
#include "stdlib.h"  
#include "io.h"  
#include "math.h"  
#include "time.h"
// #define NDEBUG
#include "assert.h"

// #include <conio.h> //清除命令行

/**
 *  * 抽象数据类型

ADT  栈（Stack）
Data
	栈的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除第一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitStack(*S):      初始化操作，建立一个空栈S。
	StackEmpty(S):      若栈为空，返回true，否则返回false。
	ClearStack(*S):     将栈清空。
    DestroyStack(*S):   若栈存在，则销毁它  【动态分配的空间时使用】
	GetTop(S, *e):      若栈存在且非空，用e返回S的栈顶元素
    Push(*S, e):        若栈S存在，插入新元素e到栈S中并成为栈顶元素
    Pop(*S, *e):        删除栈S中栈顶元素，并用e返回其值
	StackLength(S):     返回栈S的元素个数。
endADT
*/
/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 4. 修改INF的数值，确保为一个不可能存在的数
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */


#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */

/* 两栈共享空间结构 */
typedef struct 
{
        SElemType data[MAXSIZE];
        int top1;	/* 栈1栈顶指针 */
        int top2;	/* 栈2栈顶指针 */
}SqDoubleStack;



Status visit(SElemType c)
{
        printf("%d ",c);
        return OK;
}


/**
 * @brief 初始化一个空栈
 * 
 * @param S 
 * @return Status 
 */
Status InitStack(SqDoubleStack *S)
{
    memset(S->data, INF, sizeof(S->data));
    S->top1=-1;
    S->top2=MAXSIZE;
    return OK;
}

/**
 * @brief 清空栈内数据
 * 
 * @param S 
 * @return Status 
 */
Status ClearStack(SqDoubleStack *S)
{ 
    memset(S->data, INF, sizeof(S->data));
    S->top1=-1;
    S->top2=MAXSIZE;
    return OK;
}


/**
 * @brief 判断是否为空
 * 
 * @param S 
 * @return Status 若栈S为空栈，则返回TRUE，否则返回FALSE
 */
Status StackEmpty(SqDoubleStack S)
{ 
    if (S.top1==-1 && S.top2==MAXSIZE)
            return TRUE;
    else
            return FALSE;
}


/**
 * @brief 获取栈内元素个数，即栈的长度
 * 
 * @param S 
 * @return int 
 */
int StackLength(SqDoubleStack S)
{ 
        return (S.top1+1)+(MAXSIZE-S.top2);
}


/**
 * @brief 向栈空间stackNumber，插入元素e为新的栈顶元素
 * 
 * @param S 
 * @param e 
 * @param stackNumber 要进行操作的栈空间，1为栈1，2为栈2
 * @return Status 
 */
Status Push(SqDoubleStack *S,SElemType e,int stackNumber)
{
        if (S->top1+1==S->top2)	/* 栈已满，不能再push新元素了 */
                return ERROR;	
        if (stackNumber==1)			/* 栈1有元素进栈 */
                S->data[++S->top1]=e; /* 若是栈1则先top1+1后给数组元素赋值。 */
        else if (stackNumber==2)	/* 栈2有元素进栈 */
                S->data[--S->top2]=e; /* 若是栈2则先top2-1后给数组元素赋值。 */
        return OK;
}


/**
 * @brief 若栈空间stackNumber，不空则删除S的栈顶元素，用e返回其值，并返回OK；否则返回ERROR
 * 
 * @param S 
 * @param e 
 * @param stackNumber 要进行操作的栈空间，1为栈1，2为栈2
 * @return Status 
 */
Status Pop(SqDoubleStack *S,SElemType *e,int stackNumber)
{ 
        if (stackNumber==1) 
        {
                if (S->top1==-1) 
                        return ERROR; /* 说明栈1已经是空栈，溢出 */
                *e=S->data[S->top1--]; /* 将栈1的栈顶元素出栈 */
        }
        else if (stackNumber==2)
        { 
                if (S->top2==MAXSIZE) 
                        return ERROR; /* 说明栈2已经是空栈，溢出 */
                *e=S->data[S->top2++]; /* 将栈2的栈顶元素出栈 */
        }
        return OK;
}

/**
 * @brief 遍历输出，从栈底->栈顶，从栈空间1，到栈空间2
 * 
 * @param S 
 * @return Status 
 */
Status StackTraverse(SqDoubleStack S)
{
        int i;
        i=0;
        while(i<=S.top1)
        {
                visit(S.data[i++]);
        }
        i=S.top2;
        while(i<MAXSIZE)
        {
                visit(S.data[i++]);
        }
        printf("\n");
        return OK;
}

void showMenu(const char* tips)
{
    printf("================================================================\n");
    printf("\t\t\t %s \t\t\t\t\n", tips);
    printf("----------------------------------------------------------------\n");
    return;
}

int main(int argc, char* argv[])
{
    int j;
    SqDoubleStack s;
    int e;

	system("cls");//清除命令行
	// clrscr();//清除命令行
	Sleep(500);

    /****************************************************************/
    showMenu("初始化堆栈");
    if(InitStack(&s)==OK)
    {
            for(j=1;j<=5;j++)
                    Push(&s,j,1);
            for(j=MAXSIZE;j>=MAXSIZE-2;j--)
                    Push(&s,j,2);
    }
    printf("栈中元素依次为：");
    StackTraverse(s);

    printf("当前栈中元素有：%d \n",StackLength(s));

    /****************************************************************/
    showMenu("弹出栈2的栈顶元素");
    Pop(&s,&e,2);
    printf("弹出的栈顶元素 e=%d\n",e);
    printf("栈空否：%d(1:空 0:否)\n",StackEmpty(s));

    /****************************************************************/
    showMenu("向栈1进行push操作");
    for(j=6;j<=MAXSIZE-2;j++)
            Push(&s,j,1);

    printf("栈中元素依次为：");
    StackTraverse(s);

    printf("栈满否：%d(1:否 0:满)\n",Push(&s,100,1));

    /****************************************************************/
    showMenu("清除栈内数据");
    ClearStack(&s);
    printf("清空栈后，栈空否：%d(1:空 0:否)\n",StackEmpty(s));

    return 0;
}