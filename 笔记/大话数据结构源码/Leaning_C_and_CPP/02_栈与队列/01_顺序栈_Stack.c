/**
 * @file Stack.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是堆栈的顺序存储结构
 *          ————即使用数组存储
 * @version 0.1
 * @date 2021-08-30
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include "stdio.h"    
#include "stdlib.h"   
#include "io.h"  
#include "math.h"  
#include "time.h"
// #define NDEBUG 
#include "assert.h"
// #include <conio.h> //清除命令行

/**
 *  * 抽象数据类型

ADT  栈（Stack）
Data
	栈的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除第一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitStack(*S):      初始化操作，建立一个空栈S。
	StackEmpty(S):      若栈为空，返回true，否则返回false。
	ClearStack(*S):     将栈清空。
    DestroyStack(*S):   若栈存在，则销毁它  【动态分配的空间时使用】
	GetTop(S, *e):      若栈存在且非空，用e返回S的栈顶元素
    Push(*S, e):        若栈S存在，插入新元素e到栈S中并成为栈顶元素
    Pop(*S, *e):        删除栈S中栈顶元素，并用e返回其值
	StackLength(S):     返回栈S的元素个数。
endADT
*/
/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 4. 修改INF的数值，确保为一个不可能存在的数
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
// 定义一个不可能的数
#define INF   -99999  
#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status; 
typedef int SElemType; /* SElemType类型根据实际情况而定，这里假设为int */


/* 顺序栈结构 */
typedef struct
{
        SElemType data[MAXSIZE];
        int top; /* 用于栈顶指针 */
}SqStack;


Status visit(const SElemType c)
{
        printf("%d ",c);
        return OK;
}

/**
 * @brief 初始化操作，建立一个空栈S。
 * 
 * @param S 栈
 * @return Status 
 */
Status InitStack(SqStack *S)
{
	memset(S->data,INF,sizeof(S->data));
	S->top = -1;

	return OK;
}

/**
 * @brief 判断栈是否为空
 * 
 * @param S 
 * @return Status 若栈为空，返回true，否则返回false。
 */
Status StackEmpty(SqStack S)
{
	if (S.top == -1)
		return TRUE;
	else
		return FALSE;
}


/**
 * @brief 将栈清空
 * 
 * @return Status 
 */
Status ClearStack(SqStack *S)
{
	memset(S->data,INF,sizeof(S->data));
	S->top = -1;
	return OK;

}

// DestroyStack(*S):   若栈存在，则销毁它【动态分配的空间时使用】

/**
 * @brief 若栈存在且非空，用e返回S的栈顶元素
 * 
 * @param S 
 * @param e 
 * @return Status 
 */
Status GetTop(SqStack S, SElemType *e)
{
	if (StackEmpty(S))
		return ERROR;
	else
		*e = S.data[S.top];
	return OK;
}

/**
 * @brief 若栈S存在且没有满，插入新元素e到栈S中并成为栈顶元素
 * 
 * @param S 
 * @param e 
 * @return Status 
 */
Status Push(SqStack *S, SElemType e)
{
	if (S->top == MAXSIZE-1)  //栈空间已满
		return ERROR;

	S->top++;
	S->data[S->top] = e;
	return OK;
}

/**
 * @brief 删除栈S中栈顶元素，并用e返回其值
 * 
 * @param S 
 * @param e 
 * @return Status 
 */
Status Pop(SqStack *S, SElemType *e)
{
	if (StackEmpty(*S))
		return ERROR;
	else
		*e = S->data[S->top];
	S->top--;
	return OK;
}

/**
 * @brief 返回栈S的元素个数
 * 
 * @param S 
 * @return int 
 */
int StackLength(SqStack S)
{
	if (StackEmpty(S))
		return 0;
	return S.top+1;
}


/* 从栈底到栈顶依次对栈中每个元素显示 */
Status StackTraverse(SqStack S)
{
        int i;
        i=0;
        while(i<=S.top)
        {
                visit(S.data[i++]);
        }
        printf("\n");
        return OK;
}

void showMenu(const char* tips)
{
    printf("================================================================\n");
    printf("\t\t\t %s \t\t\t\t\n", tips);
    printf("----------------------------------------------------------------\n");
    return;
}

int main()
{
	int j;
	SqStack s;
	int e;
	system("cls");//清除命令行
	// clrscr();//清除命令行
	Sleep(500);
	/****************************************************************/
    showMenu("初始化堆栈");
	if(InitStack(&s)==OK)
		for(j=1;j<=10;j++)
			Push(&s,j);
	printf("栈中元素依次为（栈底->栈顶）：");
	StackTraverse(s);
	/****************************************************************/
    showMenu("Pop操作");
	Pop(&s,&e);
	printf("弹出的栈顶元素 e=%d\n",e);
	printf("栈空否：%d(1:空 0:否)\n",StackEmpty(s));
	/****************************************************************/
    showMenu("GetTop操作");
	GetTop(s,&e);
	printf("栈顶元素 e=%d 栈的长度为%d\n",e,StackLength(s));
	printf("栈中元素依次为（栈底->栈顶）：");
	StackTraverse(s);

	/****************************************************************/
    showMenu("ClearStack操作");
	ClearStack(&s);
	printf("清空栈后，栈空否：%d(1:空 0:否)\n",StackEmpty(s));
	printf("栈中元素依次为（栈底->栈顶）：");
	StackTraverse(s);
	
	return 0;
}

