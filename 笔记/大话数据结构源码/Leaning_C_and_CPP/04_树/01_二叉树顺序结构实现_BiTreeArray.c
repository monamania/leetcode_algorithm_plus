/**
 * @file BiTreeArray.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是二叉树顺序结构实现
 * @version 0.1
 * @date 2021-08-30
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include "stdio.h"    
#include "stdlib.h"  
#include "io.h"  
#include "math.h"  
#include "time.h"
// #define NDEBUG
#include "assert.h"
// #include <conio.h> //清除命令行

/**
 *  * 抽象数据类型

ADT  树（Tree）
Data
	树是由一个根节点和若干棵子树构成。树中结点具有相同数据结构及其层次关系
Operation
	InitTree(*T):          初始化操作，建立一个空树S。
    DestroyTree(*T):       若树存在，则销毁它  【动态分配的空间时使用】
    CreateTree(*T, definition): 按definition中给出的定义来构造树
	TreeEmpty(T):          若树为空，返回true，否则返回false。
    TreeDepth(T):          返回树T的深度（最大层次）
    Root(T):               返回T的根结点
	Value(T, cur_e):       cur_e是树T中的一个结点，返回此结点的值
    Assign(T,cur_e,value): 给树T的结点cur_e赋值为value
    Parent(T, cur_e):      若cur_e是树T的非根结点，则返回它的双亲，否则返回空
    LeftChild(T, cur_e):   若cur_e是树T的非叶结点，则返回它的最左孩子，否则返回空
    RightSibling(T, cur_e):若cur_e有右兄弟，则返回它的右兄弟，否则返回空
    InsertChild(*T,*p,i,c):其中p指向树T的某个结点，i为所指结点p的度加1，非空树c与T不相交，操作结果为插入c为树T中p指结点的第i棵子树
    DeleteChild(*T,*p,i):   其中p指向树T的某个结点，i为所指结点p的度，操作结果为删除T中p所指结点的第i颗子树
endADT
*/

/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 4. 修改INF的数值，确保为一个不可能存在的数
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */



#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0
#define MAXSIZE 100 /* 存储空间初始分配量 */
#define MAX_TREE_SIZE 100 /* 二叉树的最大结点数 */

typedef int Status;		/* Status是函数的类型,其值是函数结果状态代码，如OK等 */
typedef int TElemType;  /* 树结点的数据类型，目前暂定为整型 */
typedef TElemType SqBiTree[MAX_TREE_SIZE]; /* 0号单元存储根结点  */

typedef struct
{
	int level,order; /* 结点的层,本层序号(按满二叉树计算) */
}Position;

TElemType Nil=0; /*  设整型以0为空 */
