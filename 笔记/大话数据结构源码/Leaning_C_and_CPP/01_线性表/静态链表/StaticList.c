/**
 * @file order_list.h
 * @author Monomania(13015159350@qq.com)
 * @brief 这是线性表的静态链表存储结构例子
 *          ————即使用数组存储
 * @version 0.1
 * @date 2021-08-30
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include <stdio.h>
#include "string.h"
#include "stdlib.h"


/**
 * @抽象数据类型——————注意这是没有指针类型的时候使用

ADT 线性表（List）
Data
	线性表的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除低一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitList(*L):      初始化操作，建立一个空的线性表L。
	ListEmpty(L):      若线性表为空，返回true，否则返回false。
	ClearList(*L):     将线性表清空。
	GetElem(L, i, *e): 将线性表L中的第i个位置元素值返回给e。
	LocateElem(L, e):  在线性表L中查找与给定值e相等的元素，如果查找成功，返回该元素在表中序号表示成功；否则，返回0表示失                        败。
	ListInsert(*L, i, e):  在线性表L中的第i个位置插入新元素e。
	ListDelete(*L, i, *e): 删除线性表工中第i个位置元素，并用e返回其值。
	ListLength(L):     返回线性表L的元素个数。
endADT

优点：--主要用于没有指针的时候
缺点：复杂，麻烦，空间不可变

 */ 

/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求修改初始分配量MAXSIZE
 * 3. 根据需求，修改元素类型ElemType
 * 4. 根据数据元素，修改visit函数————自定义数据输出格式
 * 
 */

#define OK 1
#define ERROR 0
#define TRUE  1
#define FALSE 0

#define MAXSIZE 1000   /*存储空间初始化*/

typedef int Status;     /* Status是变量的类型,其值是函数结果状态代码，如OK等 */
typedef char ElemType;  /* ElemType类型（元素类型）根据实际情况而定，这里假设为char */

// 用于打印数据
Status visit(ElemType c)
{
    printf("%c ",c);
    return OK;
}

/* 线性表的静态链表存储结构 */
typedef struct 
{
    ElemType data;
    int cur;  /* 游标(Cursor) ，为0时表示无指向 */
} Component,StaticLinkList[MAXSIZE];

/** 
 * 本程序的理解：见文档内excle表
 * 
 * 静态链表的另一种实现方式————后插。
 * http://metronic.net.cn/metronic/show-42505.html
 * 构成：将一个结构体数组，分为数据链表 + 备用链表
 * 
 * 数据链表：通常表头位于数组下标为1(a[1])的位置。
 * 备用链表：通常备用链表的表头位于数组下标为0(a[0])的位置
 * 
 * 例子：设数组长度为6
 * 1. 初始化后结果
 * 		a[0]  a[1]  a[2] a[3] a[4] a[5]
 * data Null  Null……可以不管先
 * cur   1     2     3    4    5    0
 * 
 * 2. 存入第一个数据 值为5
 * （1）申请空间：返回获取a[0].cur的游标为1，并更新a[0].cur得下标
 * 		idx = a[0].cur;  //获取存储数据的下标——为1
 * 		a[0].cur = 	a[idx].cur; //更新游标。
 * （2）将数据存入，（上面返回的为为1即idx=1）
 * 		a[idx].data = data;
 *      a[idx].cur = 0;
 * 结果
 * 		a[0]  a[1]  a[2] a[3] a[4] a[5]
 * data Null   5 
 * cur   2     0     3    4    5    0
 */

/* 将一维数组space中各分量链成一个备用链表，space[0].cur为头指针，"0"表示空指针 */
/**
 * @brief 初始化静态数组
 * 
 * @param space 初始化用于存储数据的静态链表
 * @return Status 返回状态
 */
Status InitList(StaticLinkList space) 
{
	int i;
	for (i=0; i<MAXSIZE-1; i++)  
		space[i].cur = i+1;
	space[MAXSIZE-1].cur = 0; /* 目前静态链表为空，最后一个元素的cur为0 */
	return OK;
}


/**
 * @brief 为静态链表分配空间（即找空地）
 * 
 * @param space 静态链表
 * @return int 若备用空间链表非空，则返回分配的结点下标，否则返回
 */
int Malloc_SSL(StaticLinkList space) 
{
	int i = space[0].cur;     				/* 当前数组第一个元素的cur存的值 */
											/* 就是要返回的第一个备用空闲的下标 */                              		
	if (space[0]. cur)     					/* 判断空间是否已经满了*/     
	    space[0]. cur = space[i].cur;       /* 由于要拿出一个分量来使用了， */
	                                        /* 所以我们就得把它的下一个 */
	                                        /* 分量用来做备用 */
	return i;
}


/**
 * @brief 将下标为k的空闲结点回收到备用链表【注意是下标，不是序号，从0开始】
 * 
 * @param space 静态链表
 * @param k 要删除的空间
 */
void Free_SSL(StaticLinkList space, int k) 
{  
    space[k].cur = space[0].cur;    /* 把第一个元素的cur值赋给要删除的分量cur */
    space[0].cur = k;               /* 把要删除的分量下标赋值给第一个元素的cur */
}

 
 /* 初始条件：静态链表L已存在。操作结果：返回L中数据元素个数 */
 /**
  * @brief 获取链表L中的长度
  * 
  * @param L 静态链表
  * @return int 返回元素个数
  */
int ListLength(StaticLinkList L)
{
    int j=0;
    int i=L[MAXSIZE-1].cur;
    while(i)
    {
        i=L[i].cur;
        j++;
    }
    return j;
}


/*  在L中第i个元素之前插入新的数据元素e   */
/**
 * @brief 在L中第i个元素之前插入新的数据元素e
 * 
 * @param L 
 * @param i 
 * @param e 
 * @return Status 
 */
Status ListInsert(StaticLinkList L, int i, ElemType e)   
{  
    int j, k, l;   
    k = MAXSIZE - 1;   /* 注意k首先是最后一个元素的下标 */
    if (i < 1 || i > ListLength(L) + 1)   
        return ERROR;   
    j = Malloc_SSL(L);   /* 获得空闲分量的下标 */
	if (j)   
    {   
		L[j].data = e;   /* 将数据赋值给此分量的data */
		for(l = 1; l <= i - 1; l++)   /* 找到第i个元素之前的位置 */
		   k = L[k].cur;           
		L[j].cur = L[k].cur;    /* 把第i个元素之前的cur赋值给新元素的cur */
		L[k].cur = j;           /* 把新元素的下标赋值给第i个元素之前元素的ur */
		return OK;   
    }   
    return ERROR;   
}



/**
 * @brief 删除在L中第i个数据元素 ，这个i从1开始
 * 
 * @param L 
 * @param i 
 * @return Status 
 */
Status ListDelete(StaticLinkList L, int i)   
{ 
    int j, k;   
    if (i < 1 || i > ListLength(L))   
        return ERROR;   
    k = MAXSIZE - 1;   
    for (j = 1; j <= i - 1; j++)   
        k = L[k].cur;   
    j = L[k].cur;   
    L[k].cur = L[j].cur;   
    Free_SSL(L, j);   
    return OK;   
} 


/**
 * @brief 遍历输出
 * 
 * @param L 
 * @return Status 
 */
Status ListTraverse(StaticLinkList L)
{
    int j=0;
    int i=L[MAXSIZE-1].cur;
    while(i)
    {
            visit(L[i].data);
            i=L[i].cur;
            j++;
    }
    return j;
    printf("\n");
    return OK;
}


int main()
{
    StaticLinkList L;
    Status i;
    
    system("cls");//清除命令行
    Sleep(500);

    i=InitList(L);
    printf("初始化L后：L.length=%d\n",ListLength(L));

    i=ListInsert(L,1,'F');
    i=ListInsert(L,1,'E');
    i=ListInsert(L,1,'D');
    i=ListInsert(L,1,'B');
    i=ListInsert(L,1,'A');

    printf("\n在L的表头依次插入FEDBA后：\nL.data=");
    ListTraverse(L); 

    i=ListInsert(L,3,'C');
    printf("\n在L的“B”与“D”之间插入“C”后：\nL.data=");
    ListTraverse(L);

	printf("\n数组内data的数据：");
	for ( i = 1; i<7; ++i ){
		printf("%c ", L[i].data);
	}

	printf("\n数组内cur的数据：");
	for ( i = 1; i<7; ++i ){
		printf("%d ", L[i].cur);
	}
	printf("\n");
	printf("\n数组内最后一位的cur：%d", L[MAXSIZE-1].cur);

	i = ListDelete(L, 1);
    printf("\n在L的删除“A”后：\nL.data=");
    ListTraverse(L); 

    printf("\n");

    return 0;
}