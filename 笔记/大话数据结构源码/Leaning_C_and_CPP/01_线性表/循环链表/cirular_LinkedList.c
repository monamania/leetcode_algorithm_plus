/**
 * @file cirular_LinkedList.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是线性表的链式存储结构 中的循环链表例子
 *          ————即使用链表存储
 * @version 0.1
 * @date 2021-08-30
 * @copyright Copyright (c) 2021
 * 
 */
#include <windows.h>
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "io.h"
#include "math.h"
#include "time.h"
// #define NDEBUG 
#include "assert.h" 


/**
 *  * 抽象数据类型

ADT 线性表（List）
Data
	线性表的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除低一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitList(*L):      初始化操作，建立一个空的线性表L。
	ListEmpty(L):      若线性表为空，返回true，否则返回false。
	ClearList(*L):     将线性表清空。
	GetElem(L, i, *e): 将线性表L中的第i个位置元素值返回给e。
	LocateElem(L, e):  在线性表L中查找与给定值e相等的元素，如果查找成功，返回该元素在表中序号表示成功；否则，返回0表示失                        败。
	ListInsert(*L, i, e):  在线性表L中的第i个位置插入新元素e。
	ListDelete(*L, i, *e): 删除线性表工中第i个位置元素，并用e返回其值。
	ListLength(L):     返回线性表L的元素个数。
endADT
*/
/**
 * @使用说明 
 * 1. 将cirular_LinkedList.c文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */

typedef int ElemType;       // 元素类型 ElemType

//单链表存储结构
/* 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */
typedef struct Node{
    ElemType data; 
    struct Node *next;
} Node; 
typedef struct Node *LinkList;


#define OK 1
#define ERROR 0
#define TRUE  1
#define FALSE 0
typedef int Status;



Status visit(const ElemType e){
    printf("%d ", e);
    return OK;
}

/**
 * @brief 初始化操作，建立一个空的线性表L。
 * 
 * @param L 
 * @return Status 
 */
Status InitList(LinkList *L){
    *L = (Node *)malloc(sizeof(Node));   //头节点
    assert(*L != NULL);
    (*L)->next = (*L);   //循环链表，指针指会自身

    return OK;
}

/**
 * @brief 若线性表为空，返回true，否则返回false。
 * 
 * @param L 
 * @return Status 
 */
Status ListEmpty(LinkList L){
    if (L->next == L)   // 指向的是自己的话
        return TRUE;
    else
        return FALSE;
}

/**
 * @brief 清空链表
 * 
 * @param L 
 * @return Status 
 */
Status ClearList(LinkList *L){
    Node *p;  //强调这是一个节点
    while (!ListEmpty(*L))
    {
        p = (*L)->next;  //指向第一个节点
        (*L)->next = p->next;
        free(p);
    }
    return OK;
}

/**
 * @brief 返回线性表L的元素个数。
 * 
 * @param L 
 * @return int 
 */
int ListLength(LinkList L){
    int cnt = 0; //用于计数
    Node *p = L->next;

    if (ListEmpty(L))
        return 0;
    // printf("Start\n");
    while(p != L){
        p = p->next;
        ++cnt;
    }
    // printf("end ");
    return cnt;
}

/**
 * @brief 将线性表L中的第i个位置元素值返回给e。
 * 
 * @param L 
 * @param i 
 * @param e 
 * @return Status 正常则返回OK,不正常返回ERROR
 */
Status GetElem(LinkList L, int i, ElemType *e){
    int len = ListLength(L);
    int cnt = 1;
    Node *p = L->next;
    // printf("Start GetElem of %d\r\n",i);

    // assert(i >= 1 && i <= len);  //必须在范围内
    if (i<1 || i>len) // 输入有误
        return ERROR;

    while(cnt < i && p!=L){
        p = p->next;
        ++cnt;
    }
    if (cnt>i || p == L)  // 第i个元素不存在
        return ERROR;
    *e = p->data;
    return OK;
}

/**
 * @brief 删除线性表工中第i个位置元素，并用e返回其值。
 * 
 * @param L 
 * @param i 
 * @param e 
 * @return Status 
 */
Status ListDelete(LinkList *L,int i, ElemType *e)
{
    int len = ListLength((*L));
    int cnt = 1;
    Node *p, *q;
    if (i<1 || i>len)
        return ERROR;

    p = (*L);
    while(cnt != i && p->next!=(*L)){
        p = p->next;
        ++cnt;
    }
    if (cnt>i)
        return ERROR;
    q = p->next;
    p->next = q->next;
    *e = q->data;
    free(q);
    return OK;
}


/**
 * @brief 在线性表L中的第i个位置插入新元素e。如果没有第i个元素，则放最后面
 * 
 * @param L 
 * @param i 
 * @param e 
 * @return Status 
 */
Status ListInsert(LinkList *L,int i,ElemType e){
    int cnt = 1;
    Node *p,*new;
    p = (*L);
    while(cnt < i && p->next!=(*L))
    {
        p = p->next;
        cnt++;
    }
    if (cnt > i)
        return ERROR;
    new = (Node *)malloc(sizeof(Node));
    new->data = e;
    new->next = p->next;
    p->next = new;
    return OK;
}

/**
 * @brief 在线性表L中查找与给定值e相等的元素， 
 * 
 * @param L 
 * @param e 
 * @return int 如果查找成功，返回该元素在表中序号表示成功；否则，返回0表示失败
 */
int LocateElem(LinkList L, ElemType e){
    int cnt = 1;
    Node *p;
    p = L->next;
    while(p->data != e && p != L)
    {
        p = p->next;
        ++cnt;
    }
    if (p == L)
        return 0;
    return cnt;
}

/* 初始条件：顺序线性表L已存在 */
/**
 * @brief 依次对L的每个数据元素输出打印,遍历
 * 
 * @param L 
 * @return Status 
 */
Status ListTraverse(LinkList L)
{
    LinkList p = L->next;
    while (p!=L)
    {
        visit(p->data);
        p = p->next;
    }
    printf("\n");
    return OK;
}



void showMenu(const char* tips)
{
    printf("================================================================\n");
    printf("\t\t\t %s \t\t\t\t\n", tips);
    printf("----------------------------------------------------------------\n");
    return;
}



int main(int argc, char* argv[])
{
    LinkList L;
    ElemType e; //数据
    Status status;
    int i, idx;

    system("cls");//清除命令行
    Sleep(500);
    /****************************************************************/
    showMenu("初始化链表");
    // 初始循环链表
    InitList(&L);
    // 判断是否为空
    if (ListEmpty(L))
        printf("链表为空\r\n");
    printf("链表的长度为%d\r\n", ListLength(L));
    printf("链表内容: ");
    ListTraverse(L);
    /****************************************************************/
    showMenu("插入数据");
    ListInsert(&L, 1, 1);
    ListInsert(&L, 2, 2);
    ListInsert(&L, 1, 3);
    ListInsert(&L, 4, 4);
    if (ListEmpty(L))
        printf("链表为空\r\n");
    printf("链表的长度为%d\r\n", ListLength(L));
    printf("链表内容: ");
    ListTraverse(L);

    /****************************************************************/
    showMenu("获取数据");
    for (i = 1; i <= ListLength(L); i++)
    {
        GetElem(L, i, &e);
        printf("第%d个元素为：%d\r\n", i, e);
    }

    /****************************************************************/
    showMenu("定位元素");
    idx = LocateElem(L, 1);
    printf("值为1的第一个元素的位序为：%d\r\n", idx);

    /****************************************************************/
    showMenu("删除元素");
    printf("链表的长度为%d\r\n", ListLength(L));
    int len = ListLength(L);
    for (i = 1; i < len-1; i++)
    {
        ListDelete(&L, 1, &e);
        printf("删除的第%d个元素为%d\r\n",i,e);
    }
    printf("链表的长度为%d\r\n", ListLength(L));
    printf("链表内容: ");
    ListTraverse(L);

    /****************************************************************/
    showMenu("清空元素");
    ClearList(&L);
    printf("链表的长度为%d\r\n", ListLength(L));
    printf("链表内容: ");
    ListTraverse(L);

    return 0;
}

// ADT 线性表（List）
// Data
// 	线性表的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除低一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
// Operation
// 	InitList(*L):      初始化操作，建立一个空的线性表L。
// 	ListEmpty(L):      若线性表为空，返回true，否则返回false。
// 	ClearList(*L):     将线性表清空。
// 	GetElem(L, i, *e): 将线性表L中的第i个位置元素值返回给e。
// 	LocateElem(L, e):  在线性表L中查找与给定值e相等的元素，如果查找成功，返回该元素在表中序号表示成功；否则，返回0表示失                        败。
// 	ListInsert(*L, i, e):  在线性表L中的第i个位置插入新元素e。
// 	ListDelete(*L, i, *e): 删除线性表工中第i个位置元素，并用e返回其值。
// 	ListLength(L):     返回线性表L的元素个数。
// endADT