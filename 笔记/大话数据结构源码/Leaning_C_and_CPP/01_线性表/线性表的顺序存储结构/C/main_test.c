#include "order_list.h"
#include <windows.h>

void showMenu(const char* tips)
{
    printf("================================================================\n");
    printf("\t\t\t %s \t\t\t\t\n", tips);
    printf("----------------------------------------------------------------\n");
}

int main()
{

    SqList L;
    SqList Lb;

    ElemType e;
    Status status;
    int j, k;

    system("cls");//清除命令行
    Sleep(500);
    /*初始化操作*/
    /***************************************************************/
    showMenu("初始化");
    status = InitList(&L);
    printf("初始化L后：L.length=%d\n", L.length);

    /*头插*/
    /***************************************************************/
    showMenu("头插");
    for (j = 1; j <= 5; j++)
        status = ListInsert(&L, 1, j);
    printf("在L的表头依次插入1～5后：L.data=");
    ListTraverse(L);

    /***************************************************************/
    showMenu("判断是否为空");
    printf("L.length=%d \n", L.length);
    status = ListEmpty(L);
    printf("L是否空：i=%d(1:是 0:否)\n", status);

    /***************************************************************/
    showMenu("清除线性表");
    status = ClearList(&L);
    printf("清空L后：L.length=%d\n", L.length);
    status = ListEmpty(L);
    printf("L是否空：i=%d(1:是 0:否)\n", status);


    /***************************************************************/
    showMenu("尾插");
    for (j = 1; j <= 10; j++)
        ListInsert(&L, j, j);
    printf("在L的表尾依次插入1～10后：L.data=");
    ListTraverse(L);

    printf("L.length=%d \n", L.length);

    /***************************************************************/
    showMenu("在表头插入0");
    ListInsert(&L, 1, 0);
    printf("在L的表头插入0后：L.data=");
    ListTraverse(L);
    printf("L.length=%d \n", L.length);

    /***************************************************************/
    showMenu("获取元素");
    GetElem(L, 5, &e);
    printf("第5个元素的值为：%d\n", e);
    for (j = 3; j <= 4; j++)
    {
        k = LocateElem(L, j);
        if (k)
            printf("第%d个元素的值为%d\n", k, j);
        else
            printf("没有值为%d的元素\n", j);
    }


    /***************************************************************/
    showMenu("删除数据");
    k = ListLength(L); /* k为表长 */
    for (j = k + 1; j >= k; j--)
    {
        status = ListDelete(&L, j, &e); /* 删除第j个数据 */
        if (status == ERROR)
            printf("删除第%d个数据失败\n", j);
        else
            printf("删除第%d个的元素值为：%d\n", j, e);
    }
    printf("依次输出L的元素：");
    ListTraverse(L);

    j = 5;
    ListDelete(&L, j, &e); /* 删除第5个数据 */
    printf("删除第%d个的元素值为：%d\n", j, e);

    printf("依次输出L的元素：");
    ListTraverse(L);

    /***************************************************************/
    showMenu("unionL操作");
    //构造一个有10个数的Lb
    status = InitList(&Lb);
    for (j = 6; j <= 15; j++)
        status = ListInsert(&Lb, 1, j);

    unionL(&L, Lb);

    printf("依次输出合并了Lb的L的元素：");
    ListTraverse(L);

    return 0;
}
