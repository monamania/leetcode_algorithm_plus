#ifndef __ORDER_LIST_
#define __ORDER_LIST_ 

/**
 * @file order_list.h
 * @author Monomania(13015159350@qq.com)
 * @brief 这是线性表的顺序存储结构例子
 *          ————即使用数组存储
 * @version 0.1
 * @date 2021-08-30
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/**
 * @抽象数据类型

ADT 线性表（List）
Data
	线性表的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除低一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitList(*L):      初始化操作，建立一个空的线性表L。
	ListEmpty(L):      若线性表为空，返回true，否则返回false。
	ClearList(*L):     将线性表清空。
	GetElem(L, i, *e): 将线性表L中的第i个位置元素值返回给e。
	LocateElem(L, e):  在线性表L中查找与给定值e相等的元素，如果查找成功，返回该元素在表中序号表示成功；否则，返回0表示失                        败。
	ListInsert(*L, i, e):  在线性表L中的第i个位置插入新元素e。
	ListDelete(*L, i, *e): 删除线性表工中第i个位置元素，并用e返回其值。
	ListLength(L):     返回线性表L的元素个数。
endADT

优点：可以快速的存取
缺点：
    插入和删除效率低
    长度不好确定
    容易造成存储空间的“碎片”

 */ 

/**
 * @使用说明 
 * 1. 将order_list.h，order_list.c文件包含在工程文件中
 * 2. 根据需求修改初始分配量MAXSIZE
 * 3. 根据需求，修改元素类型ElemType
 * 4. 根据数据元素，修改visit函数————自定义数据输出格式
 */

#include "stdio.h"

#include "stdlib.h"
#include "io.h"
#include "math.h"
#include "time.h"
/**
 * 
 * 要对数据本体操作的就用指针
 * 
 * 优点：可以快速的存取
 * 缺点：
 *      插入和删除效率低
 *      长度不好确定
 *      容易造成存储空间的“碎片”
 */

/**
 * @使用说明 
 * 1. 将文件包含在工程文件中
 * 2. 根据需求修改初始分配量MAXSIZE
 * 3. 根据需求，修改元素类型ElemType
 * 4. 根据数据元素，修改visit函数————自定义数据输出格式
 * 
 */

#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

#define MAXSIZE 20 /* 存储空间初始分配量 */

typedef int Status;   /* Status是函数的类型,其值是函数结果状态代码，如OK等 */
typedef int ElemType; /* ElemType类型根据实际情况而定，这里假设为int */

// 用于打印数据
Status visit(ElemType c);

// 定义线性表
typedef struct
{
    ElemType data[MAXSIZE]; /* 数组，存储数据元素 */
    int length;             /* 线性表当前长度 */
} SqList;


/**
 * @brief 初始化线性表
 * 
 * @param L 线性表的指针
 * @return Status 
 */
Status InitList(SqList *L);

/**
 * @brief 判断是否为空
 * 
 * @param L 线性表的指针
 * @return 若L为空表，则返回TRUE，否则返回FALSE
 */
Status ListEmpty(SqList L);

/**
 * @brief 将线性表置为空表
 * 
 * @param L 线性表的指针
 * @return Status 
 */
Status ClearList(SqList *L);

/**
 * @brief 返回线性表中元素个数
 * 
 * @param L 线性表
 * @return 返回元素个数
 */
int ListLength(SqList L);

/**
 * @brief 用e返回L中第i个数据元素的值,注意i是指位置，第1个位置的数组是从0开始
 * 
 * @param L 线性表
 * @param i 第i个元素
 * @param e 
 * @return Status 成功返回TRUE，否则返回FALSE
 */
Status GetElem(SqList L, int i, ElemType *e);

/**
 * @brief 定位元素在线性表中的位置（返回第一个）
 * 
 * @param L 线性表
 * @param e 要查找的元素
 * @return int 若存在返回位序，不存在返回0。。注意！位序从1开始
 */
int LocateElem(SqList L, ElemType e);

/* 初始条件：顺序线性表L已存在,1≤i≤ListLength(L)， */
/* 操作结果：在L中第i个位置之前插入新的数据元素e，L的长度加1 */
/**
 * @brief 在L中第i个位置之前插入新的数据元素e，L的长度加1
 * 
 * @param L 顺序线性表L已存在。L的指针
 * @param i 要放在第几个元素的前面
 * @param e 要存放的内容
 * @return Status 
 */
Status ListInsert(SqList *L, int i, ElemType e);

/* 初始条件：顺序线性表L已存在，1≤i≤ListLength(L) */
/* 操作结果：删除L的第i个数据元素，并用e返回其值，L的长度减1 */
/**
 * @brief 删除第i个元素
 * 
 * @param L 
 * @param i 
 * @param e 
 * @return Status 
 */
Status ListDelete(SqList *L, int i, ElemType *e);

/* 初始条件：顺序线性表L已存在 */
/* 操作结果：依次对L的每个数据元素输出 */
/**
 * @brief 遍历输出线性表元素
 * 
 * @param L 
 * @return Status 
 */
Status ListTraverse(SqList L);

/**
 * @brief 将所有的在线性表Lb中但不在La中的数据元素插入到La中
 * 
 * @param La 由于要对La有数据操作，故而使用指针
 * @param Lb 由于不需要对Lb有数据操作，防止误操作，采用拷贝赋值
 */
void unionL(SqList *La, SqList Lb);


#endif // !__ORDER_LIST_