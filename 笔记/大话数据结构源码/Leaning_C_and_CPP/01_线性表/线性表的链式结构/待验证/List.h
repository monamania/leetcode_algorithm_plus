#ifndef __LIST_H__
#define __LIST_H__


#pragma once
//https://blog.csdn.net/qq_37941471/article/details/80458397

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <windows.h>
#include <assert.h>

typedef int DataType;
typedef struct ListNode
{
    DataType data;//节点存放的数据
    struct ListNode* next;//下一个节点
}ListNode;


ListNode* BuyNode(DataType x);//创建节点
void PrintList(ListNode *plist);//打印链表

void PushBack(ListNode **pplist,DataType x);//尾插
void PopBack(ListNode **pplist);//尾删

void PushFront(ListNode **pplist,DataType x);//头插
void PopFront(ListNode **pplist);//头删

ListNode* Find(ListNode *plist,DataType x);//查找x

void Insert(ListNode **pplist,ListNode* pos,DataType x);//在pos位置前面插入节点
void Erase(ListNode **pplist,ListNode *pos);//删除pos节点

void PrintTailToHead(ListNode *plist);//2.从尾到头打印单链表 
void EraseNonTail(ListNode* pos);//3.删除一个无头单链表的非尾节点 
void InsertNonHead(ListNode* pos,DataType x);//4.在无头单链表的一个节点前插入一个节点 
ListNode* JosepRing(ListNode* list,DataType k);//5.单链表实现约瑟夫环
void Reverse(ListNode** pplist);//6.翻转/逆置链表
void SortList(ListNode* list);//7.单链表排序（冒泡排序）
ListNode* Merge(ListNode* list1,ListNode* list2);//8.合并两个有序单链表，合并后依然有序
ListNode* FindMidNode(ListNode* plist);//9.查找链表的中间节点，要求只能遍历一次
void FindKNode(ListNode* plist,int k);//10.查找单链表的倒数第k个节点，要求只能遍历一次

ListNode* IsCycle(ListNode* plist);//11.判断链表是否带环？
ListNode* GetEntry(ListNode* plist, ListNode* MeetNode);//若带环，求其入口点
int GetCycle_Length(ListNode* MeetNode);//若带环，求环的长度

int IsCross_NoCycle(ListNode* plist1,ListNode* plist2);//12.判断两个链表是否相交？（假设链表不带环）
ListNode* GetCrossNode(ListNode* plist1, ListNode* plist2);//若相交，求交点

ListNode* IsCross_Cycle(ListNode* plist1,ListNode* plist2);//13.判断两个链表是否相交？若相交，求交点。(假设链表可能带环)

#endif // !__LIST_H__