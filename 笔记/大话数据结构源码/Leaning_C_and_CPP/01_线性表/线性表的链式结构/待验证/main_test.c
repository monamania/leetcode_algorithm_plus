#include "List.h"

void test1()
{
    ListNode* list = NULL;

    PushBack(&list,1);
    PushBack(&list,2);
    PushBack(&list,3);
    PushBack(&list,4);
    PrintList(list);

    PopBack(&list);//尾删
    PrintList(list);

    PopBack(&list);
    PopBack(&list);
    PopBack(&list);
    PopBack(&list);//一个节点
    PrintList(list);

    PopBack(&list);
    PopBack(&list);
    PopBack(&list);
    PopBack(&list);
    PopBack(&list);//空链表
    PrintList(list);
}

void test2()
{
    ListNode* list = NULL;

    PushFront(&list,4);
    PushFront(&list,3);
    PushFront(&list,2);
    PushFront(&list,1);
    PrintList(list);

    PopFront(&list);//尾删
    PrintList(list);

    PopFront(&list);
    PopFront(&list);
    PopFront(&list);
    PopFront(&list);//一个节点
    PrintList(list);

    PopFront(&list);
    PopFront(&list);
    PopFront(&list);
    PopFront(&list);
    PopFront(&list);//空链表
    PrintList(list);
}

void test3()
{
    ListNode* list = NULL;
    ListNode* pos;

    PushFront(&list,4);
    PushFront(&list,3);
    PushFront(&list,2);
    PushFront(&list,1);
    PrintList(list);

    /*pos = Find(list,1);
    Insert(&list,pos,0);
    PrintList(list);*/

    /*pos = Find(list,1);
    Insert(&list,pos,0);
    PrintList(list);*/

    pos = Find(list,3);//找不到时返回NULL，则变成了尾插
    Insert(&list,pos,0);
    PrintList(list);

    Erase(&list,pos);
    PrintList(list);
}

void test4()
{
    ListNode* list = NULL;
    ListNode* pos;

    PushFront(&list,4);
    PushFront(&list,3);
    PushFront(&list,2);
    PushFront(&list,1);
    PrintTailToHead(list);
    printf("\n");

    /*pos = Find(list,2);
    EraseNonTail(pos);
    PrintList(list);*/

    pos = Find(list,4);//非尾节点,而4是尾节点
    EraseNonTail(pos);
    PrintList(list);
}

void test5()
{
    ListNode* list = NULL;
    ListNode* pos;

    PushFront(&list,4);
    PushFront(&list,3);
    PushFront(&list,1);
    PrintList(list);

    pos = Find(list,3);
    InsertNonHead(pos,2);
    PrintList(list);
}

void test6()
{
    ListNode* list = NULL;
    ListNode* tail ;
    ListNode* man;
    PushBack(&list,1);
    PushBack(&list,2);
    PushBack(&list,3);
    PushBack(&list,4);
    PushBack(&list,5);
    PushBack(&list,6);
    PushBack(&list,7);
    PushBack(&list,8);
    PrintList(list);

    tail = Find(list,8);
    tail->next = list;

    man =  JosepRing(list,3);

    printf("幸存者：%d \n",man->data);

}
void test7()
{
    ListNode* list = NULL;

    PushFront(&list,6);
    PushFront(&list,4);
    PushFront(&list,8);
    PushFront(&list,1);
    PrintList(list);

    //Reverse(&list);
    SortList(list);
    PrintList(list);
}

void test8()
{
    ListNode* list1 = NULL;
    ListNode* list2 = NULL;
    ListNode* list = NULL;
    //ListNode* mid;
    PushBack(&list1,1);
    PushBack(&list1,2);
    PushBack(&list1,5);
    PushBack(&list1,7);
    PrintList(list1);

    PushBack(&list2,3);
    PushBack(&list2,4);
    PushBack(&list2,8);
    PushBack(&list2,9);
    PrintList(list2);

    list =  Merge(list1,list2);
    PrintList(list);
}

void test9()
{
    ListNode* list = NULL;
    ListNode* mid;

    PushFront(&list,4);
    PushFront(&list,3);
    PushFront(&list,2);
    PushFront(&list,1);
    PrintList(list);

    mid = FindMidNode(list);
    printf("中间节点是%d\n",mid->data);
    FindKNode(list,2);
    PrintList(list);
}

void test10()
{
    ListNode* list = NULL;
    ListNode* tail;
    ListNode* entry;//入口点
    ListNode* meet;//相遇点
    int length ;

    PushBack(&list, 1);
    PushBack(&list, 2);
    PushBack(&list, 3);
    PushBack(&list, 4);
    PushBack(&list, 5);
    PushBack(&list, 6);
    PrintList(list);

    tail = Find(list, 6);
    entry = Find(list, 4);
    tail->next = entry;//带环

    meet = IsCycle(list);
    printf("入口点：%d\n", GetEntry(list, meet)->data);

    length = GetCycle_Length(meet);
    printf("环的长度为%d\n",length);
}

void test11()
{
    ListNode* cross, *tail;
    ListNode* list1 = NULL;
    ListNode* list2 = NULL;

    PushBack(&list1, 1);
    PushBack(&list1, 2);
    PushBack(&list1, 3);
    PrintList(list1);

    PushBack(&list2, 4);
    PushBack(&list2, 5);
    PushBack(&list2, 6);
    PushBack(&list2, 7);
    PushBack(&list2, 8);
    PushBack(&list2, 9);
    PrintList(list2);

    cross = Find(list2, 8);
    tail = Find(list1, 3);
    tail->next = cross;

    //判断两个链表是否相交？
    if( 1 == IsCross_NoCycle(list1,list2) )
        printf("两个链表相交，");
    //求交点
    printf("交点是:%d\n", GetCrossNode(list1, list2)->data);
}

void test12()//判断两个链表是否相交？相同的入口点
{
    ListNode* cross, *tail1,*tail2;
    ListNode* entry;
    ListNode* list1 = NULL;
    ListNode* list2 = NULL;
    ListNode* meet;

    PushBack(&list1, 1);
    PushBack(&list1, 2);
    PushBack(&list1, 3);
    PrintList(list1);

    PushBack(&list2, 4);
    PushBack(&list2, 5);
    PushBack(&list2, 6);
    PushBack(&list2, 7);
    PushBack(&list2, 8);
    PushBack(&list2, 9);
    PushBack(&list2, 10);
    PushBack(&list2, 11);
    PrintList(list2);

    cross = Find(list2, 8);//交点
    tail1 = Find(list1, 3);
    tail1->next = cross;

    tail2 = Find(list2, 11);
    entry = Find(list2, 9);//入口点
    tail2->next = entry;//带环
    meet = IsCross_Cycle(list1,list2);
    printf("交点是：%d\n",meet->data);
}
int main()
{
    // test1();//尾插 尾删
    //test2();//尾插 尾删
    //test3();//查找K pos前插入节点 删除pos节点

    //test4();//2.从尾到头打印单链表 3.删除一个无头单链表的非尾节点 
    //test5();//4.在无头单链表的一个节点前插入一个节点 
    //test6();//5.单链表实现约瑟夫环
    //test7();//6.翻转/逆置链表  7.单链表排序（冒泡排序）
    //test8();//8.合并两个有序单链表，合并后依然有序
    //test9();//9.查找链表的中间节点，要求只能遍历一次 10.查找单链表的倒数第k个节点，要求只能遍历一次

    //test10();//11.判断单链表是否带环？若带环，求环的入口点;若带环，求环的长度
    //test11();//12.判断两个链表是否相交？若相交，求交点（假设链表不带环）
    test12();//13.判断两个链表是否相交？若相交，求交点。(假设链表可能带环)
    return 0;
}