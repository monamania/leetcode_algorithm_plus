/**
 * @file linked_list.c
 * @author Monomania(13015159350@qq.com)
 * @brief 这是线性表的链式存储结构例子
 *          ————即使用链表存储
 * @version 0.1
 * @date 2021-08-30
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "linked_list.h"

/**
 * @brief 用e返回第i个元素的值
 * 
 * @param L 链表
 * @param i 第i个元素
 * @param e 用于存储元素的值
 * @return Status 
 */

Status visit(ElemType c)
{
    printf("%d ", c);
    return OK;
}

/**
 * @brief 初始化线性表
 * 
 * @param L 待操作的链表
 * @return Status 
 */
Status InitList(LinkList *L)
{
    *L = (Node *)malloc(sizeof(Node)); /* 产生头结点,并使L指向此头结点 Node*强调生成的是一个节点，头节点*/
    // *L = (LinkList)malloc(sizeof(Node)); /* 产生头结点,并使L指向此头结点 LinkList强调生成的一个链表*/
    if (!(*L))                           /* 存储分配失败 */
        return ERROR;
    (*L)->next = NULL; /* 指针域为空 */

    return OK;
}


/* 初始条件：顺序线性表L已存在。 */
/**
 * @brief 判断是否为空
 * 
 * @param L 
 * @return Status 若L为空表，则返回TRUE，否则返回FALSE
 */
Status ListEmpty(LinkList L)
{
    if (L->next)
        return FALSE;
    else
        return TRUE;
}


/* 初始条件：顺序线性表L已存在。操作结果：将L重置为空表 */
/**
 * @brief 清空链表里的数据
 * 
 * @param L 由于是对原链表操作，故而使用*
 * @return Status 
 */
Status ClearList(LinkList *L)
{
    LinkList p, q;  //强调这是一个链表
    p = (*L)->next; /*  p指向第一个结点 */
    while (p)       /*  没到表尾 */
    {
        q = p->next;  //提前记住下一个节点，防止删除后忘了
        free(p);
        p = q;
    }
    (*L)->next = NULL; /* 头结点指针域为空 */
    return OK;
}


/* 初始条件：顺序线性表L已存在。 */
/**
 * @brief 获取链表长度
 * 
 * @param L 
 * @return int 返回L中数据元素个数
 */
int ListLength(LinkList L)
{
    int i = 0;
    Node* p = L->next;
    // printf("Start\n");
    while(p)
    {
        i++;
        p = p->next;
    }
    // printf("end");
    return i;
}

/* 初始条件：顺序线性表L已存在，1≤i≤ListLength(L) */
/**
 * @brief 获取链表第i个元素
 * 
 * @param L 
 * @param i 第几个元素，i从1开始
 * @param e 
 * @return Status 用e返回L中第i个数据元素的值 
 */
Status GetElem(LinkList L, int i, ElemType *e)
{
    if ( i<1 )  //输入有误
        return ERROR;
    int cnt = 1; //用于计数，是否到了第i个节点
    Node *p;  /* 声明一结点p */
    p = L->next;   /* 让p指向链表L的第一个结点 */
    while (p && cnt < i)    /* p不为空并且计数器cnt还没有等于i时，循环继续 */
    {
        p = p->next;
        cnt++;
    }
    if (!p || cnt > i)  // 第i个元素不存在
        return ERROR;
    *e = p->data;
    return OK;
}

/* 初始条件：顺序线性表L已存在 */
/**
 * @brief 定位元素的位序
 * 
 * @param L 
 * @param e 元素值
 * @return int 位序，若不存在返回0
 */
int LocateElem(LinkList L, ElemType e)
{
    int i = 0;
    Node *p = L->next;
    while (p)
    {
        i++;
        if (p->data == e) /* 找到这样的数据元素 */
            return i;
        p = p->next;
    }

    return 0;
}

/* 初始条件：顺序线性表L已存在,1≤i≤ListLength(L)， */
/**
 * @brief 在L中第i个位置之前插入新的数据元素e，L的长度加1
 * 
 * @param L 
 * @param i 位序，从1开始
 * @param e 要插入的数据
 * @return Status 
 */
Status ListInsert(LinkList *L, int i, ElemType e)
{
    int cnt = 1; //计数器，找到第i个元素
    Node *p, *s;  //p用于遍历  s用于生成新的节点
    // p = (*L)->next;  //为什么不用next,是为了定位第i个节点的前一个节点
    p = (*L);
    while (p && cnt < i)  //定位到第i个节点的前一个节点
    {
        p = p->next;
        cnt++;
    }
    if (!p || cnt > i)
        return ERROR;                   /* 第i个元素不存在 */
    s = (Node *)malloc(sizeof(Node));   /*  生成新结点(C语言标准函数) */
    s->data = e;
    s->next = p->next;   /* 将p的后继结点赋值给s的后继  */
    p->next = s;        /* 将s赋值给p的后继 */
    return OK;
}


/* 初始条件：顺序线性表L已存在，1≤i≤ListLength(L) */
/**
 * @brief 删除L的第i个数据元素，并用e返回其值，L的长度减1 
 * 
 * @param L 
 * @param i 
 * @param e 用于存储返回的值
 * @return Status 
 */
Status ListDelete(LinkList *L, int i, ElemType *e)
{
    int cnt = 1; //计数器
    Node *p, *q;
    p = (*L);
    while (p->next && cnt < i)  //定位到第i个节点的前一个节点
    {
        p = p->next;
        cnt++;
    }
    if (!(p->next) || cnt > i)  /* 第i个元素不存在 */
        return ERROR;
    q = p->next;   //要删除的节点
    p->next = q->next;  /* 将q的后继赋值给p的后继 */
    *e = q->data;       /* 将q结点中的数据给e */
    free(q);            /* 让系统回收此结点，释放内存 */
    return OK;
}


/* 初始条件：顺序线性表L已存在 */
/**
 * @brief 依次对L的每个数据元素输出打印
 * 
 * @param L 
 * @return Status 
 */
Status ListTraverse(LinkList L)
{
    LinkList p = L->next;
    while (p)
    {
        visit(p->data);
        p = p->next;
    }
    printf("\n");
    return OK;
}



/*  随机产生n个元素的值，建立带表头结点的单链线性表L（头插法） */
/**
 * @brief 随机产生n个元素的值，并采用头插法建立链表
 * 
 * @param L 
 * @param n 
 */
Status CreateListHead(LinkList *L, int n)
{
    LinkList p;
    int i;
    srand(time(0)); /* 初始化随机数种子 */
    *L = (LinkList)malloc(sizeof(Node));
    (*L)->next = NULL; /*  先建立一个带头结点的单链表 */
    for (i = 0; i < n; i++)
    {
        p = (LinkList)malloc(sizeof(Node)); /*  生成新结点 */
        p->data = rand() % 100 + 1;         /*  随机生成100以内的数字 */
        p->next = (*L)->next;
        (*L)->next = p; /*  插入到表头 */
    }

    return OK;
}

/*  随机产生n个元素的值，建立带表头结点的单链线性表L（尾插法） */
/**
 * @brief 随机产生n个元素的值，并采用尾插法建立链表
 * 
 * @param L 
 * @param n 
 */
Status CreateListTail(LinkList *L, int n)
{
    LinkList p, r;
    int i;
    srand(time(0));                      /* 初始化随机数种子 */
    *L = (LinkList)malloc(sizeof(Node)); /* L为整个线性表 */
    r = *L;                              /* r为指向尾部的结点 */
    for (i = 0; i < n; i++)
    {
        p = (Node *)malloc(sizeof(Node)); /*  生成新结点 */
        p->data = rand() % 100 + 1;       /*  随机生成100以内的数字 */
        r->next = p;                      /* 将表尾终端结点的指针指向新结点 */
        r = p;                            /* 将当前的新结点定义为表尾终端结点 */
    }
    r->next = NULL; /* 表示当前链表结束 */

    return OK;
}

