#ifndef __LINKED_LIST_H__
#define __LINKED_LIST_H__ 
#include "stdio.h"
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "io.h"
#include "math.h"
#include "time.h"
/**
 * @file linked_list.h
 * @author Monomania(13015159350@qq.com)
 * @brief 这是线性表的链式存储结构例子
 *          ————即使用链表存储
 * @version 0.1
 * @date 2021-08-30
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/**
 *  * 抽象数据类型

ADT 线性表（List）
Data
	线性表的数据对象集合为{a1, a2, ..., an}，每个元素的类型均为DataType。其中除低一个元素a1外，每一个元素有且仅有一个直接前驱元素，除了最后一个元素an外，每一个元素有且仅有一个直接后继元素。数据元素之间的关系是一对一的关系。
Operation
	InitList(*L):      初始化操作，建立一个空的线性表L。
	ListEmpty(L):      若线性表为空，返回true，否则返回false。
	ClearList(*L):     将线性表清空。
	GetElem(L, i, *e): 将线性表L中的第i个位置元素值返回给e。
	LocateElem(L, e):  在线性表L中查找与给定值e相等的元素，如果查找成功，返回该元素在表中序号表示成功；否则，返回0表示失                        败。
	ListInsert(*L, i, e):  在线性表L中的第i个位置插入新元素e。
	ListDelete(*L, i, *e): 删除线性表工中第i个位置元素，并用e返回其值。
	ListLength(L):     返回线性表L的元素个数。
endADT
*/
/**
 * @使用说明 
 * 1. 将linked_list.h，linked_list.c文件包含在工程文件中
 * 2. 根据需求，修改元素类型ElemType
 * 3. 根据需求修改visit()函数，自定义输出格式
 * 
 * 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作。————就算链表本来就是个指针，也要遵守这个准则!!
 */


typedef int ElemType;       // 元素类型 ElemType

//单链表存储结构
/* 注意
 * 强调是单链表时使用  LinkList
 * 强调是一个节点时使用 Node* 
 * 在参数列表中，如果有*（或者C++的&）表示要对原数据进行操作【不论是参数还是函数内部】。————就算链表本来就是个指针，也要遵守这个准则!!
 */
typedef struct Node{
    ElemType data; 
    struct Node *next;   
} Node; 
typedef struct Node *LinkList;


#define OK 1
#define ERROR 0
#define TRUE  1
#define FALSE 0
typedef int Status;

//根据需求修改visit()函数，自定义输出格式
Status visit(ElemType c);

/**
 * @brief 初始化线性表
 * 
 * @param L 待操作的链表
 * @return Status 
 */
Status InitList(LinkList *L);

// 判断是否为空
Status ListEmpty(LinkList L);
// 清空链表
Status ClearList(LinkList *L);
//获取链表长度
int ListLength(LinkList L);
// 获得第i个元素，并将元素值存在e中
Status GetElem(LinkList L, int i, ElemType *e);
// 定位元素的位序（从1开始），不存在则返回0
int LocateElem(LinkList L, ElemType e);
// 在L中第i个位置之前插入新的数据元素e，L的长度加1
Status ListInsert(LinkList *L, int i, ElemType e);
// 删除第i个元素
Status ListDelete(LinkList *L, int i, ElemType *e);

// 依序输出打印
Status ListTraverse(LinkList L);

// 随机产生n个元素的值，并采用头插法建立链表
Status CreateListHead(LinkList *L, int n);
// 随机产生n个元素的值，并采用尾插法建立链表
Status CreateListTail(LinkList *L, int n);

#endif // !__LINKED_LIST_H__