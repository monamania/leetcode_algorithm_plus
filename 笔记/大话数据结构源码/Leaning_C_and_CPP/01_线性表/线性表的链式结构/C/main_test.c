#include "linked_list.h"
#include <windows.h>

void showMenu(const char* tips)
{
    printf("================================================================\n");
    printf("\t\t\t %s \t\t\t\t\n", tips);
    printf("----------------------------------------------------------------\n");
    return;
}


//插入数据
void test01_insert()
{
    LinkList L;
    ElemType e; //数据
    Status status;
    int i, k;

    system("cls");//清除命令行
    Sleep(500);
    /****************** 初始化链表 **********************/
    // showMenu("初始化链表");
    status = InitList(&L);
    printf("初始化L后：ListLength(L)=%d\n", ListLength(L));

    /****************** 在表头插入数据 **********************/
    showMenu("在表头插入数据");
    for (i = 1; i <= 5; i++)
        status = ListInsert(&L, 1, i);
    printf("在L的表头依次插入1～5后：L.data=");
    ListTraverse(L);

    printf("ListLength(L)=%d \n", ListLength(L));
    status = ListEmpty(L);
    printf("L是否空：i=%d(1:是 0:否)\n", status);

    /****************** 清空数据 **********************/
    showMenu("清空数据");
    status = ClearList(&L);
    printf("清空L后：ListLength(L)=%d\n", ListLength(L));
    status = ListEmpty(L);
    printf("L是否空：i=%d(1:是 0:否)\n", status);

}


//获取数据
void test02_get()
{
    LinkList L; 
    ElemType e; //数据
    Status status;
    int i, k;
    /****************** 初始化链表 **********************/
    // showMenu("初始化链表");
    status = InitList(&L);
    printf("初始化L后：ListLength(L)=%d\n", ListLength(L));

    /****************** 在表头插入数据 **********************/
    showMenu("在表头插入数据");
    for (i = 1; i <= 10; i++)
        ListInsert(&L, i, i);
    printf("在L的表尾依次插入1～10后：L.data=");
    ListTraverse(L);

    printf("ListLength(L)=%d \n", ListLength(L));

    ListInsert(&L, 1, 0);
    printf("在L的表头插入0后：L.data=");
    ListTraverse(L);
    printf("ListLength(L)=%d \n", ListLength(L));

    /****************** 获取元素 **********************/
    showMenu("获取元素");
    GetElem(L, 5, &e);
    printf("第5个元素的值为：%d\n", e);
    for (i = 3; i <= 4; i++)
    {
        k = LocateElem(L, i);
        if (k)
            printf("第%d个元素的值为：%d\n", k, i);
        else
            printf("没有值为%d的元素：\n", i);
    }
}

//头插法创建数据
void test03_create_list_head()
{
    LinkList L; 
    ElemType e; //数据
    Status status;
    int i, k;
    /****************** 初始化链表 **********************/
    // showMenu("初始化链表");
    status = InitList(&L);
    printf("初始化L后：ListLength(L)=%d\n", ListLength(L));
    /****************** 头插法创建链表 **********************/
    showMenu("头插法创建链表");
    status = ClearList(&L);
    printf("\n清空L后：ListLength(L)=%d\n", ListLength(L));
    CreateListHead(&L, 20);
    printf("整体创建L的元素(头插法)：");
    ListTraverse(L);
}

//尾插法创建数据
void test04_create_list_tail()
{
    LinkList L; 
    ElemType e; //数据
    Status status;
    int i, k;
    /****************** 初始化链表 **********************/
    // showMenu("初始化链表");
    status = InitList(&L);
    printf("初始化L后：ListLength(L)=%d\n", ListLength(L));
    /****************** 头插法创建链表 **********************/
    showMenu("头插法创建链表");
    status = ClearList(&L);
    printf("\n清空L后：ListLength(L)=%d\n", ListLength(L));
    CreateListHead(&L, 20);
    printf("整体创建L的元素(头插法)：");
    ListTraverse(L);
    /****************** 尾插法创建链表 **********************/
    showMenu("尾插法创建链表");
    status = ClearList(&L);
    printf("\n删除L后：ListLength(L)=%d\n", ListLength(L));
    CreateListTail(&L, 20);
    printf("整体创建L的元素(尾插法)：");
    ListTraverse(L);
}

//删除数据
void test05_delete()
{
    LinkList L; 
    ElemType e; //数据
    Status status;
    int i, k;
    /****************** 初始化链表 **********************/
    // showMenu("初始化链表");
    status = InitList(&L);
    printf("初始化L后：ListLength(L)=%d\n", ListLength(L));

    /****************** 头插法创建链表 **********************/
    showMenu("头插法创建链表");
    status = ClearList(&L);
    printf("\n清空L后：ListLength(L)=%d\n", ListLength(L));
    CreateListHead(&L, 20);
    printf("整体创建L的元素(头插法)：");
    ListTraverse(L);

    /****************** 删除元素 **********************/
    showMenu("删除元素");
    k = ListLength(L); /* k为表长 */
    printf("表长为：%d\n", k);
    for (i = k + 1; i >= k; i--)
    {;
        status = ListDelete(&L, i, &e); /* 删除第j个数据 */
        if (status == ERROR)
            printf("删除第%d个数据失败\n", i);
        else
            printf("删除第%d个的元素值为：%d\n", i, e);
    }
    printf("依次输出L的元素：");
    ListTraverse(L);

    i = 5;
    ListDelete(&L, i, &e); /* 删除第5个数据 */
    printf("删除第%d个的元素值为：%d\n", i, e);

    printf("依次输出L的元素：");
    ListTraverse(L);
}

int main(int argc, char** argv)
{
    
    int i,k;
    test01_insert(); //初始化、插入数据、清空数据
    test02_get(); // 获取数据
    test03_create_list_head();//头插法创建数据
    test04_create_list_tail();//尾插法创建数据
    test05_delete(); ////删除数据

    return 0;
}