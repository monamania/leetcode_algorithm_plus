#include <stdio.h>

int main(int argc, char** argv)
{
    char str[6] = {'1','2','3','4','5','\0'};
    // char str2[10] = str;  //不能直接赋值
    printf("%s\r\n",str);
    printf("%d\r\n", str[4]);
    return 0;
}