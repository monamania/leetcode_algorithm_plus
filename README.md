# LeetCode_Algorithm_Plus

## 介绍
- **主要内容**：力扣刷题记录 + 部分学习笔记
  - 其中数据结构与算法部分笔记主要参考：王道考研+《大话数据结构》
- **笔记部分**：
  - 记录刷题过程中的知识点
    - [C++容器](笔记/C++容器)
    - [十大排序算法](笔记/十大排序算法/十大排序算法.md)
    - [string 去除空格](笔记/C++容器/字符串string/去除空格.cpp)
  - [shell编程笔记](笔记/Shell脚本编程笔记.md)
  - [数据结构与算法笔记](笔记/数据结构与算法.md)
  - [大话数据结构相关源码笔记](笔记/大话数据结构)
  - [实用小函数](笔记/实用小函数)
    - 语言不限，重在思想
    - 01_swap交换两数.c——按位操作，实现不需要中间变量的交换
    - 02_打印数据在内存中的存储情况
    - 03_判断两个参数相乘是否会溢出
    - 04_reverseList（利用指针翻转）
    - 04_reverseList（利用swap函数）
    - 05_IsBigEndian_判断系统大小端----包括大小端转换程序
- **刷题部分**：分为简单、中等、困难，并总结记录了相关算法的适用题目
- **主要语言**：C、C++、python
  - 后期可能会考虑加入go语言
- **联系方式**：monomania_no1@163.com


[代码随想录-学习](https://programmercarl.com/)
[远程开发VScode](https://blog.csdn.net/zy_workjob/article/details/104400805)
[Git的使用](https://monamania.gitee.io/posts/%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6%E5%B7%A5%E5%85%B7/git_command/)
 
## 注意
- 每次上传前请运行此文件清理一下无用的可执行文件：
  - windows下：del_exe.bat
  - linux下：clear_out.sh
  - Git提交说明：https://juejin.cn/post/6951219502319140878
  > 【注意】不要给这两个文件增加可执行权限，会被清理
  >  文件及文件夹命名时，不要包含空格，否则会清理失败


Git提交例程：
```sh
git commit -m '<type>(<scope>): <subject> [<issue_id>]'
git commit -m '新增(影响全局select组件UI样式): 更改了select组件动画效果 [issue-123]'

# 可选
feat【新增】    ———— Feature的缩写, 新的功能或特性
fix【bug修复】  ———— bug的修复
docs【文档】    ———— 文件修改, 比如修改应用了ngDoc的项目的ngDoc内容
style【界面】   ———— 格式修改. 比如改变缩进, 空格, 删除多余的空行, 补上漏掉的分号. 总之, 就是不影响代码含义和功能的修改
refractor【重构】———— 代码重构. 一些不算修复bug也没有加入新功能的代码修改
perf【优化】    ———— Performance的缩写, 提升代码性能
test【测试】    ———— 测试文件的修改
chore【其它】   ———— 其他的小改动. 一般为仅仅一两行的改动, 或者连续几次提交的小改动属于这种
```


持续更新ing





由于才刚开始可能会有很多不完善的，请多指证；
有特别好的算法也请及时告知，万分感谢！

本项目内所有程序都经过验证可用


算法参考：https://gitee.com/Doocs/leetcode
个人博客：https://monamania.gitee.io/


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

