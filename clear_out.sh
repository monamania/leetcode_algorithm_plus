#!/bin/sh
echo "删除.out文件"
find ./ -name "*.out"  | xargs rm -rf

echo "删除可执行文件文件"
find ./ -perm /+x -type f  | xargs rm -rf
# 询问方式
# find . -name "*.out"  | xargs rm -ri

echo -n "刷题文件目录下，文件个数统计: "
ls -lR ./刷题 | grep "^-" | wc -l

echo -n "刷题文件目录下，文件夹个数统计: "
ls -lR ./刷题 | grep "^d" | wc -l